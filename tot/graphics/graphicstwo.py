#coding:utf-8
'''
Created on 2015��4��24��

@author: guowu
'''
from PyQt4.QtGui import QWidget, QLabel, QPixmap, QApplication, QPushButton
from PyQt4.QtCore import QPoint, SIGNAL, Qt, QTimeLine, QSize, QString, QTimer
import sys
import time

class MyLabel(QLabel):
    def __init__(self,parent=None):
        super(MyLabel,self).__init__(parent)
        
        self.mousePress = False
        
    def mousePressEvent(self,event):
        self.mousePress = True
    def mouseReleaseEvent(self,event):
        self.mousePress = False
        self.emit(SIGNAL("clicked()"))
    def mouseMoveEvent(self,event):
        if self.mousePress == True:
            print event.pos()

class SlideGraphic(QWidget):
    def __init__(self,types,parent=None):
        super(SlideGraphic,self).__init__(parent)
        self.resize(QSize(3000,400))
        
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.setAttribute(Qt.WA_TranslucentBackground,True)
        self.type = types
        self.piclist = ["images/brownies.jpg","images/chocolate.jpg","images/cookies.jpg","images/fish.jpg","images/mussels.jpg","images/pasta.jpg","images/pizza.jpg","images/puding.jpg","images/sushi.jpg","images/trouts.jpg"]
        
        self.labelList = []
        self.index = None
        self.waitValue = 0
        self.initAllLabel()
        
        self.waitTimer = QTimer()
        self.connect(self.waitTimer, SIGNAL("timeout()"),self.slotUpdateValue)
        
        
        self.lineTimer = QTimeLine()
        self.lineTimer.setFrameRange(0,100)
        self.lineTimer.setDuration(600)
        
        self.connect(self.lineTimer, SIGNAL("frameChanged(int)"), self.updateGraphic)
        self.connect(self.lineTimer, SIGNAL("finished()"), self.finishLinetime)
        
    def slotUpdateValue(self):
        self.waitValue = 1
        self.waitTimer.stop()
        
    def setPictureSize(self,size):
        self.pictureSize = size
        
    def initAllLabel(self):
        if self.type == 0:
            self.pictureSize = 30
        else:
            self.pictureSize = 100
        
        for i in range(len(self.piclist)):
            picLabel = MyLabel(self)
            picLabel.setObjectName(str(i))
            pixmap = QPixmap(self.piclist[i]).scaled(self.pictureSize,self.pictureSize)
            picLabel.setPixmap(pixmap)
            self.labelList.append(picLabel)
            
        for i in range(len(self.labelList)):
            self.labelList[i].move(QPoint(i*(self.pictureSize+self.pictureSize/6.00) + self.pictureSize/2.00,self.pictureSize/2.00))
            self.connect(self.labelList[i], SIGNAL("clicked()"),self.slotLabelClicked)
            
    def setCenterPoint(self,point):
        self.centerPointO = point
        
    def finishLinetime(self):
        #time.sleep(0.1)
        self.otherLabelClicked()
        
    def otherLabelClicked(self):
        if self.lineTimer.state() != QTimeLine.NotRunning:
            return
        
        if self.index == self.lastIndex:
            return
        
        if self.index > self.lastIndex:
            self.index-=1
            
        if self.index < self.lastIndex:
            self.index+=1
            
        

        self.indexpos = self.labelList[int(self.index)].pos()
        self.indexcenter = self.pos() + self.labelList[int(self.index)].pos() + QPoint(self.pictureSize/2.00,self.pictureSize/2.00)
        #print self.indexcenter
        self.centerPoint = QPoint(self.centerPointO.x(),100)
        self.betPointDirect = self.indexcenter.x() - self.centerPoint.x()
        
        self.currentPos = self.pos()
        self.lastPoint = QPoint(self.pos().x() - self.betPointDirect ,self.pos().y())
        
        self.perFrameDic = float(-self.betPointDirect/100.000)
        
        #self.move(QPoint(self.pos().x() - self.betPointDirect ,self.pos().y()))
        print QPoint(self.pos().x() - self.betPointDirect ,self.pos().y())
        self.lineTimer.start()
    
    def slotLabelClicked(self):
        if self.waitTimer.isActive():
            return
        else:
            self.waitTimer.start(700)
            self.waitValue = 0
        if self.lineTimer.state() != QTimeLine.NotRunning:
            return
        currentLabel = self.sender()
        self.lastIndex = int(currentLabel.objectName())
        if self.index == None:
            self.index = self.lastIndex
            
        else:
            if self.index == self.lastIndex:
                return
        
        self.emit(SIGNAL("labelclicked"),self.lastIndex)
        
        if self.index > self.lastIndex:
            self.lineTimer.setDuration(500.00/(self.index-self.lastIndex))
            self.index-=1
            
        if self.index < self.lastIndex:
            self.lineTimer.setDuration(500.00/(self.lastIndex-self.index))
            self.index+=1
        
        self.indexpos = self.labelList[int(self.index)].pos()
        self.indexcenter = self.pos() + self.labelList[int(self.index)].pos() + QPoint(self.pictureSize/2.00,self.pictureSize/2.00)
        #print self.indexcenter
        self.centerPoint = QPoint(self.centerPointO.x(),100)
        self.betPointDirect = self.indexcenter.x() - self.centerPoint.x()
        
        self.currentPos = self.pos()
        self.lastPoint = QPoint(self.pos().x() - self.betPointDirect ,self.pos().y())
        
        self.perFrameDic = float(-self.betPointDirect/100.000)
        
        #self.move(QPoint(self.pos().x() - self.betPointDirect ,self.pos().y()))
        print QPoint(self.pos().x() - self.betPointDirect ,self.pos().y())
        self.lineTimer.start()
        
        
    def likeLabelClicked(self,indexx):
        if self.lineTimer.state() != QTimeLine.NotRunning:
            return
        self.lastIndex = int(indexx)
        if self.index == None:
            self.index = self.lastIndex
            
        else:
            if self.index == self.lastIndex:
                return
        
        if self.index > self.lastIndex:
            self.lineTimer.setDuration(500.00/(self.index-self.lastIndex))
            self.index-=1
            
        if self.index < self.lastIndex:
            self.lineTimer.setDuration(500.00/(self.lastIndex-self.index))
            self.index+=1
            
        self.indexpos = self.labelList[int(self.index)].pos()
        self.indexcenter = self.pos() + self.labelList[int(self.index)].pos() + QPoint(self.pictureSize/2.00,self.pictureSize/2.00)
        self.centerPoint = QPoint(self.centerPointO.x(),100)
        self.betPointDirect = self.indexcenter.x() - self.centerPoint.x()
        
        self.currentPos = self.pos()
        self.lastPoint = QPoint(self.pos().x() - self.betPointDirect ,self.pos().y())
        
        self.perFrameDic = float(-self.betPointDirect/100.000)
        
        #self.move(QPoint(self.pos().x() - self.betPointDirect ,self.pos().y()))
        print QPoint(self.pos().x() - self.betPointDirect ,self.pos().y())
        self.lineTimer.start()
        
    def updateGraphic(self):
        
        current = self.lineTimer.currentFrame()
        current =  current*(self.pictureSize/100.00)
        for i in range(len(self.piclist)):
            if i == int(self.index):
                self.labelList[i].resize(self.pictureSize+current,self.pictureSize+current)
                pixmap = QPixmap(self.piclist[i]).scaled(self.pictureSize+current,self.pictureSize+current)
                self.labelList[i].setPixmap(pixmap)
                self.labelList[i].move(QPoint(self.indexpos.x()-current/2.00,self.indexpos.y() - current/2.00))
            else:
                self.labelList[i].resize(self.pictureSize,self.pictureSize)
                pixmap = QPixmap(self.piclist[i]).scaled(self.pictureSize,self.pictureSize)
                self.labelList[i].setPixmap(pixmap)
                self.labelList[i].move(QPoint(i*(self.pictureSize+self.pictureSize/6.00) + self.pictureSize/2.00,self.pictureSize/2.00))
        self.labelList[int(self.index)].raise_()
        #print QPoint(self.currentPos.x() + self.perFrameDic*(current),self.currentPos.y())
        self.move(QPoint(self.currentPos.x() + self.perFrameDic*(current*100.00/self.pictureSize),self.currentPos.y()))
    

# if __name__ == "__main__":
#     app = QApplication(sys.argv)
#     mainwindow = SlideGraphic(1)
#     mainwindow.show()
#     app.exec_()       
   
    