# -*- coding: utf-8 -*-  
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from button import MyButton
import sys

QTextCodec.setCodecForTr(QTextCodec.codecForName("utf8"))


class TestDialog(QDialog):
    def __init__(self,parent=None):
        super(TestDialog,self).__init__(parent)
        self.setFixedSize(200,200)
        self.firMybutton = MyButton()
        self.firMybutton.setFixedSize(100,100)
        self.firMybutton.setIcon(QIcon("pb.png"))
        self.firMybutton.setIconSize(QSize(100,100))
        self.firMybutton.setText(self.tr("确萨"))
        
        
        myLayout = QHBoxLayout()
        myLayout.addWidget(self.firMybutton)
        self.setLayout(myLayout)
        
        
app=QApplication(sys.argv)
dialog=TestDialog()
dialog.show()
app.exec_()