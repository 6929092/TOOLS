# -*- coding: utf-8 -*-  

from PyQt4.QtCore import QTextCodec
from PyQt4.QtGui import QApplication
from mainWindow import PaintButton
import sys

QTextCodec.setCodecForTr(QTextCodec.codecForName("utf-8"))

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = PaintButton()
    window.show()
    app.exec_()

