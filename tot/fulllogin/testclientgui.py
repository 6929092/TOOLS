#coding:utf-8
'''
Created on 2015��7��1��

@author: guowu
'''
from PyQt4.QtGui import QWidget, QApplication, QPalette, QColor, QHBoxLayout,\
    QLabel, QLineEdit, QPushButton, QGridLayout, QVBoxLayout, QPainter, QPixmap,\
    QBrush, QFont
import sys
from PyQt4.QtCore import Qt, QSize, QPoint
import test

class ClientGui(QWidget):
    def __init__(self,parent=None):
        super(ClientGui,self).__init__(parent)
        
        self.setWindowFlags(Qt.WindowStaysOnTopHint)
        
        #self.setAttribute(Qt.WA_TranslucentBackground,True)
        #self.setStyleSheet("LineEdit{background-color:rgb(0,0,0,0);}")
        
        
        #self.pe = QPalette(QPalette("img/riven.jpg"))
        #self.setAutoFillBackground(True)
        #self.setPalette(self.pe)
         
        self.loginWidget = LoginGui(self)
        
        
        self.gridLayout = QHBoxLayout(self)
        self.gridLayout.setAlignment(Qt.AlignCenter)
        self.gridLayout.addWidget(self.loginWidget)
        
        self.setFullSize()
        
    def setFullSize(self):
        self.showFullScreen()
        
    def paintEvent(self,event):
        
        painter = QPainter(self)
        painter.drawPixmap(self.rect(), QPixmap(":picture/img/riven.jpg"))
        
        
class LoginGui(QWidget):
    def __init__(self,parent=None):
        super(LoginGui,self).__init__(parent)
        self.setFixedSize(QSize(400,700))
        
        self.setStyleSheet("QLineEdit{background-color:rgb(255,255,255,150);}"
                           "QLineEdit{font-family:'Microsoft YaHei'; font-size:15px;color:#666666;}"
                           "QLabel{font-family:'Microsoft YaHei'; font-size:15px;color:rgb(255,255,255);}"
                           "QPushButton{font-family:'Microsoft YaHei'; font-size:15px; color:#666666;}"
                           "QPushButton{background-color:rgb(255,255,255,250);border:1px; border-radius:2px;}"
                           "QPushButton:hover{background-color:rgb(145,215,225,200);border:1px solid;}"
                           "QPushButton:pressed{background-color:rgb(45,115,225,200);border:1px solid;}"
                           )
        
        
        self.labelLogo = QLabel()
        self.labelLogo.setFixedSize(QSize(128,128))
        logopix = QPixmap(":picture/img/send.png")
        self.labelLogo.setPixmap(logopix)
        
        self.labelTip = QLabel(u"学生签到入口")
        self.labelTip.setAlignment(Qt.AlignCenter)
        
        self.labelTerminaName = QLabel(u"学生机：")
        
        self.lineEditTerminaNameK = QLineEdit(u"student-001")
        self.lineEditTerminaNameK.setFrame(False)
        self.lineEditTerminaNameK.setFixedWidth(200)
        self.lineEditTerminaNameK.setFixedHeight(25)
        self.lineEditTerminaNameK.setReadOnly(True)
        
        self.labelStudentName = QLabel(u"用户名：")
        self.lineEditStudentName = QLineEdit()  
        self.lineEditStudentName.setFrame(False)
        self.lineEditStudentName.setFixedWidth(200)
        self.lineEditStudentName.setFixedHeight(25)
        
        self.labelStudentPass = QLabel(u"密    码：")
        self.lineEditPass = QLineEdit()
        self.lineEditPass.setFrame(False)
        self.lineEditPass.setFixedWidth(200)
        self.lineEditPass.setFixedHeight(25)
        
        self.buttonLogin = QPushButton(u"进 入")
        self.buttonLogin.setFixedSize(QSize(80,25))
        self.buttonReset = QPushButton(u"重 置")
        self.buttonReset.setFixedSize(QSize(80,25))
        
        self.gridLayout = QGridLayout()
        self.gridLayout.setAlignment(Qt.AlignCenter)
        self.gridLayout.setMargin(50)
        self.gridLayout.setSpacing(20)
        self.gridLayout.addWidget(self.labelTerminaName,0,0,1,1)
        self.gridLayout.addWidget(self.lineEditTerminaNameK,0,1,1,1)
        self.gridLayout.addWidget(self.labelStudentName,1,0,1,1)
        self.gridLayout.addWidget(self.lineEditStudentName,1,1,1,1)
        self.gridLayout.addWidget(self.labelStudentPass,2,0,1,1)
        self.gridLayout.addWidget(self.lineEditPass,2,1,1,1)
        
        
        self.logoLayout = QHBoxLayout()
        self.logoLayout.addStretch()
        self.logoLayout.addWidget(self.labelLogo)
        self.logoLayout.addStretch()
        
        self.buttonLayout = QHBoxLayout()
        self.buttonLayout.addStretch()
        self.buttonLayout.addWidget(self.buttonLogin)
        self.buttonLayout.addSpacing(20)
        self.buttonLayout.addWidget(self.buttonReset)
        self.buttonLayout.addStretch()
        
        self.lastLayout = QHBoxLayout()
        self.lastLayout.addStretch()
        self.lastLayout.addWidget(self.labelTip)
        self.lastLayout.addStretch()
        
        self.mainLayout = QVBoxLayout(self)
        self.mainLayout.addSpacing(100)
        self.mainLayout.addLayout(self.logoLayout)
        self.mainLayout.addLayout(self.gridLayout)
        self.mainLayout.addLayout(self.buttonLayout)
        self.mainLayout.addLayout(self.lastLayout)
        
        
    def paintEvent(self,event):
        painter = QPainter(self)
        painter.setRenderHint(QPainter.SmoothPixmapTransform, True)#像素光滑
        painter.setRenderHint(QPainter.Antialiasing, True)#反锯齿
        pix = QPixmap(":picture/img/login.jpg").scaled(self.width(),self.height())
        painter.setBrush(QBrush(pix))
        painter.drawRoundRect(self.rect(),5,5)
        

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = ClientGui()
    window.show()
    app.exec_()