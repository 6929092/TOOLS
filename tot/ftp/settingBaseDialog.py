#coding:utf-8
from PyQt4.QtGui import QDialog, QApplication, QLabel, QPushButton, QLineEdit,\
    QPainter, QPixmap, QComboBox
import sys
from PyQt4.QtCore import Qt, QSize, QPoint, SIGNAL, SLOT, QState

class SettingBaseDialog(QDialog):

    def __init__(self, params=None):
        super(SettingBaseDialog, self).__init__(params)
        self.setWindowFlags(Qt.Window|Qt.FramelessWindowHint )
        self.setWindowModality(Qt.ApplicationModal)
        
        
        
        
#         self.setWindowFlags(Qt.Window|Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint | Qt.CustomizeWindowHint)
#         self.setModal(True)
#         self.setAttribute(Qt.AA_ShowModal,True)
#         self.setWindowFlags(Qt.Window)
        
#         self.setFocusPolicy(Qt.TabFocus)
        self.mousePressed = False
        self.resize(QSize(435,355))
        
        self.tittleLabel = SettingBaseDialogTittleLabel(self)
        self.tittleLabel.move(QPoint(0,0))
        
        self.closeButton = CloseButton(self)
        self.closeButton.move(QPoint(self.width() -self.closeButton.width() -5, 5))
        self.connect(self.closeButton, SIGNAL("clicked()"), SLOT('close()'))
        self.setStyleSheet('QDialog{background-color:rgb(255,255,255);'
                                    'border:1px solid #2261C7;'
                            'border-radius:2px;'
                             '}')
#         self.closeButton.setAutoDefault(False)
#     def keyPressEvent(self, event):
#         return
#         
#         if event.key() == Qt.Key_0:
#     def closeEvent(self, event):
#         return
#     def hideEvent(self, event):
#         return
    def resizeEvent(self, event):
        self.closeButton.move(QPoint(self.width() -self.closeButton.width() -5, 5))
    def setTittleText(self, tittleText='test'):
        self.tittleLabel.setText(tittleText)
    #       # 鼠标移动事件
    def mouseMoveEvent(self, event):
        if self.mousePressed:
            self.move(self.pos() + event.pos() - self.currentPos)
    def mousePressEvent(self, event):
        if event.buttons() == Qt.LeftButton:
            self.currentPos = event.pos()
            self.mousePressed = True
    def mouseReleaseEvent(self, event):
        if event.buttons() == Qt.LeftButton:
            self.mousePressed = False
            
class SendfileSettingBaseDialog(QDialog):

    def __init__(self, params=None):
        super(SendfileSettingBaseDialog, self).__init__(params)
        self.setWindowFlags(Qt.Window|Qt.FramelessWindowHint )
#         self.setWindowFlags(Qt.Window|Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint | Qt.CustomizeWindowHint)
#         self.setModal(True)
#         self.setAttribute(Qt.AA_ShowModal,True)
#         self.setWindowFlags(Qt.Window)
        self.setWindowModality(Qt.ApplicationModal)
#         self.setFocusPolicy(Qt.TabFocus)
        self.mousePressed = False
        self.resize(QSize(435,355))
        
        self.tittleLabel = SettingBaseDialogTittleLabel(self)
        self.tittleLabel.move(QPoint(0,0))
        
        self.closeButton = CloseButton(self)
        self.closeButton.move(QPoint(self.width() -self.closeButton.width() -5, 5))
#         self.connect(self.closeButton, SIGNAL("clicked()"), SLOT('close()'))
        self.setStyleSheet('QDialog{background-color:rgb(255,255,255);'
                                    'border:1px solid #2261C7;'
                            'border-radius:2px;'
                             '}')
#         self.closeButton.setAutoDefault(False)
#     def keyPressEvent(self, event):
#         return
#         
#         if event.key() == Qt.Key_0:
#     def closeEvent(self, event):
#         return
#     def hideEvent(self, event):
#         return
    def resizeEvent(self, event):
        self.closeButton.move(QPoint(self.width() -self.closeButton.width() -5, 5))
    def setTittleText(self, tittleText='test'):
        self.tittleLabel.setText(tittleText)
    #       # 鼠标移动事件
    def mouseMoveEvent(self, event):
        if self.mousePressed:
            self.move(self.pos() + event.pos() - self.currentPos)
    def mousePressEvent(self, event):
        if event.buttons() == Qt.LeftButton:
            self.currentPos = event.pos()
            self.mousePressed = True
    def mouseReleaseEvent(self, event):
        if event.buttons() == Qt.LeftButton:
            self.mousePressed = False
class SettingBaseDialogtest(QDialog):

    def __init__(self, params=None):
        super(SettingBaseDialogtest, self).__init__(params)
#         self.setWindowFlags(Qt.Window|Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint | Qt.CustomizeWindowHint)
        self.setWindowFlags(Qt.Window|Qt.FramelessWindowHint )
        self.setWindowModality(Qt.ApplicationModal)
#         self.setModal(True)
        self.mousePressed = False
        self.resize(QSize(550,400))
        
        self.tittleLabel = SettingBaseDialogTittleLabel(self)
        self.tittleLabel.move(QPoint(0,0))
        self.tittleLabel.resize(QSize(550,self.tittleLabel.height()))
        
        self.closeButton = CloseButton(self)
        self.closeButton.move(QPoint(self.width() -self.closeButton.width() -5, 5))
        self.connect(self.closeButton, SIGNAL("clicked()"), SLOT('close()'))
        self.setStyleSheet('QDialog{background-color:rgb(255,255,255);'
                                    'border:1px solid #2261C7;'
                            'border-radius:2px;'
                             '}')
    def resizeEvent(self, event):
        self.closeButton.move(QPoint(self.width() -self.closeButton.width() -5, 5))
    def setTittleText(self, tittleText='test'):
        self.tittleLabel.setText(tittleText)
    #       # 鼠标移动事件
    def mouseMoveEvent(self, event):
        if self.mousePressed:
            self.move(self.pos() + event.pos() - self.currentPos)
    def mousePressEvent(self, event):
        if event.buttons() == Qt.LeftButton:
            self.currentPos = event.pos()
            self.mousePressed = True
    def mouseReleaseEvent(self, event):
        if event.buttons() == Qt.LeftButton:
            self.mousePressed = False
    def paintEvent(self, event):
        painter = QPainter(self)
        pixmap = QPixmap("img/settingotherbg.png").scaled(548, 367)
        painter.drawPixmap(1, 32, pixmap)
class CloseButton(QPushButton):
    def __init__(self, params=None):
        super(CloseButton, self).__init__(params)
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.setFixedSize(QSize(21,20))
        self.setStyleSheet("QPushButton{background-image:url(img/settingclose.png);border:0px;}"
                           "QPushButton:hover{background-image:url(img/settingclose1.png);border:0px;}")
class SettingBaseDialogTittleLabel(QLabel):
    def __init__(self, params=None):
        super(SettingBaseDialogTittleLabel, self).__init__(params)
        self.resize(QSize(435,32))
        self.setWindowFlags(Qt.FramelessWindowHint)
#         self.setStyleSheet("QLabel{background-image:url(img/settingBaseDialogTittle.png);border:0px;"
#                            "font:'SimHei';font-weight: bold;font-size:16px;color:rgb(255,255,255);"
#                            "padding-left:10px;}")
        
        self.setStyleSheet("QLabel{background-color:rgb(45,105,202);"
                           "font:'SimHei';font-weight: bold;font-size:16px;color:rgb(255,255,255);"
                            "padding-left:10px;"
                            "border-radius:2px;"
                           "}")
class TextLabel(QLabel):
    def __init__(self, parent=None):
        super(TextLabel, self).__init__(parent)
        self.setAlignment(Qt.AlignRight|Qt.AlignBottom)
#         self.setFixedHeight()
        self.setStyleSheet("QLabel{color:#333333;font-size:14px;margin-bottom:4px;font-weight: bold}")
class InputLineEdit(QLineEdit):
    def __init__(self, parent=None):
        super(InputLineEdit, self).__init__(parent)
        self.setContextMenuPolicy(Qt.NoContextMenu)
        self.setFixedWidth(200)
        self.setStyleSheet("QLineEdit{font:'SimHei';font-size:14px;font-weight: bold;color:#333333;border-radius:1px;"
                                'height:30px;width:150px;border:1px solid #2261C7;}')
#         self.setStyleSheet('QLineEdit{font:"SimHei";font-size:16px;color:rgb(146,146,146);'
#                                 'height:25px;width:50px;border:1px solid #2261C7;selection-color:pink}')
class NotNullSign(QLabel):
    def __init__(self, parent=None):
        super(NotNullSign, self).__init__(parent)
        self.setAlignment(Qt.AlignRight)
#         self.setFixedSize(QSize(2,2))
        self.setText(u'*')
        self.setStyleSheet("QLabel{background-color:rgb(255,255,255);color:red;margin-top:6px;}")
class ConfirmButton(QPushButton):
    def __init__(self, parent=None):
        super(ConfirmButton, self).__init__(parent)
        self.setStyleSheet('QPushButton{font: bold "SimHei";font-size:20px;border-radius:2px;color:#2261C8;'
                                       'background-image:url(img/save.png);width:90px;height:30px}'
                                       'QPushButton:hover{background-image:url(img/save1.png);}'
                                       'QPushButton:pressed{background-image:url(img/save2.png);}'
                                      )
#         self.setFlat(False)
        
#     def enterEvent(self, event):
#         state = QState()
#         state.assignProperty(self, 'flat', False)
class ApplicationButton(QPushButton):
    def __init__(self, parent=None):
        super(ApplicationButton, self).__init__(parent)
        self.setStyleSheet('QPushButton{font: bold "SimHei";font-size:16px;color:#2261C8;border-radius:2px;'
                                                   'background-image:url(img/save.png);width:90px;height:30px}'
                                       'QPushButton:hover{background-image:url(img/save1.png);}'
                                       'QPushButton:pressed{background-image:url(img/save2.png);}'
                                      )
    
class ComboBox(QComboBox):
    def __init__(self, parent=None):
        super(ComboBox, self).__init__(parent)
        self.setStyleSheet("QComboBox{border:1px solid #2261C7;height:20px;width:200px;border-radius:1px;"
                                    "font-size:12px;font:'SimHei';}")
class TextLabelSetting(QLabel):
    def __init__(self, parent=None):
        super(TextLabelSetting, self).__init__(parent)
        self.setAlignment(Qt.AlignRight|Qt.AlignBottom)
#         self.setFixedHeight()
        self.setFixedSize(QSize(100,20))
        self.setStyleSheet("QLabel{font-size:12px;margin-bottom:1px;}")
class AdvanceButton(QPushButton):
    def __init__(self, parent=None):
        super(AdvanceButton, self).__init__(parent)
        self.setStyleSheet('QPushButton{font: bold "SimHei";font-size:12px;color:#2261C8;border-radius:2px;'
                                                   'background-image:url(img/settingok.png);width:85px;height:25px}'
                                       'QPushButton:hover{background-image:url(img/settingok1.png);}'
                                       'QPushButton:pressed{background-image:url(img/settingok2.png);}'
                                      )
if __name__=="__main__":
    app = QApplication(sys.argv)
    tm = SettingBaseDialog()
#     tm.resize(QSize(tm.width(),150))
#     tm.tittleLabel.resize(QSize(tm.width(),100))
    
#     tm.setTittleText()
    tm.show()
    sys.exit(app.exec_())
        