#coding:utf-8
'''
Created on 2015��8��25��

@author: guowu
'''
from PyQt4.QtGui import QWidget, QPainter, QColor, QPen, QApplication, QFont
from PyQt4.QtCore import QSize, Qt, QPoint, QString, QThread, SIGNAL
import imgtest
from setinfoconfig import SetInfoConfig
import sys
import time

class ImgThread(QThread):
    def __init__(self,parent=None):
        super(ImgThread,self).__init__(parent)
        self.currentImg = 1
        
    def run(self):
        while True:
            #textx = imgtest.getxstr(self.currentImg)
            #SetInfoConfig.instance().setConfigFile("img", "%d"%self.currentImg, textx)
            
            text = SetInfoConfig.instance().getConfigFile("img", "%d"%self.currentImg)
            if text == None:
                continue
            textlist = self.parseText(text)
            self.emit(SIGNAL("imgget"),textlist)
            self.currentImg += 2
            if self.currentImg == 6570:
                break
        
            time.sleep(0.06)
    
    def parseText(self,text):
        linelist = text.split("\n")
        textlist = []
        for item in linelist:
            nlist = item.split("#")
            linetext = ""
            for its in nlist:
                xlist = its.split("&")
                #print xlist
                #print int(xlist[0])
                if len(xlist) == 1:
                    if xlist[0] == "":
                        pass
                    else:
                        for i in range(int(xlist[0])):
                            linetext = linetext + " " 
                elif len(xlist) == 2:
                    if xlist[0] == "":
                        pass
                    else:
                        for j in range(int(xlist[0])):
                            linetext = linetext + "&"
                    if xlist[1] == "":
                        pass
                    else:
                        for k in range(int(xlist[1])):
                            linetext = linetext + " "
            
            textlist.append(linetext)
        
            
        return textlist
                        

class WidgetTest(QWidget):
    
    
    def __init__(self,parent = None):
        super(WidgetTest,self).__init__(parent)
        
        self.setFixedSize(QSize(900,700))
        
        self.text = "0"
        self.textlist = []
        
        #textx = imgtest.getxstr()
        #SetInfoConfig.instance().setConfigFile("img", "001", textx)
        
        #self.text = SetInfoConfig.instance().getConfigFile("img", "001")
        
        #self.textlist = self.parseText(self.text)
        self.imgThread = ImgThread()
        self.connect(self.imgThread, SIGNAL("imgget"),self.slotImg)
        self.imgThread.start()
        
    
    def slotImg(self,textlist):
        self.textlist = textlist
        self.repaint()
    
    def paintEvent(self,event):
        font = QFont()
        font.setPointSize(4)
        painter = QPainter(self)
        painter.setFont(font)
        painter.setPen(QPen(QColor(0,0,0),2))
        painter.setBrush(Qt.NoBrush)
        for i in range(len(self.textlist)):
            painter.drawText(QPoint(0,3*i), QString(self.textlist[i]))
        
        

if __name__ == "__main__":
    
    app = QApplication(sys.argv)
    window = WidgetTest()
    window.show()
    
    app.exec_()