﻿# -*- coding: utf-8 -*-  
import socket
import threading
import sys
import time
from gameCo import TestWidget
from mainWindow  import PaintButton
from PyQt4.QtGui import *
from PyQt4.QtCore import *


host = '192.168.5.140'
port = 8805
username = 'Default'

class Client(QWidget):
    def __init__(self, parent = None):
        super(Client, self).__init__(parent)
        self.setFixedSize(630,750)
        #self.setFixedSize(900,600)
        #self.setStyleSheet("QWidget{background:blue;}")
        self.setWindowTitle('CLient')
        self.mainWiget = PaintButton(self)
        self.gameWidget = TestWidget(self)
        #self.setNameWidget = QWidget()
        
        
        self.btnSend = QPushButton(self)
        self.btnSend.setFixedSize(50,30)
        self.btnSend.setText(self.trUtf8("发送"))
        self.btnSend.setFont(QFont("times",15))
        
        self.btnSet = QPushButton(self)
        self.btnSet.setFixedSize(50,30)
        self.btnSet.setText(self.trUtf8("设置"))
        self.input = QLineEdit(self)
        self.input.setFixedSize(240,30)
        self.name = QLineEdit(self)
        self.name.setText('DeFault')
        self.name.setFixedSize(QSize(100,30))
        self.chat = QTextEdit(self)
        self.chat.setFixedSize(QSize(300,250))
        self.label = QLabel(self)
        self.label.setText('name:')
        self.label.setFixedSize(QSize(30,30))
        self.timer = QTimer()
        
        
        #self.mainWiget.move((self.geometry().width()-self.mainWiget.geometry().width())/2,(self.geometry().height()-self.mainWiget.geometry().height())/2)
        self.connect(self.mainWiget,SIGNAL("counter"),self.buttonToServer)
        
        self.connect(self, SIGNAL("paint"),self.paint)

        
        self.messages = []
        #self.messages = core.QString()
        #self.messages = {}
        #self.connect(self, SIGNAL("paint"),self.paint)

        self.build()
        
        self.createAction()
        
        

        recvThread = threading.Thread(target = self.recvFromServer)
        recvThread.setDaemon(True)
        recvThread.start()
      
      
    def paint(self,actionType):
        global username
        print actionType
        
        sList = actionType.split()
        flag = sList[11]
            #print flag
        count = sList[1]
        if sList[0] != username:
            bList = [sList[2],sList[3],sList[4],sList[5],sList[6],sList[7],sList[8],sList[9],sList[10]]
            self.mainWiget.leftMenu3.countLabel.setText(self.trUtf8(sList[1]))
            for n in range(0,9):
                if bList[n] == "1":
                    self.gameWidget.buttonList[n].setIcon(QIcon("dsaf.png"))
                    self.gameWidget.buttonList[n].setIconSize(QSize(80,80))
                else:
                    self.gameWidget.buttonList[n].setIcon(QIcon())
                    #self.mainWidget.buttonList[n].setIconSize(80,80)
    def buttonToServer(self):
        global username
        b = 'button'
        #text = QString().number(self.mainWiget.countNum)
        text = self.mainWiget.countNum
        a = self.mainWiget.slotSignal.testWidget.fList
        try:
            #s.send(a,username,text)
            s.send('%s %s %s %s %s %s %s %s %s %s %s %s' % (username, text,a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],b))
            print '%s >> %s' % (username, text)
        except:
            self.exit()
    def sendToServer(self):
        global username
        text = str(self.input.text())
        self.input.setText('')
        if text == 'q':
            self.exit()
        elif text.strip() == '':
            return
        try:
            s.send('%s: %s' % (username, text))
            print '%s >> %s' % (username, text)
        except:
            self.exit()

    def recvFromServer(self):
        while 1:
            try:
                data = s.recv(1024)
                if not data:
                    exit()
                self.messages.append(data)
                #print self.messages
            except:
                return

    def showChat(self):
        for m in self.messages:
            #mList = m.split()
            #print len(m)
            if len(m.split()) == 12:
                print "sss"
                if (m.split()[11]) != "button":
                    self.chat.append(self.trUtf8(m))
                else:
                    print "aaa"
                    self.emit(SIGNAL("paint"),m)
            else:
                    self.chat.append(self.trUtf8(m))
            #print m
            #print self.messages
        self.messages = []
    def slotExtension(self):
        global username
        name = str(self.name.text())
        if name.strip() != '':
            username = name
            print username
        self.setNameWidget.hide()

    def exit(self):
        s.close()
        sys.exit()

    def build(self):
        
        self.chat.move(10,400)
        
        self.input.move(10,660)
        self.btnSend.move(260,660)
        
        self.label.move(10,700)
        self.name.move(50,700)
        self.btnSet.move(260,700)
        
        self.gameWidget.move(320,400)
        
        
        self.mainWiget.move(10,0)
    def createAction(self):
        self.btnSend.clicked.connect(self.sendToServer)
        self.btnSet.clicked.connect(self.slotExtension)
        self.timer.timeout.connect(self.showChat)
        self.timer.start(1000)




s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print "sfa"
s.connect((host, port))
s.send(username)
firstData = s.recv(1024)
print firstData
print '[%s] connect' % username
firstNum = int(firstData)
if firstNum < 3:
    app = QApplication(sys.argv)
    c = Client()
    c.show()
    app.exec_()