# -*- coding: utf-8 -*-  

from PyQt4.QtCore import Qt
from PyQt4.QtCore import QRect
from PyQt4.QtGui  import QPushButton
from PyQt4.QtGui  import QPainter
from PyQt4.QtGui  import QPen
from PyQt4.QtGui  import QColor
from PyQt4.QtGui  import QPixmap
from PyQt4.QtGui  import QIcon
class MyButton(QPushButton):
        
    def __init__(self,parent = None):
        super(MyButton,self).__init__(parent)
        
        self.hovered = False
        self.pressed = False
        self.pressedIcon = QIcon()
        
        
        
        self.color = QColor(Qt.lightGray)
        self.shadow = QColor((Qt.white))
        self.highlight = QColor(Qt.darkGray)
        self.opacity = 1.0;
        
        
    
    def setColor(self,color):
        self.color = color
        
    def setHighlight(self,highlight):
        self.highlight = highlight
    
    def setShadow(self,shadow):
        self.shadow = shadow
        
    def setOpacity(self,opacity):
        self.opacity = opacity
        
    def enterEvent(self,event):
        #self.hovered = True
        self.setIcon(QIcon("pb1.png"))
        self.repaint()
        QPushButton.enterEvent(self,event)
        
    def leaveEvent(self,event):
        #self.hovered = False
        self.setIcon(QIcon("pb.png"))
        self.repaint()
        QPushButton.leaveEvent(self,event)
        
        
        
    def mousePressEvent(self, event):
        #self.pressed = True
        self.setIcon(QIcon("pb2.png"))
        self.repaint()
        QPushButton.mousePressEvent(self,event)
    
    def mouseReleaseEvent(self, event):
        #self.pressed = False
        self.setIcon(QIcon("pb1.png"))
        self.repaint()
        QPushButton.mouseReleaseEvent(self,event)
        
        
        
                
    def paintEvent(self,event):
        
        
        painter = QPainter(self)
        
        btnRect = self.geometry()
        
        
        iconRect = self.iconSize()
        
        
        iconPos,textPos = self.calIconTextPos(btnRect, iconRect)
        # �ػ�ͼ��
        if not self.icon().isNull():
            painter.drawPixmap(iconPos, QPixmap(self.icon().pixmap(self.iconSize())))
        # �ػ��ı�
        if not self.text().isNull():
            painter.setFont(self.font())
            painter.setPen(QPen(QColor(Qt.black),2))
            painter.drawText(textPos.x(), textPos.y(), textPos.width(), textPos.height(), Qt.AlignCenter, self.text())
        
    # ����ͼ����ı���Сλ��
    def calIconTextPos(self,btnSize,iconSize):
        
        
        iconX = (btnSize.width()-iconSize.width())/2
        iconY = (btnSize.height()-iconSize.height())/2
        
        
        iconWidth = iconSize.width()
        
        if self.text().isNull():
            iconHeight = iconSize.height()
        else:
            iconHeight = iconSize.height()
            
            
        iconPos = QRect()
        iconPos.setX(iconX)
        iconPos.setY(iconY)
        iconPos.setWidth(iconWidth)
        iconPos.setHeight(iconHeight)
        
        
        
        textPos = QRect()
        if not self.text().isNull():
            textPos.setX(iconX)
            textPos.setY(iconY)
            textPos.setWidth(iconWidth)
            textPos.setHeight(iconHeight)
        return (iconPos,textPos)

            