#coding:utf-8
'''
Created on 2014��8��26��

@author: GuoWu
'''
from PyQt4.QtGui import *
from PyQt4.QtCore import *
import sys

QTextCodec.setCodecForTr(QTextCodec.codecForName("utf8"))


class TestDialog(QDialog):
    def __init__(self,parent=None):
        super(TestDialog,self).__init__(parent)
        self.setFixedSize(300,300)
        self.setWindowFlags(Qt.FramelessWindowHint|Qt.WindowStaysOnTopHint)
        self.setStyleSheet("QWidget{border:10px;}"
                           "QPushButton{border:5px;}"
                           "QPushButton:hover{background:red;}"
                           "QPushButton:pressed{background:black;}")
        
        pix = QPixmap("back.png").scaled(self.width(),self.height())
        palette = QPalette(self)
        palette.setBrush(QPalette.Background,QBrush(pix))
        self.setPalette(palette)
        self.setMask(pix.mask())
        
        self.tipLabel = QLabel(u"圆角窗口",self)
        self.tipLabel.setFont(QFont("times",13,QFont.Bold))
        self.tipLabel.setStyleSheet("color:white;")
        
        self.closeButton = QPushButton(self)
        self.closeButton.setFixedSize(30,30)
        self.closeButton.setIcon(QIcon("0.png"))
        self.closeButton.setIconSize(QSize(30,30))
        self.connect(self.closeButton, SIGNAL("clicked()"),self.slotClose)

    def slotClose(self):
        self.close()
    
    def mouseMoveEvent(self,event):
        if self.mousePressed:
            self.move(self.pos() + event.pos() - self.currentPos)
   
    def mousePressEvent(self,event):
        if event.buttons() == Qt.LeftButton:
            self.currentPos = event.pos()
            self.mousePressed = True
   
    def mouseReleaseEvent(self,event):
        if event.buttons() == Qt.LeftButton:
            self.mousePressed = False    
        
    def paintEvent(self,event):
        self.closeButton.move(self.width()-50,55)
        self.tipLabel.move(10,10)
        
       
app=QApplication(sys.argv)
dialog=TestDialog()
dialog.show()
app.exec_()