#coding:utf-8
from PyQt4.QtGui import *
from PyQt4.QtCore import *
import sys

class FileDir(QWidget):
    def __init__(self,parent= None):
        super(FileDir,self).__init__(parent)
        self.resize(400,400)

        self.tree = QTreeView()
        
        self.model = QDirModel(self.tree)
        self.model.setReadOnly(False)
        self.model.setSorting(QDir.DirsFirst |QDir.IgnoreCase |QDir.Name)
        
        self.tree.setModel(self.model)
        self.tree.header().setStretchLastSection(True) 
        self.tree.header().setSortIndicator(0, Qt.AscendingOrder) 
        self.tree.header().setSortIndicatorShown(True) 
        self.tree.header().setClickable(True)

        index = QModelIndex(self.model.index(QDir.currentPath()))
        self.tree.expand(index)
        self.tree.scrollTo(index)
        self.tree.resizeColumnToContents(0)
        
        self.dirLine = QLineEdit()
        self.mkdirButton = QPushButton(u'新建文件夹')
        self.mkdirButton.setFixedSize(80,30)
        self.rmButton = QPushButton(u'删除文件夹')
        self.rmButton.setFixedSize(80,30)
        self.okButton = QPushButton(u'确定')
        self.okButton.setFixedSize(80,30)
        self.connect(self.mkdirButton, SIGNAL("clicked()"),self.mkDir)
        self.connect(self.rmButton,SIGNAL("clicked()"),self.rm)
        self.connect(self.okButton,SIGNAL("clicked()"),self.sure)
        hLayout = QHBoxLayout()
        hLayout.addWidget(self.mkdirButton)
        hLayout.addWidget(self.rmButton)
        hLayout.addWidget(self.okButton)
        
        
        self.mainWindow = QVBoxLayout(self)
        self.mainWindow.addWidget(self.dirLine)
        self.mainWindow.addWidget(self.tree)
        self.mainWindow.addLayout(hLayout)
        self.connect(self.tree,SIGNAL("clicked(QModelIndex)"),self.xiu)
        
        self.clickCount=0
    def xiu(self):
        self.clickCount+=1
        print self.tree.currentIndex().data().toString()
        print self.model.filePath(self.tree.currentIndex())
        self.dirLine.setText(self.model.filePath(self.tree.currentIndex()))
        if self.model.fileInfo(self.tree.currentIndex()).isDir():
            self.noydir = 'dir'
            if self.clickCount==1:
                self.tree.expand(self.tree.currentIndex())
            else:
                self.tree.collapse(self.tree.currentIndex())
                self.clickCount = 0
        else:
            self.noydir = 'file'
            
        print self.noydir
    def mkDir(self):
        index = QModelIndex(self.tree.currentIndex())
        print "mkdir"
        if (not index.isValid()):
            return
        
        dirName,ok= QInputDialog.getText(self, u"Put File:",u"Please input file name:", QLineEdit.Normal, QString()) 
        if(ok and not dirName.isEmpty()):
        #if (dirName!=''):
            if (not self.model.mkdir(index, dirName).isValid()):
                        QMessageBox.information(self,u"Create Directory",u"Failed to create the directory") 
    def rm(self):

        index = QModelIndex(self.tree.currentIndex())
        if (not index.isValid()):
                return
        
        if (self.model.fileInfo(index).isDir()):
                ok = self.model.rmdir(index)
        else:
                ok = self.model.remove(index)
        
        if (not ok):
                QMessageBox.information(self,u"Remove",u"Failed to remove %1".arg(self.model.fileName(index)))
                                                                 
    def sure(self):
        tr = self.dirLine.text()
        noydir = self.noydir
        self.emit(SIGNAL("iamsure"),tr,noydir)  
        self.close()                                                       
        
                                                                
