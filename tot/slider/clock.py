# -*- coding: UTF-8 -*-
#!/usr/bin/env python

#参考PyQt自带示例
#参考开源中国社区 大孟 的透明时钟  http://www.oschina.net/code/snippet_130581_9774

from PyQt4 import QtCore, QtGui

class AnalogClock(QtGui.QWidget):
    #时针图形坐标
    hourHand = QtGui.QPolygon([
        QtCore.QPoint(6, 8),
        QtCore.QPoint(0, 4),
        QtCore.QPoint(-6, 8),
        QtCore.QPoint(0, -50)
    ])
    #分针图形坐标
    minuteHand = QtGui.QPolygon([
        QtCore.QPoint(5, 10),
        QtCore.QPoint(0,6),
        QtCore.QPoint(-5, 10),
        QtCore.QPoint(0, -70)
    ])
    #秒针图形坐标
    secondHand = QtGui.QPolygon([
           QtCore.QPoint(3, 12),
           QtCore.QPoint(0, 8),
           QtCore.QPoint(-3, 12),
           QtCore.QPoint(0, -90)
    ])

    hourColor = QtGui.QColor(0, 0, 255)#时针颜色定义
    minuteColor = QtGui.QColor(0, 255, 0)#分针颜色定义
    secondColor = QtGui.QColor(255, 0, 0)#秒针颜色定义

    def __init__(self, parent=None):
        super(AnalogClock, self).__init__(parent)

        self.setWindowFlags(QtCore.Qt.FramelessWindowHint|QtCore.Qt.WindowStaysOnTopHint|QtCore.Qt.SubWindow )#窗体属性设置
        self.setAttribute(QtCore.Qt.WA_TranslucentBackground, True)#背景透明化

        timer = QtCore.QTimer(self)
        timer.timeout.connect(self.update)
        timer.start(1000)

        quitAction = QtGui.QAction("E&xit", self, shortcut="Ctrl+Q",
                 triggered=QtGui.qApp.quit)#定义退出方式，创建快捷方式
        self.addAction(quitAction)#添加控件动作
        self.setContextMenuPolicy(QtCore.Qt.ActionsContextMenu)#实现右键菜单

        self.setToolTip("Drag the clock with the left mouse button.\n"
                 "Use the right mouse button to open a context menu.\n"
                 "Welcome to http://showyour.name")#鼠标移动到可见控件上则会显示提示内容

        self.resize(200, 200)#窗口大小定义

    #鼠标左键点击动作
    def mousePressEvent(self, event):
        if event.button() == QtCore.Qt.LeftButton:
            self.dragPosition = event.globalPos() - self.frameGeometry().topLeft()#globalPos相对于屏幕的坐标值，跟着窗口移动；frameGeometry获得窗体的左上顶点坐标值
            event.accept()

    #鼠标左键点击后窗体移动动作
    def mouseMoveEvent(self, event):
        if event.buttons() == QtCore.Qt.LeftButton:
            self.move(event.globalPos() - self.dragPosition)#移动窗体
            event.accept()

    #绘制事件
    def paintEvent(self, event):
        side = min(self.width(), self.height())
        time = QtCore.QTime.currentTime()

        painter = QtGui.QPainter(self)
        painter.setRenderHint(QtGui.QPainter.Antialiasing)#开启抗锯齿，使图像边缘看起来更平滑
        painter.translate(self.width() / 2, self.height() / 2)#变换坐标系统，使坐标系平移
        painter.scale(side / 200.0, side / 200.0)#比例变换

        #处理时针
        painter.setPen(QtCore.Qt.NoPen)#设置绘图工具画笔，定义了如何绘制线和轮廓，为绘制时针准备
        painter.setBrush(AnalogClock.hourColor)#设置绘图工具的画刷，定义了时针颜色

        painter.save()#保存当前绘图工具状态（把状态压到栈中），与 restore() 对应
        painter.rotate(30.0 * (time.hour() + time.minute() / 60.0))#计算时针走过的读书，并逆时针旋转坐标系统
        painter.drawConvexPolygon(AnalogClock.hourHand)#绘制时针指针
        painter.restore()#恢复当前绘图工具状态（从栈中弹出一个保存的状态）

        #绘制时针刻度
        painter.setPen(AnalogClock.hourColor)#设置绘图工具画笔，定义了颜色，为绘制时针刻度准备
        for i in range(12):
            painter.drawLine(88, 0, 96, 0)#绘制从(88,0)到(96,0)的直线，并且设置当前画笔位置为(96,0)，则时针刻度长度为8
            painter.rotate(30.0)#逆时针方向旋转坐标系统【360/12=30】度

        #处理分针
        painter.setPen(QtCore.Qt.NoPen)
        painter.setBrush(AnalogClock.minuteColor)

        painter.save()
        painter.rotate(6.0 * ( time.minute() + time.second() / 60.0))#计算分针走过的度数，并逆时针旋转坐标系统
        painter.drawConvexPolygon(AnalogClock.minuteHand)#绘制分针指针
        painter.restore()

        #绘制分针刻度
        painter.setPen(AnalogClock.minuteColor)
        for j in range(60):
            if (j % 5) != 0:#不绘制与时针重合的刻度
                painter.drawLine(92, 0, 96, 0)#绘制从(92,0)到(96,0)的直线，并且设置当前画笔位置为(96,0)，则时针刻度长度为4
            painter.rotate(6.0)#逆时针方向旋转坐标系统【360/60=6】度

        #处理秒针
        painter.setPen(QtCore.Qt.NoPen)
        painter.setBrush(AnalogClock.secondColor)

        painter.save()
        painter.rotate(6.0 * time.second())#计算秒针走过的度数，并逆时针旋转坐标系统
        painter.drawConvexPolygon(AnalogClock.secondHand)#绘制秒针指针
        painter.restore()

        painter.end()

if __name__ == '__main__':

    import sys

    app = QtGui.QApplication(sys.argv)
    clock = AnalogClock()
    clock.show()
    sys.exit(app.exec_())