#coding:utf-8
'''
Created on 2015��6��29��

@author: guowu
'''
from PyQt4.QtGui import QWidget, QApplication, QCursor, QPainter, QColor, QPen,\
    QPalette
from PyQt4.QtCore import QSize, QPoint
import sys
from PyQt4.QtCore import Qt

class PaintBoard(QWidget):
    def __init__(self,parent=None):
        super(PaintBoard,self).__init__(parent)
        self.setFixedSize(QSize(700,700))
        
        pe = QPalette()
        pe.setColor(QPalette.Background,Qt.white)
        self.setAutoFillBackground(True)
        self.setPalette(pe)
        
        self.firstpoint = QPoint()
        self.lastpoint = QPoint()
        self.listlist = []
        self.alist = []
    
    def getMousePos(self):
        aaa = QCursor.pos()
        print aaa.x()
        print aaa.y()
        
    def mousePressEvent(self,event):
        self.alist.append(event.pos())
        
    def mouseReleaseEvent(self,event):
        self.listlist.append(self.alist)
        print len(self.alist)
        self.alist = []
        #self.update()
        print "release"
        
    def mouseMoveEvent(self,event):
        #self.getMousePos()
        self.alist.append(event.pos())
        self.update()
        
    def paintEvent(self,event):
                
        painter = QPainter(self)
        painter.setPen(QPen(QColor(0,0,0),3))
        
        #painter.drawLine(self.alist)
        self.paintLines(painter, self.listlist)
        self.PaintALines(painter,self.alist)
        
    def paintLines(self,painter,listlists):
        for apointlist in listlists:
            for i in range(len(apointlist)):
                if i == len(apointlist)-1:
                    pass
                else:
                    painter.drawLine(apointlist[i],apointlist[i+1])
                    
    def PaintALines(self,painter,apointlist):
        for i in range(len(apointlist)):
            if i == len(apointlist)-1:
                pass
            else:
                painter.drawLine(apointlist[i],apointlist[i+1])
        
        
        
        

class MainWindow(QWidget):
    def __init__(self,parent=None):
        super(MainWindow,self).__init__(parent)
        self.setFixedSize(QSize(900,800))
        
        
        
        pe = QPalette(QColor(Qt.darkYellow))
        self.setPalette(pe)
        
        
        self.paintBoard = PaintBoard(self)
        self.paintBoard.move(QPoint(100,50))
        
        
        

if __name__ == "__main__":
    app = QApplication(sys.argv)
    c = MainWindow()
    c.show()
    
    app.exec_()