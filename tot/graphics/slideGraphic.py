#coding:utf-8
'''
Created on 2015��4��24��

@author: guowu
'''
from PyQt4.QtGui import QWidget, QLabel, QPixmap, QApplication, QPushButton
from PyQt4.QtCore import QPoint, SIGNAL, Qt, QTimeLine, QSize
import sys

class MyLabel(QLabel):
    def __init__(self,parent=None):
        super(MyLabel,self).__init__(parent)
        
        self.mousePress = False
        
    def mousePressEvent(self,event):
        self.mousePress = True
    def mouseReleaseEvent(self,event):
        self.mousePress = False
        self.emit(SIGNAL("clicked()"))
    def mouseMoveEvent(self,event):
        if self.mousePress == True:
            print event.pos()

class SlideGraphic(QWidget):
    def __init__(self,parent=None):
        super(SlideGraphic,self).__init__(parent)
        self.resize(QSize(3000,400))
        
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.setAttribute(Qt.WA_TranslucentBackground,True)
        
        self.piclist = ["images/brownies.jpg","images/chocolate.jpg","images/cookies.jpg","images/fish.jpg","images/mussels.jpg","images/pasta.jpg","images/pizza.jpg","images/puding.jpg","images/sushi.jpg","images/trouts.jpg"]
        
        self.labelList = []
        self.index = None
        self.initAllLabel()
        
        self.lineTimer = QTimeLine()
        self.lineTimer.setFrameRange(0,100)
        self.lineTimer.setDuration(500)
        
        self.connect(self.lineTimer, SIGNAL("frameChanged(int)"), self.updateGraphic)
        
    def initAllLabel(self):
        for i in range(len(self.piclist)):
            picLabel = MyLabel(self)
            picLabel.setObjectName(str(i))
            pixmap = QPixmap(self.piclist[i]).scaled(100,100)
            picLabel.setPixmap(pixmap)
            self.labelList.append(picLabel)
            
        for i in range(len(self.labelList)):
            self.labelList[i].move(QPoint(i*300+100,100))
            self.connect(self.labelList[i], SIGNAL("clicked()"),self.slotLabelClicked)
            
    def setCenterPoint(self,point):
        self.centerPointO = point
        
    def slotLabelClicked(self):
        if self.lineTimer.state() != QTimeLine.NotRunning:
            return
        currentLabel = self.sender()
        if self.index == currentLabel.objectName():
            return
        self.index = currentLabel.objectName()
        self.indexpos = self.labelList[int(self.index)].pos()
        self.indexcenter = self.pos() + self.labelList[int(self.index)].pos() + QPoint(50,50)
        #print self.indexcenter
        self.centerPoint = QPoint(self.centerPointO.x(),100)
        self.betPointDirect = self.indexcenter.x() - self.centerPoint.x()
        
        self.currentPos = self.pos()
        self.lastPoint = QPoint(self.pos().x() - self.betPointDirect ,self.pos().y())
        
        self.perFrameDic = float(-self.betPointDirect/100.000)
        
        #self.move(QPoint(self.pos().x() - self.betPointDirect ,self.pos().y()))
        print QPoint(self.pos().x() - self.betPointDirect ,self.pos().y())
        self.lineTimer.start()
        
    def likeLabelClicked(self,indexx):
        if self.lineTimer.state() != QTimeLine.NotRunning:
            return
        if self.index == indexx:
            return
        self.index = indexx
        self.indexpos = self.labelList[int(self.index)].pos()
        self.indexcenter = self.pos() + self.labelList[int(self.index)].pos() + QPoint(50,50)
        self.centerPoint = QPoint(self.centerPointO.x(),100)
        self.betPointDirect = self.indexcenter.x() - self.centerPoint.x()
        
        self.currentPos = self.pos()
        self.lastPoint = QPoint(self.pos().x() - self.betPointDirect ,self.pos().y())
        
        self.perFrameDic = float(-self.betPointDirect/100.000)
        
        #self.move(QPoint(self.pos().x() - self.betPointDirect ,self.pos().y()))
        print QPoint(self.pos().x() - self.betPointDirect ,self.pos().y())
        self.lineTimer.start()
        
    def updateGraphic(self):
        
        current = self.lineTimer.currentFrame()
        print current
        for i in range(len(self.piclist)):
            if i == int(self.index):
                self.labelList[i].resize(100+current,100+current)
                pixmap = QPixmap(self.piclist[i]).scaled(100+current,100+current)
                self.labelList[i].setPixmap(pixmap)
                self.labelList[i].move(QPoint(self.indexpos.x()-current/2,self.indexpos.y() - current/2))
            else:
                self.labelList[i].resize(100,100)
                pixmap = QPixmap(self.piclist[i]).scaled(100,100)
                self.labelList[i].setPixmap(pixmap)
                self.labelList[i].move(QPoint(i*300+100,100))
                
        #print QPoint(self.currentPos.x() + self.perFrameDic*(current),self.currentPos.y())
        self.move(QPoint(self.currentPos.x() + self.perFrameDic*(current),self.currentPos.y()))
    
"""
if __name__ == "__main__":
    app = QApplication(sys.argv)
    mainwindow = SlideGraphic()
    mainwindow.show()
    app.exec_()       
"""    
    