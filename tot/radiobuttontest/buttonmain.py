#coding:utf-8
'''
Created on 2014��8��26��

@author: GuoWu
'''
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from aeroButton import AeroButton
import sys

QTextCodec.setCodecForTr(QTextCodec.codecForName("utf8"))


class TestDialog(QDialog):
    def __init__(self,parent=None):
        super(TestDialog,self).__init__(parent)
        self.setFixedSize(400,450)
        self.setWindowFlags(Qt.FramelessWindowHint|Qt.WindowStaysOnTopHint)
        #self.setAttribute(Qt.WA_TranslucentBackground,True)
        self.setStyleSheet("QPushButton{border:5px;}"
                           )
        
        self.imgf = "images/0.png"
        self.imgs0 = "images/00.png"
        self.imgt0 = "images/000.png"
        self.imgs1 = "images/11.png"
        self.imgt1 = "images/111.png"
        self.imgs2 = "images/22.png"
        self.imgt2 = "images/222.png"
        self.imgs3 = "images/33.png"
        self.imgt3 = "images/333.png"
        self.imgs4 = "images/44.png"
        self.imgt4 = "images/444.png"
        self.imgs5 = "images/55.png"
        self.imgt5 = "images/555.png"
        self.imgs6 = "images/66.png"
        self.imgt6 = "images/666.png"
        self.imgs7 = "images/77.png"
        self.imgt7 = "images/777.png"
        self.imgs8 = "images/88.png"
        self.imgt8 = "images/888.png"
        self.imgs9 = "images/99.png"
        self.imgt9 = "images/999.png"
        self.imgs10 = "images/xx.png"
        self.imgt10 = "images/xxx.png"
        self.tipLabel = QLabel(u"圆形按钮",self)
        self.tipLabel.setFont(QFont("times",13,QFont.Bold))
        self.tipLabel.setStyleSheet("color:white;")
        
        self.closeButton = QPushButton(self)
        self.closeButton.setFixedSize(30,30)
        self.closeButton.setIcon(QIcon("images/0.png"))
        self.closeButton.setIconSize(QSize(30,30))
        self.connect(self.closeButton, SIGNAL("clicked()"),self.slotClose)
        
        self.myButton0 = AeroButton(self.imgf,self.imgs0,self.imgt0)
        self.myButton0.setMaximumSize(90,90)
        self.myButton0.setIconSize(QSize(90,90))
        self.myButton1 = AeroButton(self.imgf,self.imgs1,self.imgt1)
        self.myButton1.setMaximumSize(90,90)
        self.myButton1.setIconSize(QSize(90,90))
        self.myButton2 = AeroButton(self.imgf,self.imgs2,self.imgt2)
        self.myButton2.setMaximumSize(90,90)
        self.myButton2.setIconSize(QSize(90,90))
        self.myButton3 = AeroButton(self.imgf,self.imgs3,self.imgt3)
        self.myButton3.setMaximumSize(90,90)
        self.myButton3.setIconSize(QSize(90,90))
        self.myButton4 = AeroButton(self.imgf,self.imgs4,self.imgt4)
        self.myButton4.setMaximumSize(90,90)
        self.myButton4.setIconSize(QSize(90,90))
        self.myButton5 = AeroButton(self.imgf,self.imgs5,self.imgt5)
        self.myButton5.setMaximumSize(90,90)
        self.myButton5.setIconSize(QSize(90,90))
        self.myButton6 = AeroButton(self.imgf,self.imgs6,self.imgt6)
        self.myButton6.setMaximumSize(90,90)
        self.myButton6.setIconSize(QSize(90,90))
        self.myButton7 = AeroButton(self.imgf,self.imgs7,self.imgt7)
        self.myButton7.setMaximumSize(90,90)
        self.myButton7.setIconSize(QSize(90,90))
        self.myButton8 = AeroButton(self.imgf,self.imgs8,self.imgt8)
        self.myButton8.setMaximumSize(90,90)
        self.myButton8.setIconSize(QSize(90,90))
        self.myButton9 = AeroButton(self.imgf,self.imgs9,self.imgt9)
        self.myButton9.setMaximumSize(90,90)
        self.myButton9.setIconSize(QSize(90,90))
        self.myButton10 = AeroButton(self.imgf,self.imgs10,self.imgt10)
        self.myButton10.setMaximumSize(90,90)
        self.myButton10.setIconSize(QSize(90,90))
        
        myButtonGroup = QButtonGroup(self)
        myButtonGroup.addButton(self.myButton0,0)
        myButtonGroup.addButton(self.myButton1,1)
        myButtonGroup.addButton(self.myButton2,2)
        myButtonGroup.addButton(self.myButton3,3)
        myButtonGroup.addButton(self.myButton4,4)
        myButtonGroup.addButton(self.myButton5,5)
        myButtonGroup.addButton(self.myButton6,6)
        myButtonGroup.addButton(self.myButton7,7)
        myButtonGroup.addButton(self.myButton8,8)
        myButtonGroup.addButton(self.myButton9,9)
        myButtonGroup.addButton(self.myButton10,10)
        
        labellog = QLabel()
        self.labellogm = QLabel()
        labellogmoney = QLabel()
        labellogmoney.setMaximumSize(80,20)
        self.labellogm.setMaximumSize(60,15)
        labellog.setMaximumSize(60,15)
        lablemag = QGridLayout()
        labellogmoney.setText(u"人数:")
        labellog.setText(u"已登录:"+"\t"+"000")
        
        lablemag.addWidget(labellogmoney,0,0)
        lablemag.addWidget(self.labellogm,0,1)
        lablemag.addWidget(labellog,1,0)
        
        self.spaceLabel = QLabel()
        self.spaceLabel.setFixedHeight(20)
        buttonLayout = QGridLayout()
        buttonLayout.addWidget(self.spaceLabel,0,0,1,4)
        buttonLayout.addWidget(self.myButton0,1,0)
        buttonLayout.addWidget(self.myButton1,1,1)
        buttonLayout.addWidget(self.myButton2,1,2)
        buttonLayout.addWidget(self.myButton3,1,3)
        buttonLayout.addWidget(self.myButton4,2,0)
        buttonLayout.addWidget(self.myButton5,2,1)
        buttonLayout.addWidget(self.myButton6,2,2)
        buttonLayout.addWidget(self.myButton7,2,3)
        buttonLayout.addWidget(self.myButton8,3,0)
        buttonLayout.addWidget(self.myButton9,3,1)
        buttonLayout.addWidget(self.myButton10,3,2)
        buttonLayout.addLayout(lablemag,4,0,1,4)
        self.connect(myButtonGroup, SIGNAL("buttonClicked(int)"),self.buttonSlot)
        self.linePath()
        self.setLayout(buttonLayout)
        self.mousePressed = False
    def buttonSlot(self,num):
        a=1
        i=1
        if i==1:
            a=a+num*100
        elif i==2:
            a=a+num*10
        elif i==3:
            a=a+num
        i+=1
        self.labellogm.setText(QString.number(a))
        if num==10:
            self.close()
    def linePath(self):
        aPoint = QPoint(0,0)
        bPoint = QPoint(self.width()-1,0)
        cPoint = QPoint(self.width()-1,self.height()-1)
        dPoint = QPoint(0,self.height()-1)
        self.linePath = QPainterPath()
        self.linePath.moveTo(aPoint)
        self.linePath.lineTo(bPoint)
        self.linePath.moveTo(bPoint)
        self.linePath.lineTo(cPoint)
        self.linePath.moveTo(cPoint)
        self.linePath.lineTo(dPoint)
        self.linePath.moveTo(dPoint)
        self.linePath.lineTo(aPoint)
        
        self.linePath.closeSubpath()
    def slotClose(self):
        self.close()
    
    def mouseMoveEvent(self,event):
        if self.mousePressed:
            self.move(self.pos() + event.pos() - self.currentPos)
   
    def mousePressEvent(self,event):
        if event.buttons() == Qt.LeftButton:
            self.currentPos = event.pos()
            self.mousePressed = True
   
    def mouseReleaseEvent(self,event):
        if event.buttons() == Qt.LeftButton:
            self.mousePressed = False    
        
        
    def paintEvent(self,event):
        self.closeButton.move(self.width()-32,2)
        self.tipLabel.move(5,5)
        p = QPainter(self)
        pix = QPixmap("images/background.png")
        p.drawPixmap(self.rect(), pix)
        p.drawPath(self.linePath)
        
        painter = QPainter(self)
        linearGradient = QLinearGradient(0,0,0,35)
        linearGradient.setColorAt(0, QColor(60,100,200))
        linearGradient.setColorAt(0.1, QColor(6,88,200))
        linearGradient.setColorAt(1, QColor(10,100,100))
        painter.setBrush(QBrush(linearGradient))
        self.menuRect = QRect(0, 0, self.width(), 35)
        painter.fillRect(self.menuRect, QBrush(linearGradient))
        
        
app=QApplication(sys.argv)
dialog=TestDialog()
dialog.show()
app.exec_()