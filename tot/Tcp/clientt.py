# -*- coding: utf-8 -*-

from PyQt4.QtGui import *
from PyQt4.QtCore import *
import sys
from PyQt4.QtNetwork import *

QTextCodec.setCodecForTr(QTextCodec.codecForName("utf8"))

class TcpClient(QDialog):
    def __init__(self,parent=None):
        super(TcpClient,self).__init__(parent)
        self.setWindowTitle(u"client")
        
        
        self.status = False 
        self.serverIP = QHostAddress()
        self.port = 8090
        self.msglist = QByteArray()
        
        vbMain = QVBoxLayout()
        self.ListWidgetContent = QListWidget(self)
        vbMain.addWidget(self.ListWidgetContent)
        
        hb = QHBoxLayout()
        self.LineEditMessage = QLineEdit(self)
        hb.addWidget(self.LineEditMessage)
        self.PushButtonSend = QPushButton(self)
        self.PushButtonSend.setText(u"发送")
        self.PushButtonSend.setEnabled(False)
        hb.addWidget(self.PushButtonSend)
        self.connect(self.PushButtonSend,SIGNAL("clicked()"),self.slotSend)
        
        hb1 = QHBoxLayout()
        LabelName = QLabel(self)
        LabelName.setText(u"用户名:")
        self.LineEditUser = QLineEdit(self)
        self.LineEditUser.setText("gw")
        hb1.addWidget(LabelName)
        hb1.addWidget(self.LineEditUser) 
        
        hb2 = QHBoxLayout()
        LabelServerIP = QLabel(self)
        LabelServerIP.setText(u"服务器地址:")
        self.LineEditIP = QLineEdit(self)
        self.LineEditIP.setText("127.0.0.1")
        hb2.addWidget(LabelServerIP)
        hb2.addWidget(self.LineEditIP) 
        
        hb3 = QHBoxLayout()
        LabelPort = QLabel(self)
        LabelPort.setText(u"端口:")
        
        self.LineEditPort = QLineEdit(self)
        self.LineEditPort.setText(str(self.port))
        hb3.addWidget(LabelPort)
        hb3.addWidget(self.LineEditPort) 
        
        
        vbMain.addLayout(hb)
        vbMain.addLayout(hb1)
        vbMain.addLayout(hb2)
        vbMain.addLayout(hb3)
        
        self.PushButtonLeave = QPushButton(self)
        self.PushButtonLeave.setText(u"连接服务器")
        vbMain.addWidget(self.PushButtonLeave)
        
        self.buttonLayout()
        mainLayout = QHBoxLayout(self)
        mainLayout.addLayout(vbMain)
        mainLayout.addLayout(self.buttonLayout)
        
        self.connect(self.PushButtonLeave,SIGNAL("clicked()"),self.slotEnter)
        self.connect(self.buttonFirst, SIGNAL("clicked()"),self.slotChat)
        self.connect(self.buttonSecond, SIGNAL("clcked()"),self.slotFight)
    def buttonLayout(self):
        self.buttonFirst = QPushButton(u"私聊")
        self.buttonSecond = QPushButton(u"123")
        self.buttonLayout = QVBoxLayout()
        self.buttonLayout.addWidget(self.buttonFirst)  
        self.buttonLayout.addWidget(self.buttonSecond)
    def slotSend(self):
        msg = self.userName + ":" + self.LineEditMessage.text()
        length = self.tcpSocket.writeData(msg)
        self.LineEditMessage.clear() 
        if length != msg.length():
            return  
    def slotChat(self):
        msg = ":chatwithanother"+self.userName
        length = self.tcpSocket.writeData(msg)
        if length != msg.length():
            return 
    def slotFight(self):
        pass
        
    def slotEnter(self):
        if not self.status:
            ip = self.LineEditIP.text()
            if not self.serverIP.setAddress(ip):
                QMessageBox.information(self,u"error",u"server ip address error!")
                return
            if self.LineEditUser.text() == "":
                QMessageBox.information(self,u"error",u"User Name error!")
                return
            self.userName = self.LineEditUser.text()
            
            self.tcpSocket = QTcpSocket(self)
            
            self.connect(self.tcpSocket,SIGNAL("connected()"),self.slotConnected)
            self.connect(self.tcpSocket,SIGNAL("disconnected()"),self.slotDisconnected)
            self.connect(self.tcpSocket,SIGNAL("readyRead()"),self.dataReceived) 
                       
            self.tcpSocket.connectToHost(self.serverIP.toString(),self.port)
            self.status = True
        else:
            msg = self.userName + ":" + u"disconnect server"
            length = self.tcpSocket.writeData(msg)
            if length != msg.length():
                return              
            self.tcpSocket.disconnectFromHost()
            self.status = False
    def slotConnected(self):
        self.PushButtonSend.setEnabled(True)
        self.PushButtonLeave.setText(u"断开服务器")
        msg = self.userName + ":" + u"connect to server"
        length = self.tcpSocket.writeData(msg)
        if length != msg.length():
            return
    def slotDisconnected(self):
        self.PushButtonSend.setEnabled(False)
        self.PushButtonLeave.setText(u"连接服务器")
    def dataReceived(self):
        while self.tcpSocket.bytesAvailable() > 0:
            length = self.tcpSocket.bytesAvailable()
            msg = QString(self.tcpSocket.read(length))
            self.ListWidgetContent.addItem(msg)
            
            
        
app=QApplication(sys.argv)
dialog=TcpClient()
dialog.show()
app.exec_()
