# -*- coding: utf-8 -*-  
from PyQt4.QtGui import *
from PyQt4.QtCore import *
import sys

QTextCodec.setCodecForTr(QTextCodec.codecForName("utf8"))


class TestWidget(QWidget):
    def __init__(self,parent=None):
        super(TestWidget,self).__init__(parent)
        self.setFixedSize(346,346)#设置大小
        self.setWindowFlags(Qt.FramelessWindowHint)#设置无边框
        self.setStyleSheet(
                           "QPushButton{background:red;}"
                           "QPushButton::hover{background:white;}"
                           "QPushButton::pressed{background:yellow;}"
                           
                           )
        
        
        
        self.button1 = QPushButton()
        self.button1.setFixedSize(100,100)
        self.button1.setObjectName("1")
        self.button2 = QPushButton()
        self.button2.setFixedSize(100,100)
        self.button2.setObjectName("2")
        self.button3 = QPushButton()
        self.button3.setFixedSize(100,100)
        self.button3.setObjectName("3")
        self.button4 = QPushButton()
        self.button4.setFixedSize(100,100)
        self.button4.setObjectName("4")
        self.button5 = QPushButton()
        self.button5.setFixedSize(100,100)
        self.button5.setObjectName("5")
        self.button6 = QPushButton()
        self.button6.setFixedSize(100,100)
        self.button6.setObjectName("6")
        self.button7 = QPushButton()
        self.button7.setFixedSize(100,100)
        self.button7.setObjectName("7")
        self.button8 = QPushButton()
        self.button8.setFixedSize(100,100)
        self.button8.setObjectName("8")
        self.button9 = QPushButton()
        self.button9.setFixedSize(100,100)
        self.button9.setObjectName("9")
        
        mainLayout = QGridLayout()
        mainLayout.setSpacing(2)
        mainLayout.addWidget(self.button1,0,0)
        mainLayout.addWidget(self.button2,0,1)
        mainLayout.addWidget(self.button3,0,2)
        mainLayout.addWidget(self.button4,1,0)
        mainLayout.addWidget(self.button5,1,1)
        mainLayout.addWidget(self.button6,1,2)
        mainLayout.addWidget(self.button7,2,0)
        mainLayout.addWidget(self.button8,2,1)
        mainLayout.addWidget(self.button9,2,2)
        mainLayout.setMargin(20)
        self.setLayout(mainLayout)
        
        self.buttonList = [self.button1,self.button2,self.button3,self.button4,self.button5,self.button6,self.button7,self.button8,self.button9]
       
        self.buttonList[4].setIcon(QIcon("dsaf.png"))
        self.buttonList[4].setIconSize(QSize(80,80))
        
        self.fList = [0,0,0,0,1,0,0,0,0]
        
        
        
        
        self.connect(self.button1,SIGNAL("clicked()"),self.slotEmit)
        self.connect(self.button2,SIGNAL("clicked()"),self.slotEmit)
        self.connect(self.button3,SIGNAL("clicked()"),self.slotEmit)
        self.connect(self.button4,SIGNAL("clicked()"),self.slotEmit)
        self.connect(self.button5,SIGNAL("clicked()"),self.slotEmit)
        self.connect(self.button6,SIGNAL("clicked()"),self.slotEmit)
        self.connect(self.button7,SIGNAL("clicked()"),self.slotEmit)
        self.connect(self.button8,SIGNAL("clicked()"),self.slotEmit)
        self.connect(self.button9,SIGNAL("clicked()"),self.slotEmit)
    def slotEmit(self):
        if self.sender().objectName() == "1" :
            self.emit(SIGNAL("paintButton"), "showFirst")
        elif self.sender().objectName() == "2":
            self.emit(SIGNAL("paintButton"), "showSecond")
        elif self.sender().objectName() == "3":
            self.emit(SIGNAL("paintButton"), "showThird")
        elif self.sender().objectName() == "4":
            self.emit(SIGNAL("paintButton"), "showFour")
        elif self.sender().objectName() == "5":
            self.emit(SIGNAL("paintButton"), "showFive")
        elif self.sender().objectName() == "6":
            self.emit(SIGNAL("paintButton"), "showSix")
        elif self.sender().objectName() == "7":
            self.emit(SIGNAL("paintButton"), "showSeven")
        elif self.sender().objectName() == "8":
            self.emit(SIGNAL("paintButton"), "showEight")
        elif self.sender().objectName() == "9":
            self.emit(SIGNAL("paintButton"), "showNine")
    
            
        
        