#coding:utf-8
'''
Created on 2014��8��30��

@author: GuoWu
'''
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from dropmenu import ButtonMenu
import sys
PADDING = 2
class FrameHintDialog(QWidget):
    def __init__(self,parent=None):
        super(FrameHintDialog,self).__init__(parent)
        self.isLeftPressDown = False
        #self.dir = enum(UP=0,DOWN=1,LEFT=3,RIGHT=4,LEFTTOP=5,LEFTBOTTOM=6,RIGHTBOTTOM=7,RIGHTTOP=8,NONE=9)
        self.dir = None
        self.resize(500,500)
        self.setMinimumHeight(100)
        self.setMinimumWidth(150)
        self.setWindowFlags(Qt.FramelessWindowHint|Qt.WindowSystemMenuHint )
        self.setMouseTracking(True)
        self.count = 0
        self.setStyleSheet(
                           "QPushButton{border:1px;}"
                           "QPushButton:hover{background:blue;}"
                           "QPushButton:pressed{background:black;}")
        
        self.buttonMenu = ButtonMenu()
        
        self.menuButton = QPushButton(self)
        self.menuButton.setFixedSize(30,30)
        self.menuButton.setIcon(QIcon("dropArrow.png"))
        self.menuButton.setIconSize(QSize(25,25))
        
        self.miniButton = QPushButton(self)
        self.miniButton.setFixedSize(30,30)
        self.miniButton.setIcon(QIcon("mini.png"))
        self.miniButton.setIconSize(QSize(25,25))
        
        self.maxButton = QPushButton(self)
        self.maxButton.setFixedSize(30,30)
        self.maxButton.setIcon(QIcon("max.png"))
        self.maxButton.setIconSize(QSize(25,25))
        
        self.closeButton = QPushButton(self)
        self.closeButton.setFixedSize(30,30)
        self.closeButton.setIcon(QIcon("close.png"))
        self.closeButton.setIconSize(QSize(25,25))
        
        self.pixmovie = QMovie("b.gif")
        self.label = QLabel(self)
        #self.label.setFixedSize(300,300)
        self.label.setMovie(self.pixmovie)
        self.pixmovie.start()
        
        self.connect(self.menuButton, SIGNAL("clicked()"),self.showMenu)
        self.connect(self.miniButton, SIGNAL("clicked()"),self.dlgMini)
        self.connect(self.maxButton, SIGNAL("clicked()"),self.dlgMax)
        self.connect(self.closeButton, SIGNAL("clicked()"),self.closeDlg)
    def dlgMini(self):
        self.showMinimized()
        
    def dlgMax(self):
        self.count+=1
        if self.count%2 != 0:
            self.showMaximized()
            self.maxButton.setIcon(QIcon("to.png"))
            self.maxButton.setIconSize(QSize(25,25))
        else:
            desktop = QApplication.desktop()
            self.showNormal()
            self.maxButton.setIcon(QIcon("max.png"))
            self.maxButton.setIconSize(QSize(25,25))
            self.move((desktop.width()-self.width())/2,(desktop.height()-self.height())/2)
    def closeDlg(self):
        self.close()
    def showMenu(self):
        print "clicked"
        print QString().number(self.pos().x())
        self.buttonMenu.contextMenu.show()
        self.buttonMenu.contextMenu.move(self.pos().x()+self.width()-120,self.pos().y()+40)
        
        
    def mouseReleaseEvent(self,event):

        if(event.button() == Qt.LeftButton):
            self.isLeftPressDown = False
            if(self.dir != None):
                self.releaseMouse();
                self.setCursor(QCursor(Qt.ArrowCursor))
    def mousePressEvent(self,event):
        if event.button() == Qt.LeftButton:
            self.isLeftPressDown = True
            if(self.dir != None):
                self.mouseGrabber()
            else:
                self.dragPosition = event.globalPos() - self.frameGeometry().topLeft()
        elif event.button() == Qt.RightButton:
            #self.close()
            pass
        QWidget.mousePressEvent(self,event)
    def mouseMoveEvent(self,event):
        gloPoint = QPoint(event.globalPos())
        rect = QRect(self.rect())
        tl = QPoint(self.mapToGlobal(rect.topLeft()))
        rb = QPoint(self.mapToGlobal(rect.bottomRight()))
    
        if(not self.isLeftPressDown):
            self.region(gloPoint)
        else:
            if(self.dir != None):
                rMove = QRect(tl, rb)
                if self.dir == "LEFT":
                    if(rb.x() - gloPoint.x() <= self.minimumWidth()):
                        rMove.setX(tl.x())
                    else:
                        rMove.setX(gloPoint.x())
                elif self.dir == "RIGHT":
                    rMove.setWidth(gloPoint.x() - tl.x())
                elif self.dir == "UP":
                    if(rb.y() - gloPoint.y() <= self.minimumHeight()):
                        rMove.setY(tl.y())
                    else:
                        rMove.setY(gloPoint.y())
                elif self.dir == "DOWN":
                    rMove.setHeight(gloPoint.y() - tl.y())
                elif self.dir == "LEFTTOP":
                    if(rb.x() - gloPoint.x() <= self.minimumWidth()):
                        rMove.setX(tl.x())
                    else:
                        rMove.setX(gloPoint.x());
                    if(rb.y() - gloPoint.y() <= self.minimumHeight()):
                        rMove.setY(tl.y())
                    else:
                        rMove.setY(gloPoint.y())
                elif self.dir == "RIGHTTOP":
                    rMove.setWidth(gloPoint.x() - tl.x())
                    rMove.setY(gloPoint.y())
                elif self.dir == "LEFTBOTTOM":
                    rMove.setX(gloPoint.x())
                    rMove.setHeight(gloPoint.y() - tl.y())
                elif self.dir == "RIGHTBOTTOM":
                    rMove.setWidth(gloPoint.x() - tl.x())
                    rMove.setHeight(gloPoint.y() - tl.y())
                self.setGeometry(rMove)
            else:
                self.move(event.globalPos() - self.dragPosition)
                event.accept()
        QWidget.mouseMoveEvent(self,event)
        
        
    def region(self,cursorGlobalPoint):
        rect = QRect(self.rect())
        tl = QPoint(self.mapToGlobal(rect.topLeft()))
        rb = QPoint(self.mapToGlobal(rect.bottomRight()))
        x = int(cursorGlobalPoint.x())
        y = int(cursorGlobalPoint.y())
    
        if(tl.x() + PADDING >= x and tl.x() <= x and tl.y() + PADDING >= y and tl.y() <= y):
            self.dir = "LEFTTOP"
            self.setCursor(QCursor(Qt.SizeFDiagCursor))
        elif(x >= rb.x() - PADDING and x <= rb.x() and y >= rb.y() - PADDING and y <= rb.y()):
            
            self.dir = "RIGHTBOTTOM"
            self.setCursor(QCursor(Qt.SizeFDiagCursor))
        elif(x <= tl.x() + PADDING and x >= tl.x() and y >= rb.y() - PADDING and y <= rb.y()):
            self.dir = "LEFTBOTTOM"
            self.setCursor(QCursor(Qt.SizeBDiagCursor))
            
        elif(x <= rb.x() and x >= rb.x() - PADDING and y >= tl.y() and y <= tl.y() + PADDING):
            self.dir = "RIGHTTOP"
            self.setCursor(QCursor(Qt.SizeBDiagCursor))
        elif(x <= tl.x() + PADDING and x >= tl.x()):
            self.dir = "LEFT"
            self.setCursor(QCursor(Qt.SizeHorCursor))
        elif( x <= rb.x() and x >= rb.x() - PADDING):
            self.dir = "RIGHT"
            self.setCursor(QCursor(Qt.SizeHorCursor))
        elif(y >= tl.y() and y <= tl.y() + PADDING):
            self.dir = "UP"
            self.setCursor(QCursor(Qt.SizeVerCursor))
        elif(y <= rb.y() and y >= rb.y() - PADDING):
            self.dir = "DOWN"
            self.setCursor(QCursor(Qt.SizeVerCursor))
        else:
            self.dir = None
            self.setCursor(QCursor(Qt.ArrowCursor))
    def paintEvent(self,event):
        self.menuButton.move(self.width()-120,0)
        self.miniButton.move(self.width()-90,0)
        self.maxButton.move(self.width()-60,0)
        self.closeButton.move(self.width()-30,0)
        self.label.move((self.width()-self.label.width()-2),(self.height()-self.label.height()-2))
        
        p = QPainter(self)
        pix = QPixmap("background.png")
        p.drawPixmap(QRect(0,60,self.width(),self.height()), pix)
        
        painter = QPainter(self)
        linearGradient = QLinearGradient(1,1,1,70)
        linearGradient.setColorAt(0, QColor(160,250,250))
        linearGradient.setColorAt(0.6, QColor(160,188,200))
        linearGradient.setColorAt(1, QColor(210,200,200))
        painter.setBrush(QBrush(linearGradient))
        self.menuRect = QRect(1, 1, self.width(), 70)
        painter.fillRect(self.menuRect, QBrush(linearGradient))
        aPoint = QPoint(0,0)
        bPoint = QPoint(self.width()-1,0)
        cPoint = QPoint(self.width()-1,self.height()-1)
        dPoint = QPoint(0,self.height()-1)
        self.linePath = QPainterPath()
        self.linePath.moveTo(aPoint)
        self.linePath.lineTo(bPoint)
        self.linePath.moveTo(bPoint)
        self.linePath.lineTo(cPoint)
        self.linePath.moveTo(cPoint)
        self.linePath.lineTo(dPoint)
        self.linePath.moveTo(dPoint)
        self.linePath.lineTo(aPoint)
        self.linePath.closeSubpath()
        p.drawPath(self.linePath)
        
        
app = QApplication(sys.argv)
c = FrameHintDialog()
c.show()
app.exec_()