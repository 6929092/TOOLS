#coding:utf-8
'''
Created on 2014骞�8鏈�26鏃�
@author: GuoWu
'''
from PyQt4.QtGui import QPushButton,QIcon
from PyQt4.QtCore import QSize

class AeroButton(QPushButton):
    def __init__(self,a,b,c,parent=None):
        super(AeroButton,self).__init__(parent)
        #self.setEnabled(True)
        self.a = a
        self.b = b
        self.setIcon(QIcon(self.a))
        self.setIconSize(self.iconSize())
        self.count = 0
    def enterEvent(self,event):
        self.hovered = True
        
        #self.paintevent()
        #QPushButton.mouseReleaseEvent(self,event)
    def leaveEvent(self,event):
        self.hovered = False
        #self.paintevent()
        #QPushButton.mouseReleaseEvent(self,event)
    def mousePressEvent(self,event):
        self.pressed = True
        if self.count%2 == 0:
            self.setIcon(QIcon(self.b))
            self.setIconSize(self.iconSize())
        else:
            self.setIcon(QIcon(self.a))
            self.setIconSize(self.iconSize())
        self.count+=1
        #QPushButton.mouseReleaseEvent(self,event)
    def mouseReleaseEvent(self,event):

        self.pressed = False
        #self.paintevent()
        #QPushButton.mouseReleaseEvent(self,event)
    def setChecked(self,boolflag):
        if boolflag:
            self.setIcon(QIcon(self.a))
            self.setIconSize(self.iconSize())
            self.count = 0
        else:
            self.setIcon(QIcon(self.b))
            self.setIconSize(self.iconSize())
            self.count = 1