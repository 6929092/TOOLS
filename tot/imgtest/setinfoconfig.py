#coding:utf-8
from PyQt4.QtCore import QObject
import os
from configparser import ConfigFileParser

class SetInfoConfig(QObject):
    _instance = 0 
    def __init__(self):
        super(SetInfoConfig,self).__init__()
        self.parser = ConfigFileParser().instance()
        
    @classmethod 
    def instance(cls):
        if (cls._instance == 0):
            cls._instance = SetInfoConfig()
            
        return cls._instance

    def setConfigFile(self, section, option, value):
        if self.parser.has_section(section):
            if self.parser.has_option(section, option):
                self.parser.changeSectionValue(section, option, value)
            else:
                self.parser.addOption(section, option, value)
        else:
            self.parser.addSection(section, option, value)
            
    def getConfigFile(self, section, option):
        if self.parser.has_section(section):
            if self.parser.has_option(section, option):
                return self.parser.getValue(section, option)
        return None
    