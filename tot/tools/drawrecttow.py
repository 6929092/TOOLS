#coding:utf-8
'''
Created on 2015��4��16��

@author: guowu
'''
from PyQt4.QtGui import QWidget, QMovie, QLabel, QPainter, QFont, QColor, QPen,\
    QConicalGradient, QApplication, QRadialGradient, QGradient, QLinearGradient,\
    QPolygon, QHBoxLayout
from PyQt4.QtCore import QPoint, QTimer, SIGNAL, Qt, QTime
import sys
import time
class MyMainWindow(QWidget): 
    def __init__(self,parent=None):
        super(MyMainWindow,self).__init__(parent)
        self.resize(160,160)
        self.enterPose = False
        self.moveFlag = False
        self.mousePressed = False
        self.setWindowFlags(Qt.FramelessWindowHint|Qt.WindowStaysOnTopHint|Qt.SubWindow ) 
        
        self.type = "second"
        self.currentTime = 0
        self.timeTimer = QTimer()
        self.timeTimer.timeout.connect(self.update)
        self.timeTimer.start(10)
        
    def setTimeType(self,type):
        if type == "second":
            self.type = "second"
        elif type == "minute":
            self.type = "minute"
        elif type == "hour":
            self.type = "hour"
        
    def resizeEvent(self,event):
        self.side = self.width()
        self.update()
    def setSize(self,size):
        self.resize(size,size)
        self.side = self.width()
    
    def paintEvent(self,event):
        if self.type == "second":
            self.currentTime = QTime.currentTime().second()
        elif self.type == "minute":
            self.currentTime = QTime.currentTime().minute()
        elif self.type == "hour":
            self.currentTime = QTime.currentTime().hour()
        
        paint=QPainter(self)
        paint.setRenderHint(QPainter.Antialiasing, True)
        self.paintCircle(paint)
        
        #paint.scale(self.width()/200.0, self.width()/200.0)#使画面可以随着界面的变化可缩放
        
        font = QFont()
        font.setPointSize(self.width()/3)
        font.setFamily("Roman times")
        paint.setFont(font)
        if self.enterPose:
            paint.setPen(QPen(QColor(0,0,0,200),2))
        else:
            paint.setPen(QPen(QColor(0,0,0,255),2))
        paint.drawText(0, 0, self.width(), self.width(), Qt.AlignCenter, str(self.currentTime))
        
        
    def paintCircle(self,painter):

        coneGradient = QLinearGradient(0, 0, 0, self.side/4)
        coneGradient.setColorAt(0.0, QColor(0,0,0))
        coneGradient.setColorAt(0.3, QColor(100,100,100))
        coneGradient.setColorAt(1.0, QColor(255,205,205))
        painter.setBrush(coneGradient)            
        painter.setPen(QPen(QColor(100,211,255),1))
        painter.drawPolygon(QPolygon([QPoint(0, 0),QPoint(self.side, 0), QPoint(self.side/4*3, self.side/4), QPoint(self.side/4, self.side/4)]))
        
        coneGradient = QLinearGradient(0, 0, 40, 0)
        coneGradient.setColorAt(0.0, QColor(0,0,0))
        coneGradient.setColorAt(0.3, QColor(100,100,100))
        coneGradient.setColorAt(1.0, QColor(255,205,205))
        painter.setBrush(coneGradient)            
        painter.setPen(QPen(QColor(100,211,255),1))
        painter.drawPolygon(QPolygon([QPoint(0, 0),QPoint(self.side/4, self.side/4), QPoint(self.side/4, self.side/4*3), QPoint(0, self.side)]))
        
        coneGradient = QLinearGradient(0, self.side, 0, self.side/4*3)
        coneGradient.setColorAt(0.0, QColor(0,0,0))
        coneGradient.setColorAt(0.3, QColor(100,100,100))
        coneGradient.setColorAt(1.0, QColor(255,205,205))
        painter.setBrush(coneGradient)            
        painter.setPen(QPen(QColor(100,211,255),1))
        painter.drawPolygon(QPolygon([QPoint(0, self.side),QPoint(self.side/4, self.side/4*3), QPoint(self.side/4*3, self.side/4*3), QPoint(self.side, self.side)]))
         
        coneGradient = QLinearGradient(self.side, 0, self.side/4*3, 0)
        coneGradient.setColorAt(0.0, QColor(0,0,0))
        coneGradient.setColorAt(0.3, QColor(100,100,100))
        coneGradient.setColorAt(1.0, QColor(255,205,205))
        painter.setBrush(coneGradient)            
        painter.setPen(QPen(QColor(100,211,255),1))
        painter.drawPolygon(QPolygon([QPoint(self.side/4*3, self.side/4*3),QPoint(self.side, self.side), QPoint(self.side, 0), QPoint(self.side/4*3,self.side/4)]))
        
        
    def mouseMoveEvent(self,event):
        if self.mousePressed:
            self.move(self.pos() + event.pos() - self.currentPos)
            self.moveFlag = True
            pass
   
    def mousePressEvent(self,event):
        print "press"
        self.emit(SIGNAL("mousepressed"),event.pos())
        print event.buttons()
        if event.buttons() == Qt.LeftButton:
            self.currentPos = event.pos()
            print "444444"
            self.mousePressed = True
   
    def mouseReleaseEvent(self,event):
        self.emit(SIGNAL("mousereleased"))
        if self.mousePressed:
            self.mousePressed = False
        else:
            pass
        
    def enterEvent(self, event):
        self.enterPose = True
        self.textPose = True
        self.emit(SIGNAL("enterwidget"))
    
    def leaveEvent(self, event):
        self.enterPose = False
        self.textPose = False
        self.emit(SIGNAL("leavewidget"))
        
class MainWindow(QWidget):
    def __init__(self, parent=None):
        super(MainWindow,self).__init__(parent)
        self.resize(500,500)
        
        self.secondWidget = MyMainWindow()
        self.secondWidget.setTimeType("second")
        self.secondWidget.setSize(100)
        self.minuteWidget = MyMainWindow()
        self.minuteWidget.setTimeType("minute")
        self.minuteWidget.setSize(100)
        self.hourWidget = MyMainWindow()
        self.hourWidget.setTimeType("hour")
        self.hourWidget.setSize(100)
        
        self.hLayout = QHBoxLayout()
        self.hLayout.setMargin(0)
        self.hLayout.setSpacing(0)
        #self.hLayout.setAlignment(Qt.AlignCenter)
        self.hLayout.addWidget(self.hourWidget)
        self.hLayout.addWidget(self.minuteWidget)
        self.hLayout.addWidget(self.secondWidget)
        
        self.setLayout(self.hLayout)
        

if __name__ == "__main__":
    app = QApplication(sys.argv)
    c = MyMainWindow()
    c.show()
    app.exec_()