#coding:utf-8
'''
Created on 2015��3��17��

@author: guowu
'''
from PyQt4.QtGui import QWidget, QApplication, QPainter, QBrush, QPen, QLabel,\
    QPixmap, QColor, QMovie, QConicalGradient, QFont
from PyQt4.QtCore import QObject, Qt, QTimer, SIGNAL, QPoint, QString, QEvent
import sys
class MyMainWindow(QWidget): 
    def __init__(self,parent=None):
        super(MyMainWindow,self).__init__(parent)
        self.setFixedSize(200,200)
        self.mousePressed = False
        self.moveFlag = False
        self.pixmovie = QMovie("a.gif")
        self.pixlabel = QLabel(self)
        self.pixlabel.setFixedSize(160,100)
        self.pixlabel.setMovie(self.pixmovie)
        self.pixmovie.start()
        self.pixlabel.move(QPoint((self.width()-self.pixlabel.width())/2,(self.height() - self.pixlabel.height())/2))
        self.pixlabel.hide()
        self.moveHorseTimer = QTimer()
        self.connect(self.moveHorseTimer, SIGNAL("timeout()"), self.updateHorse)
        self.startCount = 0
        self.enterPose = False
        self.textPose = False
        self.text = 87
        self.radio = 5760/100*self.text
        
#         self.textLabel = QLabel(self)
#         self.setStyleSheet("QLabel{background:rgb(0,0,0,0);}"
#                            "QLabel{color:rgb(255,255,255,250);}"
#                            "QLabel:hover{color:rgb(255,255,255,120);}")
#         self.textLabel.setFixedSize(200,200)
#         self.textLabel.setFont(QFont("Roman times",self.width()/3,QFont.Bold))
#         self.textLabel.setAlignment(Qt.AlignCenter)
#         self.textLabel.move(QPoint((self.width()-self.textLabel.width())/2,(self.height() - self.textLabel.height())/2))
#         self.textLabel.setText(str(89))
        
    def updateHorse(self):

        self.startCount += 10
        if self.startCount == 200:
            self.startCount = 0
            self.moveHorseTimer.stop()
            self.emit(SIGNAL("starthorse"))
        
    def paintEvent(self,event):
    
        paint=QPainter(self)
        paint.setRenderHint(QPainter.Antialiasing, True)
        self.paintCircle(paint)
        
        font = QFont()
        font.setPointSize(70)
        font.setFamily("Roman times")
        paint.setFont(font)
        if self.enterPose:
            paint.setPen(QPen(QColor(255,255,255,100),2))
        else:
            paint.setPen(QPen(QColor(255,255,255,255),2))
        paint.drawText(0, 0, self.width() - 30, self.width(), Qt.AlignCenter, str(self.text))
        
        font.setPointSize(35)
        paint.setFont(font)
        if self.enterPose:
            paint.setPen(QPen(QColor(255,255,255,100),2))
        else:
            paint.setPen(QPen(QColor(255,255,255,255),2))
        paint.drawText(40, 15, self.width(), self.width(), Qt.AlignCenter, "%")
        
        self.pixlabel.move(QPoint((self.width()-self.pixlabel.width())/2 - self.startCount,(self.height() - self.pixlabel.height())/2))
        
        
        paint.setPen(QPen(Qt.lightGray,4,Qt.SolidLine))
        paint.setBrush(Qt.NoBrush)#���û�ˢ��ʽ 
        #paint.drawPie(10,10,self.width()-20,self.width()-20,0,self.radio)
        paint.drawArc(20,20,self.width()-40,self.width()-40,0,self.radio)
        
    def paintCircle(self,painter):
#         
        width = self.width()
        niceBlue = QColor(150, 150, 200)
        coneGradient = QConicalGradient(width/2, width/2, 0)#圆心，开始角度
        coneGradient.setColorAt(0.0, Qt.darkBlue)
        coneGradient.setColorAt(0.2, niceBlue)
        coneGradient.setColorAt(0.5, Qt.white)
        coneGradient.setColorAt(1.0, Qt.darkBlue)
        painter.setBrush(coneGradient)            
        painter.setPen(Qt.NoPen)
        painter.drawEllipse(0,0,width,width)#左上角坐标，直径
        
    def mouseMoveEvent_(self,event):
        if self.mousePressed:
            #self.move(self.pos() + event.pos() - self.currentPos)
            self.moveFlag = True
            pass
   
    def mousePressEvent(self,event):
        print "press"
        self.emit(SIGNAL("mousepressed"),event.pos())
        if event.buttons() == Qt.LeftButton:
            self.currentPos = event.pos()
            self.mousePressed = True
   
    def mouseReleaseEvent(self,event):
        self.emit(SIGNAL("mousereleased"))
        if self.mousePressed:
            self.mousePressed = False
            if not self.moveFlag:
                #self.moveFlag = False
                self.moveHorseTimer.start(40)   
        else:
            pass
        
    def enterEvent(self, event):
        self.enterPose = True
        self.textPose = True
        self.pixlabel.show()
        self.emit(SIGNAL("enterwidget"))
    
    def leaveEvent(self, event):
        self.enterPose = False
        self.textPose = False
        self.pixlabel.hide()
        self.emit(SIGNAL("leavewidget"))
        
class PMainWindow(QWidget):
    def __init__(self,parent=None):
        super(PMainWindow,self).__init__(parent)
        self.circleLabel = MyMainWindow(self)
        #self.setWindowFlags(Qt.FramelessWindowHint|Qt.WindowStaysOnTopHint)
        #self.setAttribute(Qt.WA_TranslucentBackground,True)
        self.desktop = QApplication.desktop()
        #print desktop.width(),desktop.height()
        self.setFixedSize(400,400)
        self.circleLabel.move(QPoint((self.width() - self.circleLabel.width())/2,(self.height() - self.circleLabel.height())/2))
        #圆心：（250，250））
        #半径： 50：：radio
        
        #250-radio，radio*2
        
        
        #self.pixlabel.move(QPoint((self.width() - self.circleLabel.width())/2,(self.height() - self.circleLabel.height())/2))
        
        self.paintTimer = QTimer()
        self.count = 0
        self.mousePressed = False
        self.radio = self.circleLabel.width()/2 - 10
        
        self.connect(self.circleLabel, SIGNAL("enterwidget"),self.startTimer)
        self.connect(self.circleLabel, SIGNAL("leavewidget"),self.stopTimer)
        self.connect(self.circleLabel, SIGNAL("mousepressed"),self.setMousePressed)
        self.connect(self.circleLabel, SIGNAL("mousereleased"),self.setMouseReleased)
        self.connect(self.circleLabel, SIGNAL("starthorse"),self.startHorse)
        self.connect(self.paintTimer, SIGNAL("timeout()"), self.startPaintCircle)
    def startHorse(self):
        self.emit(SIGNAL("starthorse"))
    
    def setMousePressed(self, pos):
        self.mousePressed = True
        x = pos.x() + (self.width() - self.circleLabel.width())/2
        y = pos.y() + (self.height() - self.circleLabel.height())/2
        self.currentPos = QPoint(x,y)
        self.circleLabel.moveFlag = False
        
        self.emit(SIGNAL("mousepressed"))
        #self.circleLabel.pixlabel.move(500,0)
        
    def setMouseReleased(self):
        self.emit(SIGNAL("moveover"))
        self.mousePressed = False
    
    def mouseMoveEvent(self,event):
        if self.mousePressed:
            self.circleLabel.moveFlag = True
            pos = self.pos() + event.pos() - self.currentPos
            x = pos.x()
            y = pos.y()
            print pos
            print x,y
            if x < -100 or x > self.desktop.width() - 300 or y < -100 or y > self.desktop.height() - 300:
                return
            self.move(pos)
    
    def mousePressEvent(self,event):
        if event.buttons() == Qt.LeftButton:
            #self.currentPos = event.pos()
            self.mousePressed = True
    
    def mouseReleaseEvent(self,event):
        if event.buttons() == Qt.LeftButton:
            #self.circleLabel.moveFlag = False
            self.mousePressed = False  
        
    def startPaintCircle(self):
        #print "start painter"
        self.radio+=self.count*1
        self.repaint()
        self.count+=1
        
        if self.count == 10:
            self.count = 0
            self.radio = self.circleLabel.width()/2 - 10
         
    def startTimer(self):
        self.paintTimer.start(100)
    def stopTimer(self):
        self.paintTimer.stop()
        self.count = 0
        self.radio = self.circleLabel.width()/2 - 10
        self.repaint()
    
    def paintEvent(self,event):
        painter = QPainter(self)
        painter.setRenderHint(QPainter.Antialiasing, True)
        self.paintCircle(painter, self.radio)
    
    def paintCircle(self,painter,radio):

        color = QColor(5,100,0,50 - 5*(self.count))
        painter.setPen(QPen(color,8, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin))
        painter.setBrush(Qt.NoBrush)
        painter.drawEllipse(self.width()/2-self.radio,self.height()/2-self.radio,self.radio*2,self.radio*2)
        
class HeightMama(QWidget):
    def __init__(self,parent=None):
        super(HeightMama,self).__init__(parent)
        
        desktop = QApplication.desktop()
        print desktop.width(),desktop.height()
        self.setFixedSize(desktop.width(),200)
        #self.setAttribute(Qt.WA_TranslucentBackground,True)
        
        #self.setMouseTracking(not self.hasMouseTracking())
        
        self.pixmovie = QMovie("a.gif")
        self.pixlabel = QLabel(self)
        self.pixlabel.setFixedSize(160,100)
        self.pixlabel.setMovie(self.pixmovie)
        self.pixmovie.start()
        
        self.pixlabel.move(self.width(),(self.height() - self.pixlabel.height())/2)
        
        self.timer = QTimer(self)
        self.connect(self.timer,SIGNAL("timeout()"),self.updatePixLabel)
        #self.timer.start(10)
        self.count = 0
        
    def updatePixLabel(self):
        self.count+=6
        print self.count
        self.pixlabel.move(self.width()-self.count,(self.height() - self.pixlabel.height())/2)
        if self.count >= self.width() + 160:
            print "stop1111111111111111111111"
            self.count = 0
            self.timer.stop()
            self.pixlabel.move(self.width(),(self.height() - self.pixlabel.width())/2)
            self.emit(SIGNAL("stophorse"))
    
        
class EMainWindow(QWidget):
    def __init__(self,parent=None):
        super(EMainWindow,self).__init__(parent)
        self.mainWindow = PMainWindow(self)
        self.heightWidget = HeightMama(self)
        self.mainWindow.raise_()
        self.setWindowFlags(Qt.FramelessWindowHint|Qt.WindowStaysOnTopHint)
        self.setAttribute(Qt.WA_TranslucentBackground,True)
        desktop = QApplication.desktop()
        print desktop.width(),desktop.height()
        self.setFixedSize(desktop.width(),desktop.height())
        
        self.enterPose = False
#         self.pixmovie = QMovie("a.gif")
#         self.pixlabel = QLabel(self)
#         self.pixlabel.setFixedSize(160,100)
#         self.pixlabel.setMovie(self.pixmovie)
#         self.pixmovie.start()
        #self.pixlabel.hide()
        
        self.connect(self.mainWindow, SIGNAL("moveover"),self.updateLabel)
        self.connect(self.mainWindow, SIGNAL("starthorse"),self.startHorse)
        self.connect(self.mainWindow, SIGNAL("mousepressed"),self.pressedSlot)
        self.connect(self.heightWidget, SIGNAL("stophorse"),self.stopHorse)
        
    def pressedSlot(self):
        return
        self.mainWindow.lower()
        
    def updateLabel(self):
        print self.mainWindow.pos()
        
        #self.pixlabel.hide()
    def startHorse(self):
        #return
        self.mainWindow.circleLabel.pixlabel.hide()
        self.mainWindow.circleLabel.enterPose = False
        self.heightWidget.raise_()
        self.mainWindow.lower()
        x = self.mainWindow.pos().x()
        y = self.mainWindow.pos().y()
        self.heightWidget.move(-self.width()+x+300,y+100)
        self.heightWidget.timer.start(10)
    def stopHorse(self):
        print "stophorse"
        #self.mainWindow.move(self.mainWindow.pos())
        self.mainWindow.raise_()
        self.heightWidget.lower()
            #self.mainWindow.circleLabel.enterPose = True
        if self.enterPose == False:
            self.mainWindow.circleLabel.enterPose = False        #self.mainWindow.setFocus(True)
        else:
            self.mainWindow.circleLabel.enterPose = True
        
        if self.mainWindow.circleLabel.enterPose == True:
            self.mainWindow.circleLabel.pixlabel.show()
        self.mainWindow.update()

    def enterEvent(self,event):
        self.enterPose = True
    def leaveEvent(self,event):
        self.enterPose = False
        
if __name__=='__main__':
    app = QApplication(sys.argv)
    
    w = EMainWindow()
    w.show()
    app.exec_()
