#coding:utf-8
'''
Created on 2015��3��16��

@author: guowu
'''
from PyQt4.QtGui import QPushButton, QWidget, QFont, QPainter, QColor, QPen,\
    QPalette, QConicalGradient, QPolygon, QRadialGradient, QLinearGradient
from PyQt4.QtCore import QDateTime, QTimer, QPointF, Qt, QString, QObject,\
    QPoint
#import std
import math
from PyQt4.Qt import QApplication
import sys

DegreesPerMinute = 7.0
DegreesPerSecond = DegreesPerMinute / 60
MaxMinutes = 45
MaxSeconds = MaxMinutes * 60
UpdateInterval = 5
M_PI = 3.14159265358979323846


class OvenTimer(QWidget):
    def __init__(self,parent=None):
        super(OvenTimer,self).__init__(parent)
        self.finishTime = QDateTime.currentDateTime()
        
        self.updateTimer = QTimer(self)
        #self.connect(updateTimer, SIGNAL(timeout()), this, SLOT(update()))
        
        self.finishTimer = QTimer(self)
        self.finishTimer.setSingleShot(True)
        #connect(finishTimer, SIGNAL(timeout()), this, SIGNAL(timeout()))
        #connect(finishTimer, SIGNAL(timeout()), updateTimer, SLOT(stop()))
        
        font = QFont()
        font.setPointSize(8)
        self.setFont(font)
        
    def getBound(self,b , asecs, aMaxSeconds):
        if asecs < aMaxSeconds:
            a = asecs
        else:
            a = aMaxSeconds
            
        if b > a:
            return b
        else:
            return a

    def setDuration(self, secs):
        #secs = qBound(0, secs, MaxSeconds)
        secs = self.getBound(0, secs, MaxSeconds)
        #secs = 10
        self.finishTime = QDateTime.currentDateTime().addSecs(secs)
    
        if (secs > 0):
            self.updateTimer.start(UpdateInterval * 1000)
            self.finishTimer.start(secs * 1000)
        else :
            self.updateTimer.stop()
            self.finishTimer.stop()
        self.update()

    def duration(self):
        secs = QDateTime.currentDateTime().secsTo(self.finishTime)
        if (secs < 0):
            secs = 0
        return secs

    def mousePressEvent(self, event):
        
        point = QPointF(event.pos() - self.rect().center())
        theta = math.atan2(-point.x(), -point.y()) * 180.0 / M_PI
        self.setDuration(self.duration() + int(theta / DegreesPerSecond))
        self.update()
    

    def paintEvent(self, event):
        painter = QPainter(self)
        painter.setRenderHint(QPainter.Antialiasing, True)
        
        side = self.height()
        if self.width() < self.height():
            side = self.width()
        #side = qMin(self.width(), self.height())
    
        painter.setViewport((self.width() - side) / 2, (self.height() - side) / 2, side, side)
        painter.setWindow(-50, -50, 100, 100)
    
        self.draw(painter)

    def draw(self,painter):
        
        triangle = [[ -2, -49 ], [ +2, -49 ], [ 0, -47 ]]
        
        thickPen = QPen(QPalette().foreground(), 1.5)
        thinPen = QPen(QPalette().foreground(), 0.5)
        niceBlue = QColor(150, 150, 200)
        
        #绘制小三角形
        painter.setPen(thinPen)
        painter.setBrush(QPalette().foreground())
        painter.drawPolygon(QPolygon([QPoint(-2, -49),QPoint(+2, -49),QPoint(0, -47)]))
        
        
        coneGradient = QConicalGradient(0, 0, -90.0)
        coneGradient.setColorAt(0.0, Qt.darkGray)
        coneGradient.setColorAt(0.2, niceBlue)
        coneGradient.setColorAt(0.5, Qt.white)
        coneGradient.setColorAt(1.0, Qt.darkGray)
    
        painter.setBrush(coneGradient)
        painter.drawEllipse(-46, -46, 92, 92)
        
        #绘制内圈圆形
        haloGradient = QRadialGradient(0, 0, 20, 0, 0)#两端圆心坐标，中间为半径
        haloGradient.setColorAt(0.0, Qt.lightGray)
        haloGradient.setColorAt(0.8, Qt.darkGray)
        haloGradient.setColorAt(0.9, Qt.white)
        haloGradient.setColorAt(1.0, Qt.black)
        painter.setPen(Qt.NoPen)
        painter.setBrush(haloGradient)
        painter.drawEllipse(-20, -20, 40, 40)
        
        #绘制中间按钮
        knobGradient = QLinearGradient(-7, -25, 7, -25)
        knobGradient.setColorAt(0.0, Qt.black)
        knobGradient.setColorAt(0.2, niceBlue)
        knobGradient.setColorAt(0.3, Qt.lightGray)
        knobGradient.setColorAt(0.8, Qt.white)
        knobGradient.setColorAt(1.0, Qt.black)
        painter.rotate(self.duration() * DegreesPerSecond)
        painter.setBrush(knobGradient)
        painter.setPen(thinPen)
        painter.drawRoundRect(-7, -25, 14, 50, 99, 49)
    
        
        for i in range(MaxMinutes):
            if (i % 5 == 0):
                painter.setPen(thickPen)
                painter.drawLine(0, -41, 0, -44)
                painter.drawText(-15, -41, 30, 30, Qt.AlignHCenter | Qt.AlignTop, QString.number(i))
            else:
                painter.setPen(thinPen)
                painter.drawLine(0, -42, 0, -44)
            
            painter.rotate(-DegreesPerMinute)
        
        

app = QApplication(sys.argv)
ovenTimer = OvenTimer()
ovenTimer.setWindowTitle(u"Oven Timer")
ovenTimer.resize(300, 300)
ovenTimer.show()
app.exec_()



