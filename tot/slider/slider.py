
import sys
from PyQt4 import QtGui, QtCore


class Ex(QtGui.QWidget):
    def __init__(self):
        super(Ex, self).__init__()
        self.InitUI()


    def InitUI(self):
        self.label = QtGui.QLabel('Ubuntu', self)
        #self.showFullScreen()
        #self.setMinimumSize(800, 600)
        #self.setMaximumSize(800, 800)
        #self.setFixedSize(800,600)

        self.combo = QtGui.QComboBox(self)
        self.combo.addItem('Fedora')
        self.combo.addItem('Red Hat')
        
        self.slider = QtGui.QSlider(QtCore.Qt.Vertical,self)
        self.slider.setFixedHeight(400)
        self.slider.setStyleSheet( "QSlider::groove:vertical {  "
                                        "border: 1px solid #4A708B;  "
                                        "background: #C0C0C0;  "
                                        "width: 5px;  "
                                        "border-radius: 1px;  "
                                        "padding-left:-1px;  "
                                        "padding-right:-1px;  "
                                        "padding-top:-1px;  "
                                        "padding-bottom:-1px;  "
                                        "}"
                                        "QSlider::handle:vertical {"
                                        "height: 100px;"
                                        "background: white;"
                                        "margin: 0 -4px;"
                                        "}" )
        #slider.setFixedSize(250,20)
        self.spinBox = QtGui.QSpinBox(self)
        
       

        self.combo.move(self.geometry().width()/2,self.geometry().height()-100)
        self.label.move(self.geometry().width()/2,self.geometry().height()-200)
        self.slider.move(self.geometry().width()/2-200,self.geometry().height()-400)
        self.spinBox.move(self.geometry().width()/2,self.geometry().height()-300)
        
        
        
        
        
        


        self.connect(self.slider,QtCore.SIGNAL('valueChanged(int)'),self.spinBox, QtCore.SLOT('setValue(int)') )

        self.connect(self.spinBox,QtCore.SIGNAL('valueChanged(int)'),self.slider, QtCore.SLOT('setValue(int)') )

        self.connect(self.combo, QtCore.SIGNAL('activated(QString)'), self.OnActivated)



        self.setWindowTitle('QComboBox')


    def OnActivated(self):
        if  self.combo.currentText() == "Fedora":
            self.label.setText(self.tr("123"))
            self.label.adjustSize()
        elif self.combo.currentText() == "Red Hat":
            self.label.setText(self.tr("456"))
            self.label.adjustSize()

def Main():
    app = QtGui.QApplication(sys.argv)
    ex = Ex()
    ex.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    Main()
