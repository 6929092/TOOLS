#coding:utf-8
'''
Created on 2015��4��17��

@author: guowu
'''
from PyQt4.QtGui import QWidget, QApplication, QPainter, QColor, QPen,\
    QTransform, QPixmap, QLinearGradient, QFont
import sys
from PyQt4.QtCore import Qt, QTimeLine, SIGNAL, QTimer, QTime
class TurnPage(QWidget):
    def __init__(self,parent=None):
        super(TurnPage,self).__init__(parent)
        
        self.resize(200,400)
        
        self.march = 50
        
        self.inWidth = self.width() - self.march*2
        self.inHeight = self.height() - self.march*2
        
        
        self.m_pixmap = QPixmap()
        self.m_lastPixmap = QPixmap()
        self.m_animator = QTimeLine()
        #used to animate a GUI control by calling a slot periodically
        #The timeline's duration describes for how long the animation will run
        #connect the frameChanged() signal to a suitable slot in the widget you wish to animate
     
        self.m_number = 0
        self.m_transition = 0
        self.second = 0
        
        self.setAttribute(Qt.WA_OpaquePaintEvent, True)
        #Widget paints all its pixels when it receives a paint event

        self.setAttribute(Qt.WA_NoSystemBackground, True)
        #Indicates that the widget has no background, i.e. when the widget receives paint events, the background is not automatically repainted.
        self.connect(self.m_animator, SIGNAL("frameChanged(int)"), self.update)
        self.connect(self.m_animator, SIGNAL("finished()"), self.lineTimerFinished)

        self.m_animator.setFrameRange(0, 100)#设置帧数
        self.m_animator.setDuration(500)#设置为0.5秒
        #Construct a 0.6-second timeline with a frame range of 0 - 100

        self.m_animator.setCurveShape(QTimeLine.EaseInOutCurve)#设置弯曲形状
        #starts growing slowly, then runs steadily, then grows slowly again
        
        self.timer = QTimer()
        self.timer.start(1000)
        self.connect(self.timer, SIGNAL("timeout()"),self.flipTo)
        
    def lineTimerFinished(self):
        
        self.update()
        
        
    def flipTo(self):
        
        self.m_lastPixmap = self.m_pixmap
        self.m_animator.stop()
        self.m_pixmap = QPixmap(self.size())
        self.m_pixmap.fill(Qt.transparent)
        
        self.m_animator.start()
        
        
    def resizeEvent(self,event):
        self.inWidth = self.width() - self.march*2
        self.inHeight = self.height() - self.march*2
        self.update()
        
    def paintStatic(self,p):
        
        p.setBrush(Qt.NoBrush)
        p.setPen(Qt.NoPen)
        p.fillRect(self.rect(), Qt.black)
        #Fill the widget rec with black color

        pad = self.width() / 10
        self.drawFrame(p, self.rect().adjusted(pad, pad, -pad, -pad))
        p.drawPixmap(0, 0, self.m_pixmap)
        
    def drawFrame(self,painter,rect):
        print rect
        gradient = QLinearGradient(rect.topLeft(), rect.bottomLeft())
        #Set linear gradient area
        gradient.setColorAt(0.00, QColor(245, 245, 245))
        gradient.setColorAt(0.49, QColor(192, 192, 192))
        gradient.setColorAt(0.51, QColor(245, 245, 245))
        gradient.setColorAt(1.00, QColor(192, 192, 192))
        #Creates stop points at the given position with the given color
        painter.setBrush(gradient)
        painter.setPen(Qt.NoPen)
        r = rect
        painter.drawRoundedRect(r, 15, 15, Qt.RelativeSize)
        '''
            Draws outer rectangle rect with rounded corners.
            Qt::RelativeSize specifies the size relative to the bounding rectangle,
            typically using percentage measurements.
        '''
        font = QFont()
        font.setFamily("Helvetica")
        fontHeight = rect.height()/1
        font.setPixelSize(fontHeight)
        #Sets the font size to pixelSize pixels
        font.setBold(True)
        painter.setPen(QColor(181, 181, 181))
        painter.setBrush(Qt.NoBrush)
        painter.setFont(font)
        painter.drawText(r, Qt.AlignCenter, str(self.second))

        r.adjust(0, 4, 0, -4)
        #Adds 1, 4, -1 and -4 respectively to the existing coordinates of the rectangle
        painter.setPen(QColor(181, 181, 181))
        painter.setBrush(Qt.NoBrush)
        painter.drawRoundedRect(r, 15, 15, Qt.RelativeSize)
        #Draws inner rectangle rect with rounded corners.
            

        painter.setPen(QColor(159, 159, 159))
        y = rect.top() + rect.height() / 2 - 1
        painter.drawLine(rect.left(), y, rect.right(), y)
        #Draws the mid-line from (rect.left(), y) to (rect.right(), y) and sets the current pen position to (rect.right(), y)
        
        
    def paintFlip(self,p):
        p.setBrush(Qt.NoBrush)
        p.setPen(Qt.NoPen)
        p.setRenderHint(QPainter.SmoothPixmapTransform, True)#像素光滑
        p.setRenderHint(QPainter.Antialiasing, True)#反锯齿

        hw = self.width() / 2
        hh = self.height() / 2
        
        pad = self.width() / 10
        fr = self.rect().adjusted(pad, pad, -pad, -pad)#调整，march值为pad
        self.drawFrame(p, fr)#画整个长方形

        index = self.m_animator.currentFrame()
        #画上部分的也页面翻转
        if (index <= 50):

            angle = -180 * index / 100#计算翻转的角度
            transform = QTransform()
            transform.translate(hw, hh)#中心点

            transform.rotate(angle, Qt.XAxis)
            #Rotates the coordinate system counterclockwise by angle about the X axis

            p.setTransform(transform)
            self.drawFrame(p, fr.adjusted(-hw, -hh, -hw, -hh))

            p.resetTransform()
            p.setClipRect(0, hh, self.width(), hh)
            #Enables clipping, and sets the clip region to the rectangle beginning at (0, hh) with the given width and height

            self.drawFrame(p, fr)
        else:
            
            self.second = QTime.currentTime().second()
            p.setClipRect(0, hh, self.width(), hh)
            self.drawFrame(p, fr)
            angle = 180 - 180 * self.m_animator.currentFrame() / 100
            transform = QTransform()
            transform.translate(hw, hh)
            transform.rotate(angle, Qt.XAxis)
            p.setTransform(transform)
            self.drawFrame(p, fr.adjusted(-hw, -hh, -hw, -hh))
        
    def paintEvent(self,event):
        print "00000000000"
        painter = QPainter(self)
        painter.setPen(QPen(QColor(0,0,0,200),2))
        painter.drawRect(self.march,self.march,self.inWidth,self.inHeight)
        
        if (self.m_animator.state() == QTimeLine.Running):
            self.paintStatic(painter)
            self.paintFlip(painter)
        else:
            self.paintStatic(painter)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    
    c = TurnPage()
    c.show()
    
    app.exec_()
        
