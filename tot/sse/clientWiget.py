﻿# -*- coding: utf-8 -*-  

import socket
import threading
import sys
import time
from gameCo import TestWidget
from PyQt4.QtGui import *
from PyQt4.QtCore import *


host = '192.168.5.198'
port = 8806
username = 'Default'

class Client(QWidget):
    def __init__(self, parent = None):
        super(Client, self).__init__(parent)
        self.setWindowTitle('CLient')
        #self.setFixedSize(900,700)
        self.setNameWidget = QWidget()
        self.mainWidget = TestWidget()
        self.layout = QGridLayout()
        self.setNameLayout = QGridLayout(self.setNameWidget)
        self.btnSend = QPushButton(self.trUtf8("发送"))
        self.btnSend.setFont(QFont("times",15))
        self.btnSet = QPushButton(self.trUtf8("设置"))
        self.input = QLineEdit()
        self.name = QLineEdit('DeFault')
        self.chat = QTextEdit()
        self.label = QLabel('name:')
        self.timer = QTimer()
        
        
        self.messages = []
        #self.messages = core.QString()
        #self.messages = {}
        self.connect(self, SIGNAL("paint"),self.paint)

        self.build()
        
        self.createAction()
        
        

        recvThread = threading.Thread(target = self.recvFromServer)
        recvThread.setDaemon(True)
        recvThread.start()
    def paint(self,actionType):
        print actionType
        sList = actionType.split()
        flag = sList[11]
        #print flag
        count = sList[1]
        bList = [sList[2],sList[3],sList[4],sList[5],sList[6],sList[7],sList[8],sList[9],sList[10]]
        #print bList
        for n in range(0,9):
            if bList[n] == "1":
                self.mainWidget.buttonList[n].setIcon(QIcon("dsaf.png"))
                self.mainWidget.buttonList[n].setIconSize(QSize(80,80))
            else:
                self.mainWidget.buttonList[n].setIcon(QIcon())
                #self.mainWidget.buttonList[n].setIconSize(80,80)

    def sendToServer(self):
        global username
        text = str(self.input.text())
        self.input.setText('')
        if text == 'q':
            self.exit()
        elif text.strip() == '':
            return
        try:
            s.send('%s: %s' % (username, text))
            print '%s >> %s' % (username, text)
        except:
            self.exit()

    def recvFromServer(self):
        while 1:
            try:
                data = s.recv(1024)
                if not data:
                    exit()
                self.messages.append(data)
                #print self.messages
            except:
                return

    def showChat(self):
        for m in self.messages:
            #mList = m.split()
            #print len(m)
            if len(m.split()) == 12:
                print "sss"
                if (m.split()[11]) != "button":
                    self.chat.append(self.trUtf8(m))
                else:
                    print "aaa"
                    self.emit(SIGNAL("paint"),m)
            else:
                    self.chat.append(self.trUtf8(m))
            #print m
            #print self.messages
        self.messages = []
    def slotExtension(self):
        global username
        name = str(self.name.text())
        if name.strip() != '':
            username = name
            print username
        self.setNameWidget.hide()

    def exit(self):
        s.close()
        sys.exit()

    def build(self):
        self.layout.addWidget(self.chat, 0, 0, 5, 4)
        self.layout.addWidget(self.input, 5, 0, 1, 3)
        self.layout.addWidget(self.btnSend, 5, 3,1,1)
        self.setNameLayout.addWidget(self.label, 0, 0,1,1)
        self.setNameLayout.addWidget(self.name, 0, 1,1,2)
        self.setNameLayout.addWidget(self.btnSet, 0, 4,1,1)
        self.layout.addWidget(self.setNameWidget, 6, 0,1,4)
        mainLayout = QHBoxLayout()
        mainLayout.addLayout(self.layout)
        mainLayout.addWidget(self.mainWidget)
        self.setLayout(mainLayout)
        #self.layout.setSizeConstraint(QLayout.SetFixedSize)
    def createAction(self):
        self.btnSend.clicked.connect(self.sendToServer)
        self.btnSet.clicked.connect(self.slotExtension)
        self.timer.timeout.connect(self.showChat)
        self.timer.start(1000)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#time.sleep(1)
#print "连接成功"
s.connect((host, port))
s.send(username)
print '[%s] connect' % username

app = QApplication(sys.argv)
c = Client()
c.show()
app.exec_()