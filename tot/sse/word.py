import sys
from PyQt4 import QtGui, QtCore
class Window( QtGui.QMainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        self.setWindowTitle('hello')
        self.resize(800,500)

        menubar = self.menuBar()
        self.file = menubar.addMenu('&file')
        open = self.file.addAction('open')
        self.connect(open,QtCore.SIGNAL('triggered()'),self.OnOpen)

        save =self.file.addAction('save')
        self.connect(save,QtCore.SIGNAL('triggered()'),self.OnSave)
        self.file.addSeparator()
        close = self.file.addAction('close')
        self.connect(close,QtCore.SIGNAL('triggered()'),self.OnClose)

        self.label = QtGui.QLabel('this is a google text')
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.setCentralWidget(self.label)

    def OnOpen(self):
        self.label.setText('open')
    def OnClose(self):
        self.close()
    def OnSave( self):
        self.label.setText('save')
    def contextMenuEvent(self,event):
        self.file.exec_( event.globalPos())

app =QtGui.QApplication(sys.argv)
win = Window()
win.show()
app.exec_()

