#coding:utf-8

from PyQt4.QtGui import QApplication, QSlider, QSpinBox, QWidget
from PyQt4.QtGui import QHBoxLayout
from PyQt4.QtCore import Qt, QObject, SIGNAL
import sys

class TestSpinBox(QWidget):
    def __init__(self,parent=None):
        super(TestSpinBox,self).__init__(parent)
        self.setWindowTitle("Enter Your Age")
        
        
        self.setStyleSheet("QSlider:groove:horizontal {"
                                                    "border: 1px solid #bbb;"
                                                    "background: white;"
                                                    "height: 30px;"
                                                    "border-radius: 4px;"
                                                    "}"
                           
        
                            "QSlider:sub-page:horizontal {"
                                                    "background: qlineargradient(x1: 0, y1: 0,    x2: 0, y2: 1,stop: 0 #66e, stop: 1 #bbf);"
                                                    "background: qlineargradient(x1: 0, y1: 0.2, x2: 1, y2: 1,stop: 0 #bbf, stop: 1 #55f);"
                                                    "border: 1px solid #777;"
                                                    "height: 30px;"
                                                    "border-radius: 4px;"
                                                    "}"
        
                            "QSlider:add-page:horizontal {"
                                                    "background: #fff;"
                                                    "border: 1px solid #777;"
                                                    "height: 30px;"
                                                    "border-radius: 4px;"
                                                    "}"
        
                            "QSlider:handle:horizontal {"
                                                    "background: qlineargradient(x1:0, y1:0, x2:1, y2:1,stop:0 #eee, stop:1 #ccc);"
                                                    "border: 1px solid #777;"
                                                    "width: 40px;"
                                                    
                                                    "margin-top: -20px;"
                                                    "margin-bottom: -20px;"
                                                    "border-radius: 4px;"
                                                    "}"
        
                            "QSlider:handle:horizontal:hover {"
                                                    "background: qlineargradient(x1:0, y1:0, x2:1, y2:1,stop:0 #fff, stop:1 #ddd);"
                                                    "border: 1px solid #444;"
                                                    "border-radius: 4px;"
                                                    "}"
        
        
                            "QSlider:sub-page:horizontal:disabled {"
                                                                    "background: #bbb;"
                                                                    "border-color: #999;"
                                                                    "}"
        
                            "QSlider:add-page:horizontal:disabled {"
                                                                    "background: #eee;"
                                                                    "border-color: #999;"
                                                                    "}"
        
                            "QSlider:handle:horizontal:disabled {"
                                                                "background: #eee;"
                                                                "border: 1px solid #aaa;"
                                                                "border-radius: 4px;"
                                                                "}"
                            )
        "//add :groove为槽的颜色，handle为按钮的颜色，add-page 及sub-page分别问按钮前后的颜色，如果groove与add-page、sub-page同在，那么groove的颜色会被覆盖掉。"
        self.spinBox = QSpinBox()
        self.slider = QSlider(Qt.Horizontal)
        self.spinBox.setRange(0, 130)
        self.slider.setRange(0, 130)
        self.slider.setFixedHeight(100)
        self.connect(self.spinBox, SIGNAL("valueChanged(int)"),self.setSliderValue)
        self.connect(self.slider, SIGNAL("valueChanged(int)"),self.setSpinBoxValue)
        
        self.spinBox.setValue(35)
    
        layout = QHBoxLayout()
        layout.addWidget(self.spinBox)
        layout.addWidget(self.slider)
        self.setLayout(layout)
        
    def setSliderValue(self,value):
        self.slider.setValue(value)
        
    def setSpinBoxValue(self,value):
        self.spinBox.setValue(value)


if __name__=='__main__':

    app = QApplication(sys.argv)
    window = TestSpinBox()
    window.show()
    app.exec_()

