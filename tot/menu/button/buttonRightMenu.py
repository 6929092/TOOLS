#coding=utf-8  
'''
Created on 2014年8月29日

@author: GuoWu
'''
import sys  
from PyQt4 import QtGui  ,QtCore
from PyQt4.QtCore import Qt  
  
class ButtonMenu(QtGui.QPushButton):  
    def __init__(self,parent=None):  
        super(ButtonMenu, self).__init__(parent)  
        self.setFixedSize(100,100)
        self.createContextMenu()  
        self.count = 0
    def createContextMenu(self):  
        ''''' 
        创建右键菜单 
        '''  
        # 必须将ContextMenuPolicy设置为Qt.CustomContextMenu  
        # 否则无法使用customContextMenuRequested信号  
        self.setContextMenuPolicy(Qt.CustomContextMenu)  
        self.customContextMenuRequested.connect(self.showContextMenu)  
        # 创建QMenu  
        self.contextMenu = QtGui.QMenu(self)  
        self.actionA = self.contextMenu.addAction(QtGui.QIcon("0.png"),u'动作A')  
        self.actionB = self.contextMenu.addAction(QtGui.QIcon("0.png"),u'动作B')  
        self.actionC = self.contextMenu.addAction(QtGui.QIcon("0.png"),u'动作C')  
        # 将动作与处理函数相关联  
        # 这里为了简单，将所有action与同一个处理函数相关联，  
        # 当然也可以将他们分别与不同函数关联，实现不同的功能  
        self.second = self.contextMenu.addMenu(QtGui.QIcon("0.png"),u"二级菜单") 
        self.actionD = self.second.addAction(QtGui.QIcon("0.png"),u'动作A')
        self.actionE = self.second.addAction(QtGui.QIcon("0.png"),u'动作B')
        self.actionF = self.second.addAction(QtGui.QIcon("0.png"),u'动作C')
        # 将动作与处理函数相关联  
        # 这里为了简单，将所有action与同一个处理函数相关联，  
        # 当然也可以将他们分别与不同函数关联，实现不同的功能  
        self.actionA.triggered.connect(self.actionHandler)  
        self.actionB.triggered.connect(self.actionHandler)  
        self.actionC.triggered.connect(self.actionHandler) 
        self.actionD.triggered.connect(self.actionHandler)  
        self.actionE.triggered.connect(self.actionHandler)  
        self.actionF.triggered.connect(self.actionHandler)  
  
  
    def showContextMenu(self, pos):  
        ''''' 
        右键点击时调用的函数 
        '''  
        self.count+=1
        # 菜单显示前，将它移动到鼠标点击的位置  
        self.contextMenu.exec_(QtGui.QCursor.pos())
        print self.count
    def actionHandler(self):  
        ''''' 
        菜单中的具体action调用的函数 
        '''  
        if self.count%3==1:
            self.setText(u"first")
        elif self.count%3==2:
            self.setText(u"second")
        elif self.count%3==0:
            self.setText(u"third")
