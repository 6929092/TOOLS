#coding:utf-8
'''
Created on 2015年4月2日

@author: guowu
'''
from PyQt4.QtGui import QApplication, QLineEdit, QIntValidator, QValidator,\
    QLabel, QWidget, QRegExpValidator
from PyQt4.QtCore import QObject, SIGNAL, SLOT, pyqtSlot, QString, QRegExp,\
    QPoint
import sys
from PyQt4.Qt import Qt


class IpPartEdit(QLineEdit):
    def __init__(self, parent = None):
        QLineEdit.__init__(self, parent)

        self.nextTab = None
        self.preTab = None
        #self.setMaxLength(3)
        #self.setFixedWidth(40)
        self.setFrame(False)#设置无边框
        self.setAlignment(Qt.AlignLeft)
        validator = QIntValidator(0, 255, self)#限制输入数字为0到255
        self.setValidator(validator)

        self.connect(self, SIGNAL('textEdited(QString)'),self, SLOT('text_edited(QString)'))

    def set_nextTabEdit(self, nextTab):
        self.nextTab = nextTab
    def set_preTabEdit(self, preTab):
        self.preTab = preTab
    def focusInEvent(self, event):
        self.selectAll()
        super(IpPartEdit, self).focusInEvent(event)

    def keyPressEvent(self, event):
        if (event.key() == Qt.Key_Right):
            if self.nextTab:
                self.nextTab.setFocus()
                self.nextTab.selectAll()
        if (event.key() == Qt.Key_Left):
            if self.preTab:
                self.preTab.setFocus()
                self.preTab.selectAll()
        if (event.key() == Qt.Key_Backspace):
            if self.text() == "":
                if self.preTab:
                    self.preTab.setFocus()
                    self.preTab.selectAll()
        if (event.key() == Qt.Key_Period or event.key() == Qt.Key_Space):
            if self.text() != "":
                if self.nextTab:
                    self.nextTab.setFocus()
                    self.nextTab.selectAll()
        super(IpPartEdit, self).keyPressEvent(event)

    @pyqtSlot('QString')
    def text_edited(self, text):
        validator = QIntValidator(0, 255, self)
        ipaddr = text
        pos = 0
        
        state = validator.validate(ipaddr, pos)[0]
        if state == QValidator.Acceptable:
            if ipaddr.size() > 1:
                if ipaddr.size() == 2:
                    ipnum = ipaddr.toInt()[0]
                    if ipnum > 25:
                        if self.nextTab:
                            self.nextTab.setFocus()
                            self.nextTab.selectAll()
                else:
                    if self.nextTab:
                        self.nextTab.setFocus()
                        self.nextTab.selectAll()
    

class IpLineEdit(QLineEdit):
    def __init__(self, parent = None):
        QLineEdit.__init__(self, parent)
        #self.setFixedWidth(170)
        
        self.ip_part1 = IpPartEdit(self)
        
        self.ip_part2 = IpPartEdit(self)
        
        self.ip_part3 = IpPartEdit(self)
        
        self.ip_part4 = IpPartEdit(self)
        
        self.ip_part1.setAlignment(Qt.AlignCenter)
        self.ip_part2.setAlignment(Qt.AlignCenter)
        self.ip_part3.setAlignment(Qt.AlignCenter)
        self.ip_part4.setAlignment(Qt.AlignCenter)

        self.labeldot1 = QLabel('.',self)
        self.labeldot2 = QLabel('.',self)
        self.labeldot3 = QLabel('.',self)
        #self.labeldot4 = QLabel(' ',self)
        self.labeldot1.setAlignment(Qt.AlignBottom)
        self.labeldot2.setAlignment(Qt.AlignBottom)
        self.labeldot3.setAlignment(Qt.AlignBottom)
        

        QWidget.setTabOrder(self.ip_part1, self.ip_part2)
        QWidget.setTabOrder(self.ip_part2, self.ip_part3)
        QWidget.setTabOrder(self.ip_part3, self.ip_part4)
        #QWidget.setTabOrder(self.ip_part4, self.ip_part1)
        self.ip_part1.set_nextTabEdit(self.ip_part2)
        self.ip_part2.set_nextTabEdit(self.ip_part3)
        self.ip_part3.set_nextTabEdit(self.ip_part4)
        #self.ip_part4.set_nextTabEdit(self.ip_part1)
        self.ip_part1.set_preTabEdit(self.ip_part1)
        self.ip_part4.set_preTabEdit(self.ip_part3)
        self.ip_part3.set_preTabEdit(self.ip_part2)
        self.ip_part2.set_preTabEdit(self.ip_part1)

        self.connect(self.ip_part1, SIGNAL('textChanged(QString)'),self, SLOT('textChangedSlot(QString)'))
        self.connect(self.ip_part2, SIGNAL('textChanged(QString)'),self, SLOT('textChangedSlot(QString)'))
        self.connect(self.ip_part3, SIGNAL('textChanged(QString)'),self, SLOT('textChangedSlot(QString)'))
        self.connect(self.ip_part4, SIGNAL('textChanged(QString)'),self, SLOT('textChangedSlot(QString)'))

        self.connect(self.ip_part1, SIGNAL('textEdited(QString)'),self, SLOT('textEditedSlot(QString)'))
        self.connect(self.ip_part2, SIGNAL('textEdited(QString)'),self, SLOT('textEditedSlot(QString)'))
        self.connect(self.ip_part3, SIGNAL('textEdited(QString)'),self, SLOT('textEditedSlot(QString)'))
        self.connect(self.ip_part4, SIGNAL('textEdited(QString)'),self, SLOT('textEditedSlot(QString)'))

        self.refreshLineEdit()
        
    def refreshLineEdit(self):
        totalwidth = 120 - 2
        height = self.height() - 2
        self.ip_part1.resize(totalwidth/4-2,height)
        self.ip_part2.resize(totalwidth/4-2,height)
        self.ip_part3.resize(totalwidth/4-2,height)
        self.ip_part4.resize(totalwidth/4-2,height)
        
        
        partwidth = self.ip_part1.width()
        self.ip_part1.move(1+1,1)
        self.ip_part2.move(partwidth+3+1,1)
        self.ip_part3.move(partwidth*2+5+1,1)
        self.ip_part4.move(partwidth*3+7+1,1)
        
        self.labeldot1.resize(2,height-2)
        self.labeldot2.resize(2,height-2)
        self.labeldot3.resize(2,height-2)
        #self.labeldot1.resize(2,height)
        self.labeldot1.move(partwidth+1+1,1)
        self.labeldot2.move(partwidth*2+3+1,1)
        self.labeldot3.move(partwidth*3+5+1,1)
        
    def setSize(self,width,height):
        self.resize(width,height)
        self.refreshLineEdit()
        
    @pyqtSlot('QString')
    def textChangedSlot(self, text):
        ippart1 = self.ip_part1.text()
        ippart2 = self.ip_part2.text()
        ippart3 = self.ip_part3.text()
        ippart4 = self.ip_part4.text()

        ipaddr = QString('%1.%2.%3.%4').arg(ippart1).arg(ippart2).arg(ippart3).arg(ippart4)
        
        self.emit(SIGNAL('textChanged'), ipaddr)

    @pyqtSlot('QString')
    def textEditedSlot(self, text):
        ippart1 = self.ip_part1.text()
        ippart2 = self.ip_part2.text()
        ippart3 = self.ip_part3.text()
        ippart4 = self.ip_part4.text()

        ipaddr = QString('%1.%2.%3.%4').arg(ippart1).arg(ippart2).arg(ippart3).arg(ippart4)
        self.emit(SIGNAL('textEdited'), ipaddr)

    def setText(self, text):
        regexp = QRegExp('^((2[0-4]\d|25[0-5]|[01]?\d\d?).){3}(2[0-4]\d||25[0-5]|[01]?\d\d?)$')
        validator = QRegExpValidator(regexp ,self)
        nPos = 0
        state = validator.validate(QString(text), nPos)[0]

        ippart1 = QString()
        ippart2 = QString()
        ippart3 = QString()
        ippart4 = QString()

        if state == QValidator.Acceptable:  # valid
            ippartlist = text.split('.')

            strcount = len(ippartlist)
            index = 0
            if index < strcount:
                ippart1 = ippartlist[index]
            index += 1
            if index < strcount:
                ippart2 = ippartlist[index]
                index += 1
            if index < strcount:
                ippart3 = ippartlist[index]
                index += 1
            if index < strcount:
                ippart4 = ippartlist[index]

        self.ip_part1.setText(ippart1)
        self.ip_part2.setText(ippart2)
        self.ip_part3.setText(ippart3)
        self.ip_part4.setText(ippart4)

    def text(self):
        ippart1 = self.ip_part1.text()
        ippart2 = self.ip_part2.text()
        ippart3 = self.ip_part3.text()
        ippart4 = self.ip_part4.text()

        return QString('%1.%2.%3.%4').arg(ippart1).arg(ippart2).arg(ippart3).arg(ippart4)
    
    def clear(self):
        self.ip_part1.clear()
        self.ip_part2.clear()
        self.ip_part3.clear()
        self.ip_part4.clear()
   
class MyMainWindow(QWidget): 
    def __init__(self,parent=None):
        super(MyMainWindow,self).__init__(parent)
        #self.setFixedSize(200,200)
        self.ipLineEdit = IpLineEdit(self)
        self.ipLineEdit.setSize(120, 20)
        self.ipLineEdit.move(QPoint(100,100))
        self.ipLineEdit.setText(u"115.122.122.5")
        
        
if __name__ == "__main__":
    app = QApplication(sys.argv)
    c = MyMainWindow()
    c.show()
    app.exec_()
    