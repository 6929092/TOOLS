from PyQt4.QtGui import *
from PyQt4.QtCore import *
from game import TestWidget

class SlotSignal(QWidget):
    def __init__(self,parent=None):
        super(SlotSignal,self).__init__(parent)
        self.setStyleSheet("QLineEdit{background:blue;}"
                           "QLineEdit{color:white;}"
                           "QLabel{color:white;}"
                           )
       
        self.resize(304,434)
        self.testWidget = TestWidget(self)
        self.lineHour = QLineEdit(self)
        self.lineHour.setFixedSize(30,20)
        self.lineHour.setFont(QFont("times",15))
        self.lineHour.setDisabled(True)
        #self.lineHour.setFrame(False)
        self.lineMinute = QLineEdit(self)
        self.lineMinute.setFixedSize(30,20)
        self.lineMinute.setFont(QFont("times",15))
        self.lineMinute.setDisabled(True)
        self.lineSecond = QLineEdit(self)
        self.lineSecond.setFixedSize(30,20)
        self.lineSecond.setFont(QFont("times",15))
        self.lineSecond.setDisabled(True)
        #self.lineSecond.setFrame(False)
        self.lineCount = QLineEdit(self)
        self.lineCount.setFixedSize(30,30)
        self.lineCount.setFont(QFont("times",15))
        self.lineCount.setDisabled(True)
        self.countTemp = 0
        self.hourTemp = 0
        self.minuteTemp = 0
        self.secondTemp = 0
        
        self.testWidget.move((self.geometry().width()-self.testWidget.geometry().width())/2,(self.geometry().height()-self.testWidget.geometry().height())/2)
        self.lineHour.move(self.geometry().width()/2 - 60,35)
        self.lineMinute.move(self.geometry().width()/2 - 30,35)
        self.lineSecond.move(self.geometry().width()/2,35)
        self.lineCount.move(self.geometry().width()/2 + 30,35)
        #self.testWidget.hide()
        self.connect(self.testWidget,SIGNAL("paintButton"),self.slotPaintButton )
        
        self.msTimer = QTimer()
        self.connect(self.msTimer,SIGNAL("timeout()"),self.slotTime)
        
        
        
        
        #self.msTimer.start(10)
        
        
        self.timeWidget = Time(self)
        self.timeWidget.move((self.geometry().width() - self.timeWidget.geometry().width())/2,5)
        
        
        
        
   
        
        
        
        
        
    def slotTime(self):
        
        

        self.countTemp+=1
        if(self.countTemp==100):
    
            self.countTemp=0
            self.secondTemp+=1
            if(self.secondTemp==60):
        
                self.secondTemp=0
                self.minuteTemp+=1
                if(self.minuteTemp==1):
                    self.msTimer.stop()
                    self.emit(SIGNAL("stop"))
                if(self.minuteTemp==60):
            
                    self.minuteTemp=0
                    self.hourTemp+=1
                    if(self.hourTemp==24):
                
                        self.hourTemp=0
                        
        self.countstr = QString.number(self.countTemp)
        self.hourstr = QString.number(self.hourTemp)
        self.minutestr = QString.number(self.minuteTemp)
        self.secondstr = QString.number(self.secondTemp)
        
        self.lineHour.setText(self.hourstr)
        self.lineMinute.setText(self.minutestr)
        self.lineSecond.setText(self.secondstr)
        self.lineCount.setText(self.countstr)
        

        
    def slotPaintButton(self,actionType):
        self.emit(SIGNAL("paintButton"),actionType)
        
        
class Time(QWidget):
    def __init__(self,parent = None):
        super(Time,self).__init__(parent)
        self.setFixedSize(160,30)
        self.label = QLabel(self)
        self.label.setFont(QFont("times",15))
        #self.event = QTimerEvent(self)
        
        self.timerEvent(0)
        self.startTimer(1000)

    def timerEvent(self,event):
        #self.event = event
        self.dt = QDateTime.currentDateTime()
        self.date = self.dt.date()
        self.time = self.dt.time()
        dtsrt = QString("%1-%2-%3 %4:%5:%6").arg(self.date.year()).arg(self.date.month()).arg(self.date.day()).arg(self.time.hour()).arg(self.time.minute()).arg(self.time.second())
        
        self.label.setText(dtsrt)
        
    