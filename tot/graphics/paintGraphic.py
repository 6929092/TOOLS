#coding:utf-8
'''
Created on 2015��4��23��

@author: guowu
'''
from PyQt4.QtGui import QWidget, QPalette, QLabel, QApplication, QPixmap,QPushButton,\
    QHBoxLayout, QVBoxLayout
import sys
from PyQt4.QtCore import QPoint, QTimeLine, SIGNAL, QSize

from slideGraphic import SlideGraphic

class ToolButton(QWidget):
    def __init__(self,parent=None):
        super(ToolButton,self).__init__(parent)
        self.resize(QSize(240,60))
        self.leftButton = QPushButton()
        self.leftButton.setFixedSize(QSize(100,50))
        
        self.rightButton = QPushButton()
        self.rightButton.setFixedSize(QSize(100,50))
        
        self.hLayout = QHBoxLayout()
        self.hLayout.addWidget(self.leftButton)
        self.hLayout.addWidget(self.rightButton)
        self.setLayout(self.hLayout)
        
        self.connect(self.leftButton, SIGNAL("clicked()"),self.slotLeftButtonClicked)
        self.connect(self.rightButton, SIGNAL("clicked()"),self.slotRightButtonClicked)
    def slotLeftButtonClicked(self):
        self.emit(SIGNAL("leftbuttonclicked"))
    
    def slotRightButtonClicked(self):
        self.emit(SIGNAL("rightbuttonclicked"))

class PaintGraphic(QWidget):
    def __init__(self,parent=None):
        super(PaintGraphic,self).__init__(parent)
        self.setFixedSize(300,500)
        
        self.slideGraphic = SlideGraphic(self)
        self.slideGraphic.setCenterPoint(QPoint(150,250))
        self.buttonLayout = ToolButton(self)
        
        self.labelBackground = QLabel(self)
        pixmap = QPixmap("icons/surfacing.png").scaled(500,500)
        self.labelBackground.setPixmap(pixmap)
        
        self.lineTimer = QTimeLine()
        self.connect(self.lineTimer, SIGNAL("frameChanged(int)"), self.updatePosition)
        self.lineTimer.setDuration(600)
        self.lineTimer.setFrameRange(0, 300)
        self.lineTimer.setCurveShape(QTimeLine.EaseInCurve)
        self.labellist = []
        
        self.count = 0
        #self.initAllPicture()
#         self.mainLayout = QVBoxLayout(self)
#         self.mainLayout.addStretch()
#         self.mainLayout.addWidget(self.buttonLayout)

        self.buttonLayout.move(QPoint((self.width()-self.buttonLayout.width())/2,self.height()-self.buttonLayout.height()))
        self.buttonLayout.raise_()
        self.slideGraphic.raise_()
        
        self.connect(self.buttonLayout, SIGNAL("leftbuttonclicked"),self.slotLeftMove)
        self.connect(self.buttonLayout, SIGNAL("rightbuttonclicked"),self.slotRightMove)
    
    def initAllPicture(self):
        piclist = ["images/brownies.jpg","images/chocolate.jpg","images/cookies.jpg","images/fish.jpg","images/mussels.jpg","images/pasta.jpg","images/pizza.jpg","images/puding.jpg","images/sushi.jpg","images/trouts.jpg"]
        
        for i in range(len(piclist)):
            picLabel = QLabel(self)
            pixmap = QPixmap(piclist[i])#.scaled(1440,900)
            picLabel.setPixmap(pixmap)
            self.labellist.append(picLabel)
        
        for i in range(len(self.labellist)):
            self.labellist[i].move(i*300+50,100)
        
        self.labelBackground.move(QPoint(0,0))
        
    def slotLeftMove(self):
        if self.lineTimer.state() != QTimeLine.NotRunning:
            return
        self.count+=1
        if self.count > 9:
            self.count-=1
            return
        
        self.labelBackgroundPosX = self.labelBackground.pos().x()
        print self.labelBackgroundPosX
        #if self.labelBackgroundPosX <= 10:
            #return
        self.perDirect = float(-20/300.00)
        
        
        self.slideGraphic.likeLabelClicked(self.count)
        self.lineTimer.start()
        
    def slotRightMove(self):
        if self.lineTimer.state() != QTimeLine.NotRunning:
            return
        self.count-=1
        if self.count < 0:
            self.count+=1
            return
        print "moveright"
        self.labelBackgroundPosX = self.labelBackground.pos().x()
        print self.labelBackgroundPosX
        #if self.labelBackgroundPosX >= -10:
            #return
        self.perDirect = float(20/300.00)
        
        self.slideGraphic.likeLabelClicked(self.count)
        self.lineTimer.start()
        
    def startLineTimer(self):
        print "push"
        self.lineTimer.start()
         
    def updatePosition(self):
        print "00000000000000000000000"
        index = self.lineTimer.currentFrame()
        print index
        self.labelBackground.move(QPoint(self.labelBackgroundPosX + self.perDirect*index, 0)) 
        
        
if __name__ == "__main__":
    app = QApplication(sys.argv)
    mainwindow = PaintGraphic()
    mainwindow.show()
    app.exec_()