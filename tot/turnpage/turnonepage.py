#coding:utf-8
'''
Created on 2015��4��17��

@author: guowu
'''
from PyQt4.QtGui import QWidget, QApplication, QPainter, QColor, QPen,\
    QTransform, QPixmap, QLinearGradient, QFont
import sys
from PyQt4.QtCore import Qt, QTimeLine, SIGNAL, QTimer, QTime

import math

class TurnPage(QWidget):
    def __init__(self,parent=None):
        super(TurnPage,self).__init__(parent)
        
        self.resize(150,150)
        self.march = 50
        self.inWidth = self.width() - self.march*2
        self.inHeight = self.height() - self.march*2
        self.m_pixmap = QPixmap()
        self.m_lastPixmap = QPixmap()
        self.m_animator = QTimeLine()
     
        self.m_number = 0
        self.m_transition = 0
        self.second = 0
        
        self.setAttribute(Qt.WA_OpaquePaintEvent, True)
        #Widget paints all its pixels when it receives a paint event

        self.setAttribute(Qt.WA_NoSystemBackground, True)
        #Indicates that the widget has no background, i.e. when the widget receives paint events, the background is not automatically repainted.
        self.connect(self.m_animator, SIGNAL("frameChanged(int)"), self.update)
        self.connect(self.m_animator, SIGNAL("finished()"), self.lineTimerFinished)

        self.m_animator.setFrameRange(0, 80)#设置帧数
        self.m_animator.setDuration(800)#设置为0.5秒
        #Construct a 0.6-second timeline with a frame range of 0 - 100

        self.m_animator.setCurveShape(QTimeLine.EaseInOutCurve)#设置弯曲形状
        #starts growing slowly, then runs steadily, then grows slowly again
        
        self.timer = QTimer()
        self.timer.start(1000)
        self.connect(self.timer, SIGNAL("timeout()"),self.flipTo)
        
    def lineTimerFinished(self):
        pass
        #self.update()
        
    def flipTo(self):
        
        self.m_lastPixmap = self.m_pixmap
        self.m_animator.stop()
        self.m_pixmap = QPixmap(self.size())
        self.m_pixmap.fill(Qt.transparent)
        
        self.m_animator.start()
        
        
    def resizeEvent(self,event):
        self.inWidth = self.width() - self.march*2
        self.inHeight = self.height() - self.march*2
        self.update()
        
    def paintStatic(self,p):
        
        p.setRenderHint(QPainter.SmoothPixmapTransform, True)#像素光滑
        p.setRenderHint(QPainter.Antialiasing, True)#反锯齿
        p.setBrush(Qt.NoBrush)
        p.setPen(Qt.NoPen)
        p.fillRect(self.rect(), Qt.black)
        #Fill the widget rec with black color
        pad = self.width() / 10
        self.drawFrame(p, self.rect().adjusted(pad, pad, -pad, -pad))
        #p.drawPixmap(0, 0, self.m_pixmap)
        
    def drawFrame(self,painter,rect):
        print rect
        gradient = QLinearGradient(rect.topLeft(), rect.bottomLeft())
        #Set linear gradient area
        gradient.setColorAt(0.00, QColor(245, 245, 245))
        gradient.setColorAt(0.98, QColor(192, 192, 192))
        gradient.setColorAt(0.99, QColor(245, 245, 245))
        gradient.setColorAt(1.00, QColor(192, 192, 192))
        #Creates stop points at the given position with the given color
        painter.setBrush(gradient)
        painter.setPen(Qt.NoPen)
        r = rect
        painter.drawRoundedRect(r, 0, 0, Qt.RelativeSize)
        '''
            Draws outer rectangle rect with rounded corners.
            Qt::RelativeSize specifies the size relative to the bounding rectangle,
            typically using percentage measurements.
        '''
        
        font = QFont()
        font.setFamily("Helvetica")
        #fontHeight = rect.height()/1
        font.setPixelSize(100)
        #Sets the font size to pixelSize pixels
        font.setBold(True)
        painter.setPen(QColor(181, 181, 181))
        painter.setBrush(Qt.NoBrush)
        painter.setFont(font)
        painter.drawText(r, Qt.AlignCenter, str(self.second))

        r.adjust(0, 4, 0, -4)
        #Adds 1, 4, -1 and -4 respectively to the existing coordinates of the rectangle
        painter.setPen(QColor(181, 181, 181))
        painter.setBrush(Qt.NoBrush)
        #painter.drawRoundedRect(r, 15, 15, Qt.RelativeSize)
        #Draws inner rectangle rect with rounded corners.
        
        painter.setPen(QColor(159, 159, 159))
        y = rect.top() + rect.height()  - 1
        painter.drawLine(rect.left(), y, rect.right(), y)
        
        
        #Draws the mid-line from (rect.left(), y) to (rect.right(), y) and sets the current pen position to (rect.right(), y)
        
        
    def paintFlip(self,p):
        
        p.setBrush(Qt.NoBrush)
        p.setPen(Qt.NoPen)
        p.setRenderHint(QPainter.SmoothPixmapTransform, True)#像素光滑
        p.setRenderHint(QPainter.Antialiasing, True)#反锯齿
        
        pad = self.width() / 10
        hw = self.width()/2
        hh = self.height() - pad
        fr = self.rect().adjusted(pad, pad, -pad, -pad)#调整，march值为pad
        #self.drawFrame(p, fr)#画整个长方形

        index = self.m_animator.currentFrame()
        #return
        #画上部分的也页面翻转
        if (index <= 80):
            angle = -90 * index / 50#计算翻转的角度
#             print angle
#             h = abs((self.height() - pad)/2*(1-math.sin(angle)))
#             print h,h,h,h,h,h
#             p.resetTransform()
#             p.setClipRect(0, 0, self.width(), h)
#             self.drawFrame(p, fr)
            
            
            transform = QTransform()
            transform.translate(hw, hh)#中心点,原点
            transform.rotate(angle, Qt.XAxis)#绕X轴旋转角度
            #Rotates the coordinate system counterclockwise by angle about the X axis
            p.setTransform(transform)
            self.drawFrame(p, fr.adjusted(-hw, -hh, -hw, -hh))
        
    def paintEvent(self,event):
        print "00000000000"
        painter = QPainter(self)
        #painter.setPen(QPen(QColor(0,0,0,200),2))
        #painter.drawRect(self.march,self.march,self.inWidth,self.inHeight)
        self.second = QTime.currentTime().second()
        if (self.m_animator.state() == QTimeLine.Running):
            self.paintStatic(painter)
            self.paintFlip(painter)
        else:
            return

if __name__ == "__main__":
    app = QApplication(sys.argv)
    
    c = TurnPage()
    c.show()
    
    app.exec_()
        
