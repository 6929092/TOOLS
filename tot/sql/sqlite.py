#-*- coding: utf-8 -*-
from PyQt4.QtGui import * 
from PyQt4.QtCore import * 
from PyQt4.QtSql import * 

import sys 
#创建数据库连接
QTextCodec.setCodecForTr(QTextCodec.codecForName("utf8"))

class TestWidget(QObject): 
    def __init__(self,parent = None): 
        super(TestWidget,self).__init__(parent) 
        
        
    def runSql(self,string):
        self.list = []
        query = QSqlQuery()
        query.exec_(string)
        while query.next() :   #query.next()指向查找到的第一条记录，然后每次后移一条记录  
            ele1=query.value(0).toString()   
            print ele1
            self.list.append(ele1)
        print self.list
        #query.exec_("commit") 
        return self.list
    def addData(self,str1):
        self.q.exec_(str1)
        self.q.exec_("commit")  
    def createConnection(self): 
    #选择数据库类型，这里为sqlite3数据库
        db=QSqlDatabase.addDatabase("QSQLITE") 
        #创建数据库test0.db,如果存在则打开，否则创建该数据库
        db.setDatabaseName("test0.db") 
        #打开数据库
        db.open() 
        #创建QsqlQuery对象，用于执行sql语句
        self.q=QSqlQuery() 
        self.q.exec_("create table if not exists t1 (f1 integer primary key,f2 varchar(20),f3 varchar(20),f4 integer,f5 integer,f6 integer)")
        self.q.exec_("delete from t1") 
        #这里使用 u 将字符串转换成unicode编码，解决中文乱码
        
        
        self.q.exec_("insert into t1(f1,f2,f3,f4,f5,f6) values(1,u'过','000',10,10,10)") 
        self.q.exec_("insert into t1(f1,f2,f3,f4,f5,f6) values(2,u'过','111',5,10,10)") 
        self.q.exec_("insert into t1(f1,f2,f3,f4,f5,f6) values(3,u'过','222',15,10,10)") 
        self.q.exec_("insert into t1(f1,f2,f3,f4,f5,f6) values(4,u'过','333',6,10,10)") 
        



