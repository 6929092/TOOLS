#coding:utf-8
'''
Created on 2014年8月29日
@author: GuoWu
'''
import sys  
from PyQt4.QtGui import *
from PyQt4.QtCore import *


#include "CustomIconStyle.h"

# class CustomIconStyle(QCommonStyle):
#     def __init__(self):
#         super(CustomIconStyle,self).__init__()
#         
#         self.mSize = 20
# 
#     def setCustomSize(self,nSize):
#     
#         self.mSize = nSize
#     
#     
#     def pixelMetric(self,metric, option, widget):
#      
#         s = QCommonStyle.pixelMetric(metric, option, widget)
#         if (metric == QStyle.PM_SmallIconSize):
#             s = self.mSize
#              
#         return s

  
class ButtonMenu(QWidget):  
    def __init__(self,parent=None):
        super(ButtonMenu, self).__init__(parent)  
        self.createContextMenu() 
        self.setStyleSheet("QMenu{background:purple;}"
                           "QMenu{border:1px solid lightgray;}"
                           "QMenu{border-color:green;}"
                            "QMenu::item{padding:0px 40px 0px 20px;}"
                            "QMenu::item{height:50px;}"    
                            "QMenu::item{color:blue;}"
                            "QMenu::item{background:white;}"
                            "QMenu::item{margin:1px 0px 0px 0px;}"
                            "QMenu::item:selected:enabled{background:lightgray;}"
                            "QMenu::item:selected:enabled{color:white;}"   
                            "QMenu::item:selected:!enabled{background:transparent;}"
                            )
          
        
    def createContextMenu(self):  
        ''''' 
        创建菜单 
        '''  
        
        # 创建QMenu  
        self.contextMenu = QMenu(self)  
        
        pixmap = QPixmap("0.png").scaled(200,200)
        icon = QIcon(pixmap)
        
        #self.actionA=QAction(icon,self.tr("新建(&N)"),self);
        
        self.actionA = self.contextMenu.addAction(icon,u'动作A')  
        self.actionB = self.contextMenu.addAction(QIcon("0.png"),u'动作B')  
        self.actionC = self.contextMenu.addAction(QIcon("0.png"),u'动作C') 
         
        # 将动作与处理函数相关联  
        # 这里为了简单，将所有action与同一个处理函数相关联，  
        # 当然也可以将他们分别与不同函数关联，实现不同的功能  
        self.second = self.contextMenu.addMenu(QIcon("0.png"),u"二级菜单") 
        self.actionD = self.second.addAction(QIcon("0.png"),u'动作A')
        self.actionE = self.second.addAction(QIcon("0.png"),u'动作B')
        self.actionF = self.second.addAction(QIcon("0.png"),u'动作C')
        # 将动作与处理函数相关联  
        # 这里为了简单，将所有action与同一个处理函数相关联，  
        # 当然也可以将他们分别与不同函数关联，实现不同的功能  
        self.actionA.triggered.connect(self.actionHandler)  
        self.actionB.triggered.connect(self.actionHandler)  
        self.actionC.triggered.connect(self.actionHandler) 
        self.actionD.triggered.connect(self.actionHandler)  
        self.actionE.triggered.connect(self.actionHandler)  
        self.actionF.triggered.connect(self.actionHandler)  
        
        #pStyle = QCommonStyle() 
        
        #pStyle.setCustomSize(60)
        #self.contextMenu.setStyle(pStyle) 
        
    def actionHandler(self):
        pass
