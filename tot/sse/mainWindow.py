# -*- coding: utf-8 -*-  
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from slotSignal import SlotSignal
from rightMenu import RightMenu
from leftMenu import LeftMenu
import sys
import random
class PaintButton(QWidget):
    def __init__(self,parent=None):
        super(PaintButton,self).__init__(parent)
        self.setFixedSize(604,450)
        #self.setWindowFlags(Qt.Dialog|Qt.FramelessWindowHint)
        #self.setStyleSheet("QWidget{background:blue;}")
        self.slotSignal = SlotSignal(self)
        self.rightMenu = RightMenu(self)
        self.rightMenu2 = RightMenu(self)
        self.rightMenu2.restartButton.setText(self.trUtf8("两分钟"))
        self.rightMenu2.hide()
        self.rightMenu3 = RightMenu(self)
        self.rightMenu3.restartButton.setText(self.trUtf8("五分钟"))
        self.leftMenu = LeftMenu(self)
        self.leftMenu2 = LeftMenu(self)
        self.leftMenu2.hide()
        self.leftMenu3 = LeftMenu(self)
        #self.leftMenu3.hide()
        self.countNum = 1
        
        self.slotSignal.move((self.geometry().width()-self.slotSignal.geometry().width())/2,(self.geometry().height()-self.slotSignal.geometry().height())/2)
        self.rightMenu.move(self.geometry().width()-110,(self.geometry().height()/2-self.rightMenu.geometry().height())/2)
        self.rightMenu2.move(self.geometry().width()-110,(self.geometry().height()-self.rightMenu.geometry().height())/2)
        self.rightMenu3.move(self.geometry().width()-110,(self.geometry().height()/2-self.rightMenu.geometry().height())/2 + self.geometry().height()/2)

        #self.slotSignal.move((self.geometry().width()-self.slotSignal.geometry().width()),(self.geometry().height()-self.slotSignal.geometry().height()))
        self.leftMenu.move(40,(self.geometry().height()/2-self.leftMenu.geometry().height())/2)
        self.leftMenu2.move(40,(self.geometry().height()-self.leftMenu.geometry().height())/2)
        self.leftMenu3.move(40,(self.geometry().height()/2-self.leftMenu.geometry().height())/2 + self.geometry().height()/2)

        self.connect(self.slotSignal,SIGNAL("paintButton"),self.slotTest )
        self.connect(self.slotSignal,SIGNAL("stop"),self.buttonStop)
        self.connect(self.rightMenu,SIGNAL("restart"),self.buttonStart)
        self.connect(self, SIGNAL("counter"),self.count)
    def count(self):
        countStr = QString().number(self.countNum)
        self.leftMenu.countLabel.setText(countStr)
    def slotTest(self,actionType):
        self.emit(SIGNAL("counter"))
        if actionType == "showFive" and self.slotSignal.testWidget.fList[4]:
            self.countNum+=1
            self.slotSignal.msTimer.start(10)
            #self.slotSignal.testWidget.button1.setIcon()
            self.randNum = random.randint(0,8)
            print self.randNum
            while self.randNum == 4:
                self.randNum = random.randint(0,8)
            for n in range(0,9):
                if n != self.randNum :
                    self.slotSignal.testWidget.buttonList[n].setIcon(QIcon())
                    self.slotSignal.testWidget.buttonList[n].setIconSize(QSize())
                else:
                    self.slotSignal.testWidget.buttonList[n].setIcon(QIcon("dsaf.png"))
                    self.slotSignal.testWidget.buttonList[n].setIconSize(QSize(80,80))
                    self.slotSignal.testWidget.fList[4] = 0
                    self.slotSignal.testWidget.fList[self.randNum] = 1
                    
        elif actionType == "showFirst" and self.slotSignal.testWidget.fList[0]:
            self.countNum+=1
            #self.slotSignal.testWidget.button1.setIcon()
            self.randNum = random.randint(0,8)
            print self.randNum
            while self.randNum == 0:
                self.randNum = random.randint(0,8)
            for n in range(0,9):
                if n != self.randNum :
                    self.slotSignal.testWidget.buttonList[n].setIcon(QIcon())
                    self.slotSignal.testWidget.buttonList[n].setIconSize(QSize())
                else:
                    self.slotSignal.testWidget.buttonList[n].setIcon(QIcon("dsaf.png"))
                    self.slotSignal.testWidget.buttonList[n].setIconSize(QSize(80,80))
                    self.slotSignal.testWidget.fList[0] = 0
                    self.slotSignal.testWidget.fList[self.randNum] = 1
                    
        elif actionType == "showSecond" and self.slotSignal.testWidget.fList[1]:
            self.countNum+=1
            #self.slotSignal.testWidget.button1.setIcon()
            self.randNum = random.randint(0,8)
            print self.randNum
            while self.randNum == 1:
                self.randNum = random.randint(0,8)
            for n in range(0,9):
                if n != self.randNum :
                    self.slotSignal.testWidget.buttonList[n].setIcon(QIcon())
                    self.slotSignal.testWidget.buttonList[n].setIconSize(QSize())
                else:
                    self.slotSignal.testWidget.buttonList[n].setIcon(QIcon("dsaf.png"))
                    self.slotSignal.testWidget.buttonList[n].setIconSize(QSize(80,80))
                    self.slotSignal.testWidget.fList[1] = 0
                    self.slotSignal.testWidget.fList[self.randNum] = 1
                    
        elif actionType == "showThird" and self.slotSignal.testWidget.fList[2]:
            self.countNum+=1
            #self.slotSignal.testWidget.button1.setIcon()
            self.randNum = random.randint(0,8)
            print self.randNum
            while self.randNum == 2:
                self.randNum = random.randint(0,8)
            for n in range(0,9):
                if n != self.randNum :
                    self.slotSignal.testWidget.buttonList[n].setIcon(QIcon())
                    self.slotSignal.testWidget.buttonList[n].setIconSize(QSize())
                else:
                    self.slotSignal.testWidget.buttonList[n].setIcon(QIcon("dsaf.png"))
                    self.slotSignal.testWidget.buttonList[n].setIconSize(QSize(80,80))
                    self.slotSignal.testWidget.fList[2] = 0
                    self.slotSignal.testWidget.fList[self.randNum] = 1
                    
        elif actionType == "showFour" and self.slotSignal.testWidget.fList[3]:
            self.countNum+=1
            #self.slotSignal.testWidget.button1.setIcon()
            self.randNum = random.randint(0,8)
            print self.randNum
            while self.randNum == 3:
                self.randNum = random.randint(0,8)
            for n in range(0,9):
                if n != self.randNum :
                    self.slotSignal.testWidget.buttonList[n].setIcon(QIcon())
                    self.slotSignal.testWidget.buttonList[n].setIconSize(QSize())
                else:
                    self.slotSignal.testWidget.buttonList[n].setIcon(QIcon("dsaf.png"))
                    self.slotSignal.testWidget.buttonList[n].setIconSize(QSize(80,80))
                    self.slotSignal.testWidget.fList[3] = 0
                    self.slotSignal.testWidget.fList[self.randNum] = 1
                    
        elif actionType == "showSix" and self.slotSignal.testWidget.fList[5]:
            self.countNum+=1
            #self.slotSignal.testWidget.button1.setIcon()
            self.randNum = random.randint(0,8)
            print self.randNum
            while self.randNum == 5:
                self.randNum = random.randint(0,8)
            for n in range(0,9):
                if n != self.randNum :
                    self.slotSignal.testWidget.buttonList[n].setIcon(QIcon())
                    self.slotSignal.testWidget.buttonList[n].setIconSize(QSize())
                else:
                    self.slotSignal.testWidget.buttonList[n].setIcon(QIcon("dsaf.png"))
                    self.slotSignal.testWidget.buttonList[n].setIconSize(QSize(80,80))
                    self.slotSignal.testWidget.fList[5] = 0
                    self.slotSignal.testWidget.fList[self.randNum] = 1
                   
        elif actionType == "showSeven" and self.slotSignal.testWidget.fList[6]:
            self.countNum+=1
            #self.slotSignal.testWidget.button1.setIcon()
            self.randNum = random.randint(0,8)
            print self.randNum
            while self.randNum == 6:
                self.randNum = random.randint(0,8)
            for n in range(0,9):
                if n != self.randNum :
                    self.slotSignal.testWidget.buttonList[n].setIcon(QIcon())
                    self.slotSignal.testWidget.buttonList[n].setIconSize(QSize())
                else:
                    self.slotSignal.testWidget.buttonList[n].setIcon(QIcon("dsaf.png"))
                    self.slotSignal.testWidget.buttonList[n].setIconSize(QSize(80,80))
                    self.slotSignal.testWidget.fList[6] = 0
                    self.slotSignal.testWidget.fList[self.randNum] = 1
                    
        elif actionType == "showEight" and self.slotSignal.testWidget.fList[7]:
            self.countNum+=1
            #self.slotSignal.testWidget.button1.setIcon()
            self.randNum = random.randint(0,8)
            print self.randNum
            while self.randNum == 7:
                self.randNum = random.randint(0,8)
            for n in range(0,9):
                if n != self.randNum :
                    self.slotSignal.testWidget.buttonList[n].setIcon(QIcon())
                    self.slotSignal.testWidget.buttonList[n].setIconSize(QSize())
                else:
                    self.slotSignal.testWidget.buttonList[n].setIcon(QIcon("dsaf.png"))
                    self.slotSignal.testWidget.buttonList[n].setIconSize(QSize(80,80))
                    self.slotSignal.testWidget.fList[7] = 0
                    self.slotSignal.testWidget.fList[self.randNum] = 1
                   
        elif actionType == "showNine" and self.slotSignal.testWidget.fList[8]:
            self.countNum+=1
            #self.slotSignal.testWidget.button1.setIcon()
            self.randNum = random.randint(0,8)
            print self.randNum
            while self.randNum == 8:
                self.randNum = random.randint(0,8)
            for n in range(0,9):
                if n != self.randNum :
                    self.slotSignal.testWidget.buttonList[n].setIcon(QIcon())
                    self.slotSignal.testWidget.buttonList[n].setIconSize(QSize())
                else:
                    self.slotSignal.testWidget.buttonList[n].setIcon(QIcon("dsaf.png"))
                    self.slotSignal.testWidget.buttonList[n].setIconSize(QSize(80,80))
                    self.slotSignal.testWidget.fList[8] = 0
                    self.slotSignal.testWidget.fList[self.randNum] = 1
                    
        else:
            pass
        
    def buttonStop(self):
        for n in range(0,9):
            self.slotSignal.testWidget.fList[n] = 0
            
    def buttonStart(self):
        for n in range(0,9):
            self.slotSignal.testWidget.fList[n] = 0
            self.slotSignal.testWidget.buttonList[n].setIcon(QIcon())
        self.countNum = 1
        self.leftMenu.countLabel.setText(u"0")
        self.slotSignal.countTemp = 0
        self.slotSignal.secondTemp = 0
        self.slotSignal.minuteTemp = 0
        self.slotSignal.hourTemp = 0
        self.slotSignal.lineCount.setText(u"0")
        self.slotSignal.lineHour.setText(u"0")
        self.slotSignal.lineMinute.setText(u"0")
        self.slotSignal.lineSecond.setText(u"0")
        self.slotSignal.msTimer.stop()
        self.slotSignal.testWidget.fList[4] = 1
        self.slotSignal.testWidget.buttonList[4].setIcon(QIcon("dsaf.png"))
        self.slotSignal.testWidget.buttonList[4].setIconSize(QSize(80,80))
        #elif actionType == "showFirst":
            
#app = QApplication(sys.argv)    
#c = PaintButton()
#c.show()
#app.exec_()
        
        