# -*- coding: utf-8 -*-
from PyQt4.QtGui import *
from PyQt4.QtCore import *
import sys

QTextCodec.setCodecForTr(QTextCodec.codecForName("utf8"))

class MainWindow(QMainWindow):
    def __init__(self,parent=None):
        super(MainWindow,self).__init__(parent)
        self.setWindowTitle(self.tr("工具栏插入控件"))
        self.text = QTextEdit()
        self.text.setReadOnly(True)
        self.setCentralWidget(self.text);
        
        self.ToolBar=self.addToolBar("Contral")
        
        self.label1 = QLabel(self.tr("ctrl1:"))
        self.box = QComboBox()
        self.box.insertItem(0,self.tr("ComboBox 1"))
        self.box.insertItem(1,self.tr("ComboBox 2"))
        self.box.insertItem(2,self.tr("ComboBox 3"))
        
        self.ToolBar.addWidget(self.label1)
        self.ToolBar.addWidget(self.box)
        self.ToolBar.addSeparator()
        
        self.label2 = QLabel(self.tr("ctrl2:"))
        self.spin = QSpinBox()
        self.spin.setRange(1,10)
        self.ToolBar.addWidget(self.label2)
        self.ToolBar.addWidget(self.spin)
        
        self.doc = QString()
        self.spinStr = QString()
        
        self.box.currentIndexChanged.connect(self.slotComboBox)
        self.spin.valueChanged.connect(self.slotSpinBox)
    
    def slotComboBox(self):
        self.combo = str(self.box.currentIndex() + 1  )
        self.doc = "QComboBox:"  + self.combo + "\n" + "QSpinBox:" + self.spinStr.setNum(self.spin.value())
        self.text.setText(self.doc)
            
    def slotSpinBox(self):
        doc = "QComboBox:" + str(self.box.currentIndex() + 1) + "\n" + "QSpinBox:" + self.spinStr.setNum(self.spin.value())
        self.text.setText(doc)

            
app=QApplication(sys.argv)
main=MainWindow()
main.show()
app.exec_()
