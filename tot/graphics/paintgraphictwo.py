#coding:utf-8
'''
Created on 2015��4��23��

@author: guowu
'''
from PyQt4.QtGui import QWidget, QPalette, QLabel, QApplication, QPixmap,QPushButton,\
    QHBoxLayout, QVBoxLayout, QIcon
import sys
from PyQt4.QtCore import QPoint, QTimeLine, SIGNAL, QSize

#from slideGraphic import SlideGraphic
from graphicstwo import SlideGraphic
from PyQt4.QtCore import Qt

class ToolButton(QWidget):
    def __init__(self,parent=None):
        super(ToolButton,self).__init__(parent)
        self.resize(QSize(280,60))
        #self.setWindowFlags(Qt.FramelessWindowHint)
        
        self.leftButton = QPushButton(self)
        self.leftButton.setFixedSize(QSize(40,40))
        self.leftButton.setIcon(QIcon("icons/go-back.png"))
        self.leftButton.setIconSize(QSize(40,40))
        
        self.rightButton = QPushButton(self)
        self.rightButton.setFixedSize(QSize(40,40))
        self.rightButton.setIcon(QIcon("icons/go-forward.png"))
        self.rightButton.setIconSize(QSize(40,40))
        
        self.leftButton.move(QPoint(-1,10))
        self.rightButton.move(QPoint(241,10))
        
        self.labelWidget = SlideGraphic(0,self)
        self.labelWidget.setCenterPoint(QPoint(140,30))
        self.labelWidget.lower()
        self.connect(self.labelWidget, SIGNAL("labelclicked"),self.slotLabelClicked)
        
        self.connect(self.leftButton, SIGNAL("clicked()"),self.slotLeftButtonClicked)
        self.connect(self.rightButton, SIGNAL("clicked()"),self.slotRightButtonClicked)
    
    def slotLabelClicked(self,index):
        self.index = index
        self.emit(SIGNAL("labelclicked"),index)
        
    def slotLeftButtonClicked(self):
        self.emit(SIGNAL("leftbuttonclicked"))
    
    def slotRightButtonClicked(self):
        self.emit(SIGNAL("rightbuttonclicked"))


class PaintGraphic(QWidget):
    def __init__(self,parent=None):
        super(PaintGraphic,self).__init__(parent)
        self.setFixedSize(300,500)
        
        self.slideGraphic = SlideGraphic(1,self)
        self.slideGraphic.setCenterPoint(QPoint(150,250))
        self.slideGraphic.move(QPoint(0,50))
        self.buttonLayout = ToolButton(self)
        
        self.labelBackground = QLabel(self)
        pixmap = QPixmap("icons/surfacing.png").scaled(500,500)
        self.labelBackground.setPixmap(pixmap)
        
        self.lineTimer = QTimeLine()
        self.connect(self.lineTimer, SIGNAL("frameChanged(int)"), self.updatePosition)
        self.lineTimer.setDuration(600)
        self.lineTimer.setFrameRange(0, 300)
        self.lineTimer.setCurveShape(QTimeLine.EaseInCurve)
        self.labellist = []
        
        self.count = 0
        
        #self.initAllPicture()
#         self.mainLayout = QVBoxLayout(self)
#         self.mainLayout.addStretch()
#         self.mainLayout.addWidget(self.buttonLayout)

        self.buttonLayout.move(QPoint((self.width()-self.buttonLayout.width())/2,self.height()-self.buttonLayout.height()))
        self.buttonLayout.raise_()
        self.slideGraphic.raise_()
        
        self.connect(self.buttonLayout, SIGNAL("leftbuttonclicked"),self.slotLeftMove)
        self.connect(self.buttonLayout, SIGNAL("rightbuttonclicked"),self.slotRightMove)
        self.connect(self.buttonLayout, SIGNAL("labelclicked"),self.slotLabelClicked)
    
    def initAllPicture(self):
        piclist = ["images/brownies.jpg","images/chocolate.jpg","images/cookies.jpg","images/fish.jpg","images/mussels.jpg","images/pasta.jpg","images/pizza.jpg","images/puding.jpg","images/sushi.jpg","images/trouts.jpg"]
        
        for i in range(len(piclist)):
            picLabel = QLabel(self)
            pixmap = QPixmap(piclist[i])#.scaled(1440,900)
            picLabel.setPixmap(pixmap)
            self.labellist.append(picLabel)
        
        for i in range(len(self.labellist)):
            self.labellist[i].move(i*300+50,100)
        
        self.labelBackground.move(QPoint(0,0))
    def slotLabelClicked(self,index):
        if self.count > index:
            a = self.count - index
            flag = True
        elif self.count < index:
            a = index - self.count
            flag = False
        else:
            a = 0
            flag = True
        self.count = index
        self.slotMove(flag,a)
        
    def slotMove(self,flag,a):
        if self.lineTimer.state() != QTimeLine.NotRunning:
            return
        self.labelBackgroundPosX = self.labelBackground.pos().x()
        if not flag:
            self.perDirect = float(-20*a/300.00)
        else:
            self.perDirect = float(20*a/300.00)
        self.slideGraphic.likeLabelClicked(self.count)
        self.lineTimer.start()
        
    def slotLeftMove(self):
        if self.lineTimer.state() != QTimeLine.NotRunning:
            return
        self.count+=1
        if self.count > 9:
            self.count-=1
            return
        
        self.labelBackgroundPosX = self.labelBackground.pos().x()
        print self.labelBackgroundPosX
        #if self.labelBackgroundPosX <= 10:
            #return
        self.perDirect = float(-20/300.00)
        
        
        self.slideGraphic.likeLabelClicked(self.count)
        self.buttonLayout.labelWidget.likeLabelClicked(self.count)
        self.lineTimer.start()
        
    def slotRightMove(self):
        if self.lineTimer.state() != QTimeLine.NotRunning:
            return
        self.count-=1
        if self.count < 0:
            self.count+=1
            return
        print "moveright"
        self.labelBackgroundPosX = self.labelBackground.pos().x()
        print self.labelBackgroundPosX
        #if self.labelBackgroundPosX >= -10:
            #return
        self.perDirect = float(20/300.00)
        
        self.slideGraphic.likeLabelClicked(self.count)
        self.buttonLayout.labelWidget.likeLabelClicked(self.count)
        self.lineTimer.start()
        
    def startLineTimer(self):
        print "push"
        self.lineTimer.start()
         
    def updatePosition(self):
        print "00000000000000000000000"
        index = self.lineTimer.currentFrame()
        print index
        self.labelBackground.move(QPoint(self.labelBackgroundPosX + self.perDirect*index, 0)) 
        
        
if __name__ == "__main__":
    app = QApplication(sys.argv)
    mainwindow = PaintGraphic()
    mainwindow.show()
    app.exec_()