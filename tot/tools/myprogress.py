#coding:utf-8
'''
Created on 2015��3��17��

@author: guowu
'''
from PyQt4.QtGui import QWidget, QApplication, QPainter, QBrush, QPen, QLabel,\
    QFont, QConicalGradient, QColor, QRadialGradient
from PyQt4.QtCore import QObject, Qt, QTimer, SIGNAL, QPoint
import sys
class MyProgressBar(QWidget): 
    def __init__(self,parent=None):
        super(MyProgressBar,self).__init__(parent)
        self.setFixedSize(100,100)
        self.radio = 0
        self.timer = QTimer()
        self.count = 0
        #self.timer.start(100)
        self.connect(self.timer, SIGNAL("timeout()"), self.updateValue)
        self.label = QLabel(self)
        self.label.setFont(QFont("Arial",10,QFont.Bold))
        self.label.setFixedSize(self.width(),self.width())
        self.label.setText(str(0) + "%")
        self.label.setAlignment(Qt.AlignCenter)
    def updateValue(self):
        self.count += 1
        self.setValue(self.count)
    def setValue(self,value):
        
        self.radio = 5760/100*value
        if value >= 100:
            self.radio = 5760
            value = 100
        self.label.setText(str(value) + "%")
        self.label.setAlignment(Qt.AlignCenter)
        #self.label.setFont(QFont("Arial",14,QFont.Bold))
        self.repaint()
        
    def setSize(self,value):
        self.setFixedSize(value,value)
        self.label.setFixedSize(self.width(),self.width())
        self.label.setFont(QFont("Arial",self.width()/10,QFont.Bold))
        
    def paintEvent(self,event):
    
        paint=QPainter()
        paint.begin(self)
        
        #paint.setBrush(QBrush(Qt.red,Qt.SolidPattern))#���û�ˢ��ʽ 
        """
        #长方形
        paint.setPen(QPen(Qt.blue,4,Qt.DashLine)) #���û�����ʽ 
        paint.drawRect(20,20,160,160)
        """
        """
        #圆角矩形
        paint.setPen(QPen(Qt.blue,1,Qt.SolidLine)) #���û�����ʽ 
        paint.drawRoundRect(20,20,200,200,100,100)
        """
        width = self.width()
        if self.width() > self.height():
            width = self.height()
        #设置曲线光滑，去锯齿
        paint.setRenderHint(QPainter.Antialiasing, True)
        #绘制圆和椭圆
        #核心代码：
        #paint.setPen(QPen(Qt.blue,1, Qt.SolidLine))
        #paint.drawEllipse(0,0,width,width)#左上角坐标，和椭圆圆的长直径和短直径
        
        niceBlue = QColor(150, 150, 200)
        coneGradient = QConicalGradient(width/3 + width/6, width/3 + width/6, 0)#圆心，开始角度
        coneGradient.setColorAt(0.0, Qt.darkGray)
        coneGradient.setColorAt(0.2, niceBlue)
        coneGradient.setColorAt(0.5, Qt.white)
        coneGradient.setColorAt(1.0, Qt.darkGray)
        paint.setBrush(coneGradient)
        paint.drawEllipse(0,0,width,width)
        
        
        coneGradientover = QConicalGradient(width/3 + width/6, width/3 + width/6, 0)#圆心，开始角度
        coneGradientover.setColorAt(0.0, Qt.red)
        coneGradientover.setColorAt(0.25, Qt.blue)
        coneGradientover.setColorAt(0.5, Qt.red)
        coneGradientover.setColorAt(0.75, Qt.blue)
        coneGradientover.setColorAt(1.0, Qt.red)
        paint.setBrush(coneGradientover)
        paint.setPen(QPen(Qt.darkBlue,2,Qt.SolidLine))
        paint.drawPie(0,0,width,width,0,self.radio)
        
        haloGradient = QRadialGradient(width/3 + width/6, width/3 + width/6, width/6, width/3 + width/6, width/3 + width/6)
        haloGradient.setColorAt(0.0, Qt.lightGray)
        haloGradient.setColorAt(0.8, Qt.darkGray)
        haloGradient.setColorAt(0.9, Qt.white)
        haloGradient.setColorAt(1.0, Qt.darkRed)
        paint.setPen(Qt.NoPen)
        paint.setBrush(haloGradient)
        paint.drawEllipse(width/3,width/3,width/3,width/3)
        
        paint.end()
        
        self.label.move(QPoint(0,(width-self.label.height())/2))


if __name__=='__main__':
    app = QApplication(sys.argv)
    w = MyProgressBar()
    w.setSize(200)
    w.show()
    w.timer.start(100)
    #w.setValue(90)
    app.exec_()
