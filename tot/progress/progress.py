#coding:utf-8
'''
Created on 2014��12��27��

@author: guowu
'''
from PyQt4.QtGui import QWidget, QApplication, QProgressBar
import sys

class ProgressBarTest(QWidget):
    def __init__(self,parent=None):
        super(ProgressBarTest,self).__init__(parent)
        
        self.setFixedSize(500,500)
        self.progressBar = QProgressBar(self)
        
        self.progressBar.move(100,100)
        self.progressBar.setFixedWidth(210)
        self.progressBar.setValue(50)
        
        self.setStyleSheet("QProgressBar{"
                                        "border: 2px solid grey;"
                                        "border-radius: 5px;"
                                        "text-align: center;"
                                         "}")
        
        
        
        
"""        
QProgressBar {
     border: 2px solid grey;
     border-radius: 5px;
 }

 QProgressBar::chunk {
     background-color: #05B8CC;
     width: 20px;
 }
 
 self.setStyleSheet("QProgressBar{"
                                        "color:black;"
                                        "border: 1px solid #808080;"
                                        "text-align: center;"
                                        "background-color:white;"
                                         "}"
                            "QProgressBar::chunk {"
                                            
                                            "background-color:rgb(128,128,255);"
                                            "margin: 0.5px;"
                                            "}")

This leaves the text-align, which we customize by positioning the text in the center of the progress bar.

 QProgressBar {
     border: 2px solid grey;
     border-radius: 5px;
     text-align: center;
 }A margin can be included to obtain more visible chunks.

 



In the screenshot above, we use a margin of 0.5 pixels.

 QProgressBar::chunk {
     background-color: #CD96CD;
     width: 10px;
     margin: 0.5px;
 }
        
"""     
        
     
    
app = QApplication(sys.argv)
c = ProgressBarTest()
c.show()
app.exec_()   