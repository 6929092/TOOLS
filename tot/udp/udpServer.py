# -*- coding: utf-8 -*-
from PyQt4.QtGui import *
from PyQt4.QtCore import *
import sys
from PyQt4.QtNetwork import *
import binascii
import macaddr
import time
import socket

QTextCodec.setCodecForTr(QTextCodec.codecForName("utf8"))

class Udpserver(QDialog):
    def __init__(self,parent=None):
        super(Udpserver,self).__init__(parent)
        self.setWindowTitle(self.tr("UDP Server"))
        vbMain = QVBoxLayout(self)
        
        LableTimer=QLabel(self.tr("Timer:"))
        vbMain.addWidget(LableTimer)
        self.LineEditText=QLineEdit()
        vbMain.addWidget(self.LineEditText)
        self.PushButtonStart = QPushButton(self.tr("Start"))
        vbMain.addWidget(self.PushButtonStart)
        self.connect(self.PushButtonStart,SIGNAL("clicked()"),self.slotButton)
        self.port = 5555
        self.isStarted = False
        
        self.udpSocket = QUdpSocket(self)
        
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_RAW)
        self.sock.bind(('192.168.7.243', self.port))
        self.sock.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
        self.sock.setsockopt(socket.SOL_SOCKET,socket.SO_BROADCAST,1)        
        
        
        self.timer = QTimer(self)
        
        
        self.timer.timeout.connect(self.slotTimeout)
        
    
    def mac2Hex(self,macAddr):
        macArray = macAddr.split(':')
        i = 0
        hexMac = ""
        while i < 6:
            hexMac = hexMac + "%s" % macArray[i] 
            i+=1 
        return hexMac
    
    # BroadCast Msg
    def getBroadCastMsg(self,macAddr):  
        wolmsg = "00e06f689ce500e06f689ce50842FFFFFFFFFFFF"
        hexMac = self.mac2Hex(macAddr)
        #print hexMac
        i = 0
        while i < 16:       
            wolmsg = wolmsg + hexMac
            i+=1
        return wolmsg
  
    def startTerminal(self,macAddr):
        
        print "afdsae"
        msg = self.getBroadCastMsg(macAddr)
        print msg
        #print len(binascii.a2b_hex(msg))
        self.sock.sendto(binascii.a2b_hex(msg),('<broadcast>',self.port))
        #print self.udpSocket.writeDatagram(binascii.a2b_hex(msg),QHostAddress.Broadcast,self.port) 
    
    def slotTimeout(self):
        msg = self.LineEditText.text()
        length = 0
        if msg == "" :
            return
        length = self.udpSocket.writeDatagram(msg.toLatin1(),QHostAddress.Broadcast,self.port)
        if length != msg.length():
            return
        
    def slotButton(self):
        if self.isStarted == False: 
            self.isStarted = True      
            self.PushButtonStart.setText("Stop")
            self.timer.start(1) 
        else:
            self.isStarted = False
            self.PushButtonStart.setText("Start")
            self.timer.stop()
        
app=QApplication(sys.argv)
dialog=Udpserver()
#dialog.startTerminal(macaddr.macthree)
while True:
#     for i in range(100):
#         dialog.startTerminal(macaddr.macone)
#     for i in range(100):
#         dialog.startTerminal(macaddr.mactwo)
    
    dialog.startTerminal(macaddr.macthree)
    
    time.sleep(3)
    
dialog.show()
app.exec_()
