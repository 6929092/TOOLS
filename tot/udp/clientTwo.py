# -*- coding: utf-8 -*-  
from PyQt4.QtCore  import QObject, SIGNAL, QString,QCoreApplication
from PyQt4.QtNetwork import QUdpSocket,QHostAddress
import sys

class MultiBroadCastClient(QObject): 
    def __init__(self,parent = None):
        super(MultiBroadCastClient,self).__init__(parent)
        
        mcast_addr = QHostAddress("224.0.1.20")

        
        self.udpSocket = QUdpSocket(self)
        self.udpSocket.bind(5555)
        
        #self.udpSocket.setSocketOption(QAbstractSocket.MulticastLoopbackOption, 0)
        #self.udpSocket.joinMulticastGroup(mcast_addr)
        
        self.connect(self.udpSocket, SIGNAL("readyRead()"), self.dataReceived)
        self.data = QString()
        
    def dataReceived(self):
        while True:
            while self.udpSocket.hasPendingDatagrams():
                self.data = self.udpSocket.readDatagram(self.udpSocket.pendingDatagramSize())
                print self.data[0]

if __name__ == "__main__":
    app = QCoreApplication(sys.argv)
    broadcast = MultiBroadCastClient()
    broadcast.dataReceived()
    app.exec_()
