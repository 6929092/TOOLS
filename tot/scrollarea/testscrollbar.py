#coding:utf-8
'''
Created on 2015��8��3��

@author: guowu
'''
from PyQt4.QtGui import QWidget, QScrollArea, QVBoxLayout, QPushButton,\
    QApplication, QPainter, QBrush, QColor, QLinearGradient
from PyQt4.QtCore import QSize, Qt, SIGNAL
import sys


class TestScrollBar(QWidget):
    def __init__(self,parent=None):
        super(TestScrollBar,self).__init__(parent)
        
        self.setFixedSize(QSize(500,500))
        
        self.testWidget = TestWidget()
        
        self.scrollArea = QScrollArea()
        self.scrollArea.setWidget(self.testWidget)
        self.scrollArea.horizontalScrollBar().hide()
        
        self.nextButton = QPushButton()
        self.nextButton.setText(u"下")
        self.preButton = QPushButton(u"上")
        
        
        self.connect(self.nextButton, SIGNAL("clicked()"),self.slotNext)
        self.connect(self.preButton, SIGNAL("clicked()"),self.slotPre)
        
        
        self.mainLayout = QVBoxLayout()
        self.mainLayout.addWidget(self.preButton)
        self.mainLayout.addWidget(self.scrollArea)
        self.mainLayout.addWidget(self.nextButton)
        self.setLayout(self.mainLayout)
    
    def slotNext(self):
        
        value = self.scrollArea.verticalScrollBar().value()
        print value
        self.scrollArea.verticalScrollBar().setValue(value+20)
        
        value = self.scrollArea.verticalScrollBar().value()
        print value
        
    def slotPre(self):
        
        value = self.scrollArea.verticalScrollBar().value()
        print value
        self.scrollArea.verticalScrollBar().setValue(value-20)
    
class TestWidget(QWidget):
    def __init__(self,parent=None):
        super(TestWidget,self).__init__(parent)
        
        self.setFixedSize(QSize(500,400))
        
    def paintEvent(self,event):
        
        painter = QPainter(self)
        
        linearGradient = QLinearGradient(0,0,0,self.height())
        linearGradient.setColorAt(0, QColor(0,0,255))
        #linearGradient.setColorAt(0.1, QColor(6,88,200))
        linearGradient.setColorAt(1, QColor(200,150,255))
        painter.setBrush(QBrush(linearGradient))
        
        painter.setPen(Qt.NoPen)
        painter.drawRect(self.rect())

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window  = TestScrollBar()
    window.show()
    app.exec_()
    
    