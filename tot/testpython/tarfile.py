#
# 这里将一个文件树中的所有文件和子目录归档到一个tar归档文件，然后压缩 
import tarfile, os 
# compression表示压缩算法，gz表示gzip颜色，bz2表示bzip2压缩，
# 空字符串表示不压缩 
# folder_to_backup: 要归档的文件夹 
# dest_folder 表示目标文件夹 
def make_tar(folder_to_backup, dest_folder, compression = 'bz2'):
    # dest_ext 表示扩展名 
      if compression: 
        dest_ext = '.' + compression 
      else: 
        dest_ext = '' 
      arc_name = os.path.basename(folder_to_backup) 
      # dest_name 为目标文件名，dest_path 为目标文件路径 
      dest_name = '%s.tar%s' % (arc_name, dest_ext) 
      dest_path = os.path.join(dest_folder, dest_name) 
      # 压缩方法决定了open的第二个参数是 "w", 或"w:gz", 或"w:bz2"
      if compression: 
        dest_cmp = ':' + compression 
      else: 
        dest_cmp = ''
      out = tarfile.TarFile.open(dest_path, 'w' + dest_cmp)
      out.add(folder_to_backup, arc_name)
      out.close() 
      return dest_path 
dest_path = make_tar('d:/8 file_system', 'd:/') 
print(dest_path)