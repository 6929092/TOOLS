#coding:utf-8
'''
Created on 2015��4��16��

@author: guowu
'''
from PyQt4.QtGui import QWidget, QMovie, QLabel, QPainter, QFont, QColor, QPen,\
    QConicalGradient, QApplication, QRadialGradient, QGradient, QLinearGradient,\
    QPolygon, QBrush, QSizeGrip
from PyQt4.QtCore import QPoint, QTimer, SIGNAL, Qt, QTime, QString
import sys
import time
import math
class MyMainWindow(QWidget): 
    def __init__(self,parent=None):
        super(MyMainWindow,self).__init__(parent)
        self.setWindowFlags(Qt.FramelessWindowHint|Qt.Tool)
        self.setAttribute(Qt.WA_TranslucentBackground,True)
        sizeGrip=QSizeGrip(self)#右上角的可缩放图标
        self.resize(480,200)
        self.enterPose = False
        self.moveFlag = False
        self.mousePressed = False
        self.side = self.height()
        self.circleRadio = self.height()/25
        self.count = 0
        self.countPainter = 10
        #self.setWindowFlags(Qt.FramelessWindowHint|Qt.WindowStaysOnTopHint|Qt.SubWindow ) 
        self.updatePoint()
        
        self.type = "second"
        self.currentTime = 0
        self.timeTimer = QTimer()
        self.timeTimer.timeout.connect(self.update)
        self.timeTimer.start(10)
        
        self.cicleTimer = QTimer()
        self.cicleTimer.timeout.connect(self.movePoint)
        self.cicleTimer.start(100)
        
        self.cicleTimerT = QTimer()
        self.cicleTimerT.timeout.connect(self.movePointT)
        
    def movePoint(self):
        
        if self.count >= self.side/10*4 - self.circleRadio*4:
            self.cicleTimer.stop()
            self.cicleTimerT.start(100)
            
        midPoint = (self.firstUpPoint + self.firstDownPoint)/2
        print self.firstUpPoint
        
        cirlePointY = self.count + self.circleRadio*2 + self.firstUpPoint.y()
        
        cha = midPoint.y() - self.firstUpPoint.y() - self.circleRadio*2
        aa = (cirlePointY - midPoint.y())
        aaa = math.sqrt(aa*aa)
        
        self.countPainter = 255*(cha-aaa)/cha
        
        self.count+=1
        #aaa = (255-10)*(self.count - (self.pointRightDownIn.y()-self.pointRightUpIn.y())/2)/(self.pointRightDownIn.y()-self.pointRightUpIn.y())/2
        #self.countPainter = math.sqrt(aaa*aaa)
    def movePointT(self):
        self.count-=1
        if self.count <= 0:
            self.cicleTimerT.stop()
            self.cicleTimer.start(100)
        
            
        midPoint = (self.firstUpPoint + self.firstDownPoint)/2
        
        cirlePointY = self.count + self.circleRadio*2 + self.firstUpPoint.y()
        
        cha = midPoint.y() - self.firstUpPoint.y() - self.circleRadio*2
        aa = (cirlePointY - midPoint.y())
        aaa = math.sqrt(aa*aa)
        
        self.countPainter = 255*(cha-aaa)/cha
        
    def setTimeType(self,type):
        if type == "second":
            self.type = "second"
        elif type == "minute":
            self.type = "minute"
        elif type == "hour":
            self.type = "hour"
            
    def updatePoint(self):
        self.pointLeftUpOut = QPoint(self.side/10, self.side/10)
        self.pointLeftUpIn = QPoint(self.side/10*3, self.side/10*3)
        self.pointLeftDownOut = QPoint(self.side/10, self.side/10*9)
        self.pointLeftDownIn = QPoint(self.side/10*3, self.side/10*7)
        self.pointRightUpOut = QPoint(self.side/10*9*3 -self.side/10*4, self.side/10)
        self.pointRightUpIn = QPoint(self.side/10*9*3-self.side/10*6, self.side/10*3)
        self.pointRightDownOut = QPoint(self.side/10*9*3-self.side/10*4, self.side/10*9)
        self.pointRightDownIn = QPoint(self.side/10*9*3-self.side/10*6, self.side/10*7)
        
        self.firstUpPoint = QPoint(self.side/10*9, self.side/10*3)
        self.firstDownPoint = QPoint(self.side/10*9,self.side/10*7)
        self.secondUpPoint = QPoint(self.side/10*7*2+self.side/10,self.side/10*3)
        self.secondDownPoint = QPoint(self.side/10*7*2+self.side/10,self.side/10*7)
                
    def setSize(self,size):
        self.resize(size*2.4,size)
        self.side = self.height()
        self.updatePoint()
        self.update()
    def resizeEvent(self,event):
        self.side = self.height()
        self.circleRadio = self.height()/25
        self.updatePoint()
        self.update()
    
    def paintEvent(self,event):
        
        second = QTime.currentTime().second()
        minute = QTime.currentTime().minute()
        hour = QTime.currentTime().hour()
        currentTime = QString("%1:%2:%3").arg(hour).arg(minute).arg(second)
        paint=QPainter(self)
        paint.setRenderHint(QPainter.Antialiasing, True)
        self.drawRects(paint)
        self.paintCircle(paint)
        self.paintPoints(paint, self.count)
        #paint.drawEllipse(QRectF)
        #paint.scale(self.width()/200.0, self.width()/200.0)#使画面可以随着界面的变化可缩放
        
        font = QFont()
        font.setPointSize(self.height()/3)
        font.setFamily("Roman times")
        paint.setFont(font)
        if self.enterPose:
            paint.setPen(QPen(QColor(0,0,0,200),2))
        else:
            paint.setPen(QPen(QColor(0,0,0,255),2))
        #paint.drawText(0, 0, self.width(), self.height(), Qt.AlignCenter, str(currentTime))
        
        paint.setPen(QPen(QColor(250,20,20,20),1))
        paint.setBrush(Qt.NoBrush)
        paint.drawLine(self.firstUpPoint,self.firstDownPoint)
        paint.drawLine(self.secondUpPoint,self.secondDownPoint)
        paint.setBrush(Qt.NoBrush)
        paint.setPen(QPen(QColor(0,0,0,255),2))
        paint.drawText(self.pointLeftUpIn.x(), self.pointLeftUpIn.y(), self.firstDownPoint.x()-self.pointLeftUpIn.x(), self.firstDownPoint.y()-self.pointLeftUpIn.y(), Qt.AlignCenter, str(hour))
        paint.drawText(self.firstUpPoint.x(), self.firstUpPoint.y(), self.secondDownPoint.x()-self.firstUpPoint.x(), self.secondDownPoint.y()-self.firstUpPoint.y(), Qt.AlignCenter, str(minute))
        paint.drawText(self.secondUpPoint.x(), self.secondUpPoint.y(), self.pointRightDownIn.x()-self.secondUpPoint.x(), self.pointRightDownIn.y()-self.secondUpPoint.y(), Qt.AlignCenter, str(second))
        
        #paint.drawre
    def paintPoints(self,painter,count):
        firstUpCirclePoint = QPoint(self.firstUpPoint.x(),count + self.firstUpPoint.y())
        firstDownCirclePoint = QPoint(self.firstUpPoint.x(),count+self.circleRadio*4 + self.firstUpPoint.y())
        secondUpCirclePoint = QPoint(self.secondUpPoint.x(),count + self.firstUpPoint.y())
        secondDownCirclePoint = QPoint(self.secondUpPoint.x(),count+self.circleRadio*4 + self.firstUpPoint.y())
        
        
        #print self.countPainter
        painter.setBrush(QBrush(QColor(10,10,10,self.countPainter)))
        painter.setPen(Qt.NoPen)
        painter.drawEllipse(firstUpCirclePoint, self.circleRadio, self.circleRadio)
        painter.drawEllipse(firstDownCirclePoint, self.circleRadio, self.circleRadio)
        painter.drawEllipse(secondUpCirclePoint, self.circleRadio, self.circleRadio)
        painter.drawEllipse(secondDownCirclePoint, self.circleRadio, self.circleRadio)
        
    def drawRects(self,painter):
        painter.setBrush(QBrush(QColor(210,210,210,100)))            
        painter.setPen(Qt.NoPen)
        painter.drawRect(self.pointLeftUpIn.x()+2,self.pointLeftUpIn.y()+2,self.firstDownPoint.x()-self.pointLeftUpIn.x()-4,self.firstDownPoint.y()-self.pointLeftUpIn.y()-4)
        painter.drawRect(self.firstUpPoint.x()+2,self.firstUpPoint.y()+2,self.secondDownPoint.x()-self.firstUpPoint.x()-4,self.secondDownPoint.y()-self.firstUpPoint.y()-4)
        painter.drawRect(self.secondUpPoint.x()+2,self.secondUpPoint.y()+2,self.pointRightDownIn.x()-self.secondUpPoint.x()-4,self.pointRightDownIn.y()-self.secondUpPoint.y()-4)
        
    def paintCircle(self,painter):
        #print self.side
        coneGradient = QLinearGradient(self.side/10, self.side/10, self.side/10, self.side/10*3)
        coneGradient.setColorAt(0.0, QColor(0,0,0))
        coneGradient.setColorAt(0.3, QColor(100,100,100))
        coneGradient.setColorAt(1.0, QColor(255,205,205))
        painter.setBrush(coneGradient)            
        painter.setPen(QPen(QColor(100,211,255),1))
        painter.drawPolygon(QPolygon([self.pointLeftUpOut,self.pointLeftUpIn,self.pointRightUpIn,self.pointRightUpOut]))
        
        coneGradient = QLinearGradient(self.side/10, self.side/10, self.side/10*3, self.side/10)
        coneGradient.setColorAt(0.0, QColor(0,0,0))
        coneGradient.setColorAt(0.3, QColor(100,100,100))
        coneGradient.setColorAt(1.0, QColor(255,205,205))
        painter.setBrush(coneGradient)            
        painter.setPen(QPen(QColor(100,211,255),1))
        painter.drawPolygon(QPolygon([self.pointLeftUpIn,self.pointLeftUpOut,self.pointLeftDownOut,self.pointLeftDownIn]))
        
        coneGradient = QLinearGradient(self.side/10, self.side/10*9, self.side/10, self.side/10*7)
        coneGradient.setColorAt(0.0, QColor(0,0,0))
        coneGradient.setColorAt(0.3, QColor(100,100,100))
        coneGradient.setColorAt(1.0, QColor(255,205,205))
        painter.setBrush(coneGradient)            
        painter.setPen(QPen(QColor(100,211,255),1))
        painter.drawPolygon(QPolygon([self.pointLeftDownOut,self.pointLeftDownIn,self.pointRightDownIn,self.pointRightDownOut]))
         
        coneGradient = QLinearGradient(self.side/10*9*3-self.side/10*4, self.side/10, self.side/10*9*3-self.side/10*6, self.side/10)
        coneGradient.setColorAt(0.0, QColor(0,0,0))
        coneGradient.setColorAt(0.3, QColor(100,100,100))
        coneGradient.setColorAt(1.0, QColor(255,205,205))
        painter.setBrush(coneGradient)            
        painter.setPen(QPen(QColor(100,211,255),1))
        painter.drawPolygon(QPolygon([self.pointRightUpIn,self.pointRightUpOut,self.pointRightDownOut,self.pointRightDownIn]))
        
        
    def mouseMoveEvent(self,event):
        if self.mousePressed:
            self.move(self.pos() + event.pos() - self.currentPos)
            self.moveFlag = True
            pass
   
    def mousePressEvent(self,event):
        print "press"
        self.emit(SIGNAL("mousepressed"),event.pos())
        print event.buttons()
        if event.buttons() == Qt.LeftButton:
            self.currentPos = event.pos()
            print "444444"
            self.mousePressed = True
   
    def mouseReleaseEvent(self,event):
        self.emit(SIGNAL("mousereleased"))
        if self.mousePressed:
            self.mousePressed = False
        else:
            pass
        
    def enterEvent(self, event):
        self.enterPose = True
        self.textPose = True
        self.emit(SIGNAL("enterwidget"))
    
    def leaveEvent(self, event):
        self.enterPose = False
        self.textPose = False
        self.emit(SIGNAL("leavewidget"))
        
class MainWindow(QWidget):
    def __init__(self, parent=None):
        super(MainWindow,self).__init__(parent)
        

if __name__ == "__main__":
    app = QApplication(sys.argv)
    c = MyMainWindow()
    c.setSize(200)
    c.show()
    app.exec_()