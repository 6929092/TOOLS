#coding:utf-8
'''
Created on 2015��7��15��

@author: guowu
'''

from PyQt4.QtGui import QWidget, QTextEdit, QPushButton, QGraphicsScene,\
    QGraphicsWidget, QGraphicsLinearLayout, QGraphicsView, QApplication,\
    QTransform, QHBoxLayout, QPainter, QLabel, QGraphicsLayoutItem, QFont,\
    QPixmap
from PyQt4.QtCore import Qt, QTime, QCoreApplication, QEventLoop, QObject,\
    SIGNAL, QPoint, QTimer, QBasicTimer, QElapsedTimer
import sys
import time


class TestGraphicWidget(QGraphicsWidget):
    def __init__(self,parent=None):
        super(TestGraphicWidget,self).__init__(parent)
        self.setWindowFlags(Qt.Window)
        self.setWindowTitle("Widget Item")
        self.resize(500,500)
        self.pix = QPixmap("login.jpg").scaled(500,500)
        
        
    def closeEvent(self,event):
        print "closeclosetest"
        self.emit(SIGNAL("closeall"))
        
    def paintEvent(self,event):
        print "paintevent"
        painter = QPainter(self)
        #painter.drawPixmap(QPoint(0,0), self.pix)
        
        
def testGraphicScene(QGraphicsScene):
    def __init__(self,parent=None):
        super(testGraphicScene,self).__init__(parent)
        
        #创建部件，并关联它们的信号和槽
        self.edit = QLabel(u"0000000000000000000")
        self.button = QPushButton("clear")
        
        #self.connect(self.button, SIGNAL("clicked()"), self.closeAll)
        #将部件添加到场景中
        self.textEdit = self.addWidget(self.edit)
        self.pushButton = self.addWidget(self.button)
        
        self.form = TestGraphicWidget()
        
        
        #将部件添加到布局管理器中
        layout = QGraphicsLinearLayout(self.form)
        layout.addItem(self.textEdit)
        layout.addItem(self.pushButton)
        
        
        #创建图形部件，设置其为一个顶层窗口，然后在其上应用布局
        
        self.form.setWindowFlags(Qt.Window)
        self.form.setWindowTitle("Widget Item")
        self.form.setLayout(layout)
        
        self.addItem(self.form)


class TestMainWindow(QWidget):
    def __init__(self,parent=None):
        super(TestMainWindow,self).__init__(parent)
        self.firstButton = QPushButton(u"翻转")
        self.secondButton = QPushButton(u"翻转")
        self.thirdButton = QPushButton(u"翻转")
        
        self.mainLayout = QHBoxLayout(self)
        self.mainLayout.addWidget(self.firstButton)
        self.mainLayout.addWidget(self.secondButton)
        self.mainLayout.addWidget(self.thirdButton)
        
        self.connect(self.firstButton, SIGNAL("clicked()"), self.closeAll)
        self.connect(self.secondButton, SIGNAL("clicked()"), self.closeAll)
        self.connect(self.thirdButton, SIGNAL("clicked()"), self.closeAll)
    
    def closeAll(self):
        self.emit(SIGNAL("buttonclicked"))


class MainWindow(QWidget):
    def __init__(self,parent=None):
        super(MainWindow,self).__init__(parent)
        
        #self.setStyleSheet("QGraphicsView{background:rgb(0,0,0,0);border:0px;}")
        
        
        self.formflag = 0
        
        self.scene = QGraphicsScene()
        
        self.setWindowFlags(Qt.FramelessWindowHint|Qt.WindowStaysOnTopHint)
        self.setAttribute(Qt.WA_TranslucentBackground,True)
        
        #创建部件，并关联它们的信号和槽
        self.editthree = QLabel(u"0000000000000000000")
        self.edit = QLabel(u"0000000000000000000")
        self.edittwo = QLabel(u"111111111111111111")
        self.button = QPushButton(u"翻滚吧")
        self.connect(self.button, SIGNAL("clicked()"), self.closeAll)
        #将部件添加到场景中
        self.textEdit = self.scene.addWidget(self.edit)
        #self.textEdit = QGraphicsLayoutItem(self.edit)
        self.pushButton = self.scene.addWidget(self.button)
        self.textEditTwo = self.scene.addWidget(self.edittwo)
        self.textEditThree = self.scene.addWidget(self.editthree)
         
        self.form = TestGraphicWidget()
        
        self.connect(self.form, SIGNAL("closeall"),self.close)
        #将部件添加到布局管理器中
        self.layout = QGraphicsLinearLayout(self.form)
        self.layout.addItem(self.textEdit)
        self.layout.addItem(self.textEditTwo)
        self.layout.addItem(self.pushButton)
        self.layout.addItem(self.textEditThree)
        
        self.edit.hide()
        self.editthree.hide()
        self.layout.removeItem(self.textEditTwo)
        self.layout.removeItem(self.pushButton)
        self.layout.removeItem(self.textEdit)
        self.edittwo.move(QPoint(100,100))
        
        
         
        #创建图形部件，设置其为一个顶层窗口，然后在其上应用布局
         
        #self.form.setWindowFlags(Qt.Window|Qt.FramelessWindowHint)
        #self.form.setWindowTitle("Widget Item")
        #self.form.setLayout(layout)
         
        self.scene.addItem(self.form)
        
        
        #self.form.hide()
        
        self.view = QGraphicsView(self)
        self.view.setScene(self.scene)
        self.view.setRenderHint(QPainter.Antialiasing)
        self.view.setViewportUpdateMode(QGraphicsView.BoundingRectViewportUpdate)
        self.view.resize(1440,900)
        self.view.setStyleSheet("background: transparent;border:0px;")
        self.view.setWindowFlags(Qt.FramelessWindowHint)            
        self.view.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.view.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.view.move(QPoint(0,0))
        #self.view.setAttribute(Qt.WA_TranslucentBackground,True)
        
        #self.form.resize(500,500)
        #self.form.setWindowFlags(Qt.FramelessWindowHint)
        #for(int i=1;i<=360;i++)
    
    def setOne(self):
        
        self.edittwo.hide()
        self.edit.show()
        #self.layout.removeItem(self.textEditTwo)
        #self.layout.addItem(self.textEdit)
        self.edit.move(QPoint(100,100))
        self.button.move(QPoint(100,200))
        
        self.view.update()
        return
        
        
        self.layout.removeItem(self.textEdit)
        self.layout.removeItem(self.textEditTwo)
        self.layout.removeItem(self.pushButton)
        
        self.edittwo.hide()
        
        self.edit.move(QPoint(0,0))
        self.button.move(QPoint(100,200))
        
        self.view.update()
    
    def setTwo(self):
        
        self.edit.hide()
        self.edittwo.show()
        #self.layout.removeItem(self.textEdit)
        #self.layout.addItem(self.textEditTwo)
        self.edittwo.move(QPoint(100,100))
        self.button.move(QPoint(100,200))
        
        self.view.update()
        return
        
        self.layout.removeItem(self.textEdit)
        self.layout.removeItem(self.textEditTwo)
        self.layout.removeItem(self.pushButton)
        self.edit.hide()
        
        self.edittwo.move(QPoint(100,100))
        self.button.move(QPoint(100,200))
        
        self.form.resize(500,500)
        
        self.view.update()
        
    def transeformT(self,count):
        
        r = self.form.boundingRect()
        for i in range(1,count):
            print "............."
            self.form.setTransform(QTransform()
                                   .translate(r.width() / 2, r.height() / 2)
                                   .rotate(364.00/count*i - 360 * 1, Qt.YAxis)
                                   .translate(-r.width() / 2, -r.height() / 2))
            self.waitMethod()
            #self.sleep(1)
            #time.sleep(1)
            self.view.update()
#             if i == 180:
#                 self.form.close()
#                 self.view.close()
#                 break
                #self.close()
                
        #self.closeAll()
        
    def transeformS(self,count):
        r = self.form.boundingRect()
        for i in range(1,count):
            print "............."
            self.form.setTransform(QTransform()
                                   .translate(r.width() / 2, r.height() / 2)
                                   .rotate(182.00/count*i - 360 * 1, Qt.YAxis)
                                   .translate(-r.width() / 2, -r.height() / 2))
            self.waitMethod()
            self.view.update()
            
    def transeformR(self,count):
        r = self.form.boundingRect()
        for i in range(1,count):
            print "............."
            self.form.setTransform(QTransform()
                                   .translate(r.width() / 2, r.height() / 2)
                                   .rotate(91.00/count*i - 360 * 1, Qt.YAxis)
                                   .translate(-r.width() / 2, -r.height() / 2))
            self.waitMethod()
            self.view.update()
        
        self.form.setTransform(QTransform()
                                   .translate(r.width() / 2, r.height() / 2)
                                   .rotate(270 - 360 * 1, Qt.YAxis)
                                   .translate(-r.width() / 2, -r.height() / 2))
        self.view.update()
        if self.formflag %2 == 0:
            self.setTwo()
        else:
            self.setOne()
        
        for i in range(1,count):
            self.form.setTransform(QTransform()
                                       .translate(r.width() / 2, r.height() / 2)
                                       .rotate(270 + 93.00/count*i - 360 * 1, Qt.YAxis)
                                       .translate(-r.width() / 2, -r.height() / 2))
            self.waitMethod()
            self.view.update()
            
            
            
    
    def transeformB(self,count):
        r = self.form.boundingRect()
        for i in range(1,count):
            print "............."
            self.form.setTransform(QTransform()
                                   .translate(r.width(), r.height())
                                   .rotate(91.00/count*i - 360 * 1, Qt.YAxis)
                                   .translate(-r.width(), -r.height()))
            self.waitMethod()
            self.view.update()
        
        self.form.setTransform(QTransform()
                                   .translate(r.width(), r.height())
                                   .rotate(270 - 360 * 1, Qt.YAxis)
                                   .translate(-r.width(), -r.height()))
        self.view.update()
        
        
        for i in range(1,count):
            self.form.setTransform(QTransform()
                                       .translate(r.width(), r.height())
                                       .rotate(270 + 93.00/count*i - 360 * 1, Qt.YAxis)
                                       .translate(-r.width(), -r.height()))
            self.waitMethod()
            self.view.update()
            
            
        
    def transeform(self):
        r = self.form.boundingRect()
        for i in range(1,361):
            print "............."
            self.form.setTransform(QTransform()
                                   .translate(r.width() / 2, r.height() / 2)
                                   .rotate(i - 360 * 1, Qt.YAxis)
                                   .translate(-r.width() / 2, -r.height() / 2))
            self.waitMethod()
            self.view.update()
#             
    def closeAll(self):
        self.formflag += 1
        c.transeformR(30)
        #self.form.close()
        #self.view.close()
    def closeEvent(self,event):
        print "close"
        self.form.close()
        self.view.close()
        self.close()
        
        
        
    def sleep(self,msec):
        
        dieTime = QTime.currentTime().addMSecs(msec)
        
        print dieTime,QTime.currentTime()
        #a = 0
        while( QTime.currentTime() < dieTime ):
            #print "000000000000"
            QCoreApplication.processEvents(QEventLoop.AllEvents, 100)
            
    
    def waitMethod(self):
        #tt = QElapsedTimer()
        #tt.start()
        q = QEventLoop()
        t = QTimer()
        t.setSingleShot(True)
        self.connect(t, SIGNAL("timeout()"), q.quit)
        t.start(1)   # 5s timeout
        q.exec_()
        if(t.isActive()):
            t.stop()
        else:
            pass
        
        #print tt.elapsed()
         


if __name__ == "__main__":
    app = QApplication(sys.argv)
    
    font = QFont()
    font.setPointSize(16)
    font.setFamily(("songti"))
    app.setFont(font)
    
    c = MainWindow()
    c.show()
    c.move(QPoint(0,0))
    app.exec_()
        
        
