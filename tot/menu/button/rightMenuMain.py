#coding:utf-8
'''
Created on 2014��8��26��

@author: GuoWu
'''
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from buttonRightMenu import ButtonMenu
import sys
QTextCodec.setCodecForTr(QTextCodec.codecForName("utf8"))

class TestDialog(QDialog):
    def __init__(self,parent=None):
        super(TestDialog,self).__init__(parent)
        self.mainButton = ButtonMenu(self)
        self.mainButton.move(100,100)

app = QApplication(sys.argv)  
window = TestDialog()  
window.show()  
sys.exit(app.exec_())  