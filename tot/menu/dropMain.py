#coding:utf-8
'''
Created on 2014��8��26��

@author: GuoWu
'''
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from dropmenu import ButtonMenu
import sys
QTextCodec.setCodecForTr(QTextCodec.codecForName("utf8"))

class TestDialog(QDialog):
    def __init__(self,parent=None):
        super(TestDialog,self).__init__(parent)
        self.resize(500,500)
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.count = 0
        self.setStyleSheet("QPushButton{border:1px;}"
                           "QPushButton:hover{background:blue;}"
                           "QPushButton:pressed{background:black;}")
        
        self.buttonMenu = ButtonMenu(self)
        
        
        self.menuButton = QPushButton(self)
        self.menuButton.setFixedSize(30,30)
        self.menuButton.setIcon(QIcon("dropArrow.png"))
        self.menuButton.setIconSize(QSize(25,25))
        
        self.miniButton = QPushButton(self)
        self.miniButton.setFixedSize(30,30)
        self.miniButton.setIcon(QIcon("mini.png"))
        self.miniButton.setIconSize(QSize(25,25))
        
        self.maxButton = QPushButton(self)
        self.maxButton.setFixedSize(30,30)
        self.maxButton.setIcon(QIcon("max.png"))
        self.maxButton.setIconSize(QSize(25,25))
        
        self.closeButton = QPushButton(self)
        self.closeButton.setFixedSize(30,30)
        self.closeButton.setIcon(QIcon("close.png"))
        self.closeButton.setIconSize(QSize(25,25))
        
        self.pixmovie = QMovie("ouou.gif")
        self.label = QLabel(self)
        #self.label.setFixedSize(300,300)
        self.label.setMovie(self.pixmovie)
        self.pixmovie.start()
        
        self.connect(self.menuButton, SIGNAL("clicked()"),self.showMenu)
        self.connect(self.miniButton, SIGNAL("clicked()"),self.dlgMini)
        self.connect(self.maxButton, SIGNAL("clicked()"),self.dlgMax)
        self.connect(self.closeButton, SIGNAL("clicked()"),self.closeDlg)
    def dlgMini(self):
        self.showMinimized()
        
    def dlgMax(self):
        self.count+=1
        if self.count%2 != 0:
            self.showMaximized()
            self.maxButton.setIcon(QIcon("to.png"))
            self.maxButton.setIconSize(QSize(25,25))
        else:
            desktop = QApplication.desktop()
            self.showNormal()
            self.maxButton.setIcon(QIcon("max.png"))
            self.maxButton.setIconSize(QSize(25,25))
            self.move((desktop.width()-self.width())/2,(desktop.height()-self.height())/2)
    def closeDlg(self):
        self.close()
    def showMenu(self):
        print "clicked"
        print QString().number(self.pos().x())
        self.buttonMenu.contextMenu.show()
        self.buttonMenu.contextMenu.move(self.pos().x()+self.width()-120,self.pos().y()+40)
    def mouseMoveEvent(self,event):
        if self.mousePressed:
            self.move(self.pos() + event.pos() - self.currentPos)
   
    def mousePressEvent(self,event):
        if event.buttons() == Qt.LeftButton:
            self.currentPos = event.pos()
            self.mousePressed = True
   
    def mouseReleaseEvent(self,event):
        if event.buttons() == Qt.LeftButton:
            self.mousePressed = False    
        
    def paintEvent(self,event):
        self.menuButton.move(self.width()-120,0)
        self.miniButton.move(self.width()-90,0)
        self.maxButton.move(self.width()-60,0)
        self.closeButton.move(self.width()-30,0)
        self.label.move((self.width()-self.label.width()-2),(self.height()-self.label.height()-2))
        
        p = QPainter(self)
        pix = QPixmap("background.png")
        p.drawPixmap(QRect(0,60,self.width(),self.height()), pix)
        
        painter = QPainter(self)
        linearGradient = QLinearGradient(0,0,0,70)
        linearGradient.setColorAt(0, QColor(160,250,250))
        linearGradient.setColorAt(0.6, QColor(160,188,200))
        linearGradient.setColorAt(1, QColor(210,200,200))
        painter.setBrush(QBrush(linearGradient))
        self.menuRect = QRect(0, 0, self.width(), 70)
        painter.fillRect(self.menuRect, QBrush(linearGradient))
        
        aPoint = QPoint(0,0)
        bPoint = QPoint(self.width()-1,0)
        cPoint = QPoint(self.width()-1,self.height()-1)
        dPoint = QPoint(0,self.height()-1)
        
        self.linePath = QPainterPath()
        self.linePath.moveTo(aPoint)
        self.linePath.lineTo(bPoint)
        self.linePath.moveTo(bPoint)
        self.linePath.lineTo(cPoint)
        self.linePath.moveTo(cPoint)
        self.linePath.lineTo(dPoint)
        self.linePath.moveTo(dPoint)
        self.linePath.lineTo(aPoint)
        self.linePath.closeSubpath()
        
        p.drawPath(self.linePath)
        
app = QApplication(sys.argv)  
window = TestDialog()  
window.show()  
sys.exit(app.exec_())  